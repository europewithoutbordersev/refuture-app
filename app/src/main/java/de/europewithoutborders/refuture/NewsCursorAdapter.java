package de.europewithoutborders.refuture;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.europewithoutborders.refuture.data.ReFutureContract;

/**
 * Custom CursorAdapter that creates a list of news with their timestamp and title.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsCursorAdapter extends CursorAdapter {

    private static final String CHARSET_NAME = "UTF-8";
    private static final String TAG = "NewsCursorAdapter";

    public NewsCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.item_news, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvDate = (TextView) view.findViewById(R.id.newsDate);
        TextView tvHeading = (TextView) view.findViewById(R.id.newsHeading);

        String timestampString = cursor.getString(cursor.getColumnIndexOrThrow(ReFutureContract.NewsEntry.COLUMN_TIMESTAMP));
        Timestamp timestamp = new Timestamp(Long.parseLong(timestampString));
        Date date = new Date(timestamp.getTime());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy");
        String newsDate = simpleDateFormat.format(date);

        String heading = cursor.getString(cursor.getColumnIndexOrThrow(ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING));

        tvDate.setText(newsDate);
        try {
            tvHeading.setText(URLDecoder.decode(heading, CHARSET_NAME));
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
            Log.e(TAG, "encoding not supported", uee);
        }
    }
}
