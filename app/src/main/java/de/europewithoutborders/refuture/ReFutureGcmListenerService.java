package de.europewithoutborders.refuture;

import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

import de.europewithoutborders.refuture.restApi.RequestBodyBuilder;
import de.europewithoutborders.refuture.service.ReFutureRestServiceHelper;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Listener Service for GCM messages
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureGcmListenerService extends GcmListenerService {

    private static final String TAG = "GcmListenerService";

    /**
     * Will be called when a message arrives
     *
     * @param from ID of the sender
     * @param data Bundle to contain message data in key-value-pairs
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "From: " + from);
        int id = Integer.parseInt(data.getString("id"));
        ReFutureRestServiceHelper serviceHelper = ReFutureRestServiceHelper.getInstance(this);
        RequestBodyBuilder builder = new RequestBodyBuilder(this);
        if (from.startsWith("/topics/locations")) {
            Log.d(TAG, "location id:" + id);
            Util.getSharedPrefs(getBaseContext()).edit().putInt(Constants.SHARED_PREF_REST_ID_PARAMETER, id).apply();
            serviceHelper.postSinglePoi(builder.buildSinglePoiRequestBody().getBytes());
        } else {
            Log.d(TAG, "news id:" + id);
            Util.getSharedPrefs(getBaseContext()).edit().putInt(Constants.SHARED_PREF_REST_ID_PARAMETER, id).apply();
            serviceHelper.postSingleNews(builder.buildSingleNewsRequestBody().getBytes());
        }

    }

}
