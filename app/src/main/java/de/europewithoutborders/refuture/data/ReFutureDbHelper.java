package de.europewithoutborders.refuture.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import de.europewithoutborders.refuture.data.ReFutureContract.CountryEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.CountryLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeLocalizationEntry;

/**
 * Helper used to create a Database and tables as well as initial things. Further it's used to open a connection.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureDbHelper extends SQLiteOpenHelper {

    private static final String TAG = "DbHelper";

    /**
     * Current Database Version
     */
    private static final int DATABASE_VERSION = 1;

    /**
     * The name of the Database used to store on the device
     */
    private static final String DATABASE_NAME = "refuture.db";

    /**
     * Constructor used to create the database with the given version number.
     * @param context To use to open or create the database
     */
    public ReFutureDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        /**
         * Creates a table which stores POI Types
         */
        final String SQL_CREATE_POI_TYPE_TABLE = "CREATE TABLE " + PoiTypeEntry.TABLE_NAME + " (" +
                PoiTypeEntry.COLUMN_ID + " TEXT PRIMARY KEY ON CONFLICT IGNORE, " +
                PoiTypeEntry.COLUMN_ICON + " TEXT NOT NULL " +
                ");";

        /**
         * Creates a table which stores the localized values for POI Types
         */
        final String SQL_CREATE_POI_TYPE_LOCALIZATION_TABLE = "CREATE TABLE " + PoiTypeLocalizationEntry.TABLE_NAME + " (" +
                PoiTypeLocalizationEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID + " TEXT NOT NULL, " +
                PoiTypeLocalizationEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                PoiTypeLocalizationEntry.COLUMN_DESCRIPTION + " TEXT NULL, " +
                PoiTypeLocalizationEntry.COLUMN_LANGUAGE + " TEXT NOT NULL, " +

                // Set up the city column as a foreign key to city table.
                " FOREIGN KEY (" + PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID + ") REFERENCES " +
                PoiTypeEntry.TABLE_NAME + " (" + PoiTypeEntry.COLUMN_ID + ")" +
                " );";

        /**
         * Creates a table which stores POIs
         */
        final String SQL_CREATE_POI_TABLE = "CREATE TABLE " + PoiEntry.TABLE_NAME + " (" +
                PoiEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                PoiEntry.COLUMN_TYPE_ID + " TEXT NOT NULL, " +
                PoiEntry.COLUMN_LATITUDE + " REAL NOT NULL, " +
                PoiEntry.COLUMN_LONGITUDE + " REAL NOT NULL, " +
                PoiEntry.COLUMN_RATIO + " INTEGER NULL, " +
                PoiEntry.COLUMN_CREATED_BY_GOVERNMENT + " BOOLEAN NOT NULL CHECK (" + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT + " IN (0,1)), " +

                // Set up the type column as a foreign key to poi type table.
                " FOREIGN KEY (" + PoiEntry.COLUMN_TYPE_ID + ") REFERENCES " +
                PoiTypeEntry.TABLE_NAME + " (" + PoiTypeEntry.COLUMN_ID + ")" +
                " );";

        /**
         * Creates a table which stores the localized values for POIs
         */
        final String SQL_CREATE_POI_LOCALIZATION_TABLE = "CREATE TABLE " + PoiLocalizationEntry.TABLE_NAME + " (" +
                PoiLocalizationEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                PoiLocalizationEntry.COLUMN_POI_ID + " INTEGER NOT NULL, " +
                PoiLocalizationEntry.COLUMN_NAME + " TEXT NULL, " +
                PoiLocalizationEntry.COLUMN_DESCRIPTION + " TEXT NULL, " +
                PoiLocalizationEntry.COLUMN_CAPACITY + " TEXT NULL, " +
                PoiLocalizationEntry.COLUMN_LANGUAGE + " TEXT NOT NULL, " +

                // Set up the poi column as a foreign key to poi table.
                " FOREIGN KEY (" + PoiLocalizationEntry.COLUMN_POI_ID + ") REFERENCES " +
                PoiEntry.TABLE_NAME + " (" + PoiEntry.COLUMN_ID + ")" +
                " );";


        /**
         * Creates a table which stores News
         */
        final String SQL_CREATE_NEWS_TABLE = "CREATE TABLE " + NewsEntry.TABLE_NAME + " (" +
                NewsEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                NewsEntry.COLUMN_LATITUDE + " REAL NULL, " +
                NewsEntry.COLUMN_LONGITUDE + " REAL NULL, " +
                NewsEntry.COLUMN_COUNTRY + " TEXT NULL, " +
                NewsEntry.COLUMN_TIMESTAMP + " INTEGER NOT NULL " +
                " );";

        /**
         * Creates a table which stores the localized values for News
         */
        final String SQL_CREATE_NEWS_LOCALIZATION_TABLE = "CREATE TABLE " + NewsLocalizationEntry.TABLE_NAME + " (" +
                NewsLocalizationEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                NewsLocalizationEntry.COLUMN_NEWS_ID + " INTEGER NOT NULL, " +
                NewsLocalizationEntry.COLUMN_LANGUAGE + " TEXT NOT NULL, " +
                NewsLocalizationEntry.COLUMN_HEADING + " TEXT NOT NULL, " +
                NewsLocalizationEntry.COLUMN_BODY + " TEXT NOT NULL, " +

                // Set up the news column as a foreign key to news table.
                " FOREIGN KEY (" + NewsLocalizationEntry.COLUMN_NEWS_ID + ") REFERENCES " +
                NewsEntry.TABLE_NAME + " (" + NewsEntry.COLUMN_ID + ")" +
                " );";

        /**
         * Creates a table which stores Countries
         */
        final String SQL_CREATE_COUNTRY_TABLE = "CREATE TABLE " + CountryEntry.TABLE_NAME + " (" +
                CountryEntry.COLUMN_CODE + " TEXT PRIMARY KEY ON CONFLICT REPLACE, " +
                CountryEntry.COLUMN_PROCEEDING_DURATION + " INTEGER NULL, " +
                CountryEntry.COLUMN_UNEMPLOYMENT_RATE + " REAL NULL, " +
                CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED + " BOOLEAN NOT NULL CHECK (" + CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED + " IN (0,1)) " +
                " );";

        /**
         * Creates a table which stores the localized values for Countries
         */
        final String SQL_CREATE_COUNTRY_LOCALIZATION_TABLE = "CREATE TABLE " + CountryLocalizationEntry.TABLE_NAME + " (" +
                CountryLocalizationEntry.COLUMN_ID + " INTEGER PRIMARY KEY ON CONFLICT REPLACE, " +
                CountryLocalizationEntry.COLUMN_COUNTRY_CODE + " TEXT NOT NULL, " +
                CountryLocalizationEntry.COLUMN_NAME + " TEXT NOT NULL, " +
                CountryLocalizationEntry.COLUMN_LANGUAGE + " TEXT NOT NULL, " +
                CountryLocalizationEntry.COLUMN_LODGING_CAPACITY + " TEXT NULL, " +
                CountryLocalizationEntry.COLUMN_CAPACITY + " TEXT NULL, " +
                CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT + " TEXT NULL, " +
                CountryLocalizationEntry.COLUMN_REGULATION_INFLUX + " TEXT NULL, " +

                // Set up the news column as a foreign key to news table.
                " FOREIGN KEY (" + CountryLocalizationEntry.COLUMN_COUNTRY_CODE + ") REFERENCES " +
                CountryEntry.TABLE_NAME + " (" + CountryEntry.COLUMN_CODE + ")" +
                " );";

        // execute SQL statements
        db.execSQL(SQL_CREATE_POI_TYPE_TABLE);
        db.execSQL(SQL_CREATE_POI_TYPE_LOCALIZATION_TABLE);
        db.execSQL(SQL_CREATE_POI_TABLE);
        db.execSQL(SQL_CREATE_POI_LOCALIZATION_TABLE);
        db.execSQL(SQL_CREATE_COUNTRY_TABLE);
        db.execSQL(SQL_CREATE_COUNTRY_LOCALIZATION_TABLE);
        db.execSQL(SQL_CREATE_NEWS_TABLE);
        db.execSQL(SQL_CREATE_NEWS_LOCALIZATION_TABLE);
        Log.d(TAG, "Created Tables!");

        // execute SQL insert statement
        db.execSQL("INSERT INTO '" + PoiTypeEntry.TABLE_NAME + "'" +
                " ('" + PoiTypeEntry.COLUMN_ID + "', '" + PoiTypeEntry.COLUMN_ICON + "') VALUES" +
                " ('housing', 'housing');");
        db.execSQL("INSERT INTO '" + PoiTypeEntry.TABLE_NAME + "'" +
                " ('" + PoiTypeEntry.COLUMN_ID + "', '" + PoiTypeEntry.COLUMN_ICON + "') VALUES" +
                " ('medicine', 'medicine');");
        db.execSQL("INSERT INTO '" + PoiTypeEntry.TABLE_NAME + "'" +
                " ('" + PoiTypeEntry.COLUMN_ID + "', '" + PoiTypeEntry.COLUMN_ICON + "') VALUES" +
                " ('information', 'information');");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop tables if they exist. Will only be used when upgrading to a higher Database version
        db.execSQL("DROP TABLE IF EXISTS " + PoiLocalizationEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PoiEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PoiTypeLocalizationEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PoiTypeEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NewsLocalizationEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + NewsEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CountryLocalizationEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CountryEntry.TABLE_NAME);

        // create the Database schema
        onCreate(db);
    }
}
