package de.europewithoutborders.refuture.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import de.europewithoutborders.refuture.data.ReFutureContract.CountryEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.CountryLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeLocalizationEntry;

/**
 * Provider which will be called through the ContentResolver with the correct authority.
 *
 * @see ReFutureContract#CONTENT_AUTHORITY Content authority for ReFuture
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureProvider extends ContentProvider {

    private static final String TAG = "ReFutureProvider";

    /**
     * URI Matcher to create a mapping from URIs to unique IDs
     */
    private static final UriMatcher sUriMatcher = buildUriMatcher();

    /**
     * Database Helper to access the data
     */
    private ReFutureDbHelper mOpenHelper;

    // Unique IDs for the URIs
    private static final int COUNTRY = 100;
    private static final int COUNTRY_ID = 101;
    private static final int COUNTRY_LOCALIZED = 150;
    private static final int NEWS = 200;
    private static final int NEWS_ID = 201;
    private static final int NEWS_LOCALIZED = 250;
    private static final int POI = 400;
    private static final int POI_ID = 401;
    private static final int POI_LOCALIZED = 450;
    private static final int POI_TYPE = 500;
    private static final int POI_TYPE_LOCALIZED = 550;

    /**
     * Query Builder to create JOINs between POIs, POIs Localizations, POI Type and POI Type Localizations
     */
    private static final SQLiteQueryBuilder sPoiAndTypeQueryBuilder;
    /**
     * Query Builder to create JOINs between POI Type and POI Type Localizations
     */
    private static final SQLiteQueryBuilder sPoiTypeLocalizationBuilder;
    /**
     * Query Builder to create JOINs between News and News Localizations
     */
    private static final SQLiteQueryBuilder sNewsLocalizationQueryBuilder;
    /**
     * Query Builder to create JOINs between Countries and Country Localizations
     */
    private static final SQLiteQueryBuilder sCountryLocalizationBuilder;

    /**
     * Create the matching from the URIs to the IDs
     *
     * @return A UriMatcher with a correct matching from URIs to ints
     */
    static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = ReFutureContract.CONTENT_AUTHORITY;

        // Matching from BASE URIs
        matcher.addURI(authority, ReFutureContract.PATH_COUNTRY, COUNTRY);
        matcher.addURI(authority, ReFutureContract.PATH_COUNTRY_LOCALIZED, COUNTRY_LOCALIZED);
        matcher.addURI(authority, ReFutureContract.PATH_NEWS, NEWS);
        matcher.addURI(authority, ReFutureContract.PATH_NEWS_LOCALIZED, NEWS_LOCALIZED);
        matcher.addURI(authority, ReFutureContract.PATH_POI, POI);
        matcher.addURI(authority, ReFutureContract.PATH_POI_LOCALIZED, POI_LOCALIZED);
        matcher.addURI(authority, ReFutureContract.PATH_POI_TYPE, POI_TYPE);
        matcher.addURI(authority, ReFutureContract.PATH_POI_TYPE_LOCALIZED, POI_TYPE_LOCALIZED);

        // Matching from URIs with appended IDs
        matcher.addURI(authority, ReFutureContract.PATH_COUNTRY + "/#", COUNTRY_ID);
        matcher.addURI(authority, ReFutureContract.PATH_NEWS + "/#", NEWS_ID);
        matcher.addURI(authority, ReFutureContract.PATH_POI + "/#", POI_ID);

        return matcher;
    }

    static{
        sPoiAndTypeQueryBuilder = new SQLiteQueryBuilder();
        // Inner join which looks like:
        // poi INNER JOIN poi_type ON poi.type_id = poi_type.id
        // INNER JOIN poi_localized ON poi.id = poi_localized.poi_id
        // INNER JOIN poi_type_localized ON poi_type.id = poi_type_localized.poi_type_id
        sPoiAndTypeQueryBuilder.setTables(
                PoiEntry.TABLE_NAME + " INNER JOIN " +
                        PoiTypeEntry.TABLE_NAME +
                        " ON " + PoiEntry.TABLE_NAME +
                        "." + PoiEntry.COLUMN_TYPE_ID +
                        " = " + PoiTypeEntry.TABLE_NAME +
                        "." + PoiTypeEntry.COLUMN_ID +
                " INNER JOIN " + PoiLocalizationEntry.TABLE_NAME +
                        " ON " + PoiEntry.TABLE_NAME +
                        "." + PoiEntry.COLUMN_ID +
                        " = " + PoiLocalizationEntry.TABLE_NAME +
                        "." + PoiLocalizationEntry.COLUMN_POI_ID +
                " INNER JOIN " + PoiTypeLocalizationEntry.TABLE_NAME +
                        " ON " + PoiTypeEntry.TABLE_NAME +
                        "." + PoiTypeEntry.COLUMN_ID +
                        " = " + PoiTypeLocalizationEntry.TABLE_NAME +
                        "." + PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID
        );

        sPoiTypeLocalizationBuilder = new SQLiteQueryBuilder();
        // Inner join which looks like:
        // poi_type INNER JOIN poi_type_localized ON poi_type.id = poi_type_localized.poi_type_id
        sPoiTypeLocalizationBuilder.setTables(
                PoiTypeEntry.TABLE_NAME + " INNER JOIN " +
                        PoiTypeLocalizationEntry.TABLE_NAME +
                        " ON " + PoiTypeEntry.TABLE_NAME +
                        "." + PoiTypeEntry.COLUMN_ID +
                        " = " + PoiTypeLocalizationEntry.TABLE_NAME +
                        "." + PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID
        );

        sNewsLocalizationQueryBuilder = new SQLiteQueryBuilder();
        // Inner join which looks like:
        // news INNER JOIN news_localized ON news.id = news_localized.news_id
        sNewsLocalizationQueryBuilder.setTables(
                NewsEntry.TABLE_NAME + " INNER JOIN " +
                        NewsLocalizationEntry.TABLE_NAME +
                        " ON " + NewsEntry.TABLE_NAME +
                        "." + NewsEntry.COLUMN_ID +
                        " = " + NewsLocalizationEntry.TABLE_NAME +
                        "." + NewsLocalizationEntry.COLUMN_NEWS_ID
        );

        sCountryLocalizationBuilder = new SQLiteQueryBuilder();
        // Inner join which looks like:
        // country INNER JOIN country_localized ON country.code = country_localized.country_code
        sCountryLocalizationBuilder.setTables(
                CountryEntry.TABLE_NAME + " INNER JOIN " +
                        CountryLocalizationEntry.TABLE_NAME +
                        " ON " + CountryEntry.TABLE_NAME +
                        "." + CountryEntry.COLUMN_CODE +
                        " = " + CountryLocalizationEntry.TABLE_NAME +
                        "." + CountryLocalizationEntry.COLUMN_COUNTRY_CODE
        );

    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new ReFutureDbHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // get readable access to the Database
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        // find the matching ID to the URI
        final int match = sUriMatcher.match(uri);
        Cursor cursor = null;

        // get the requested Cursor
        switch (match) {
            case COUNTRY: {
                cursor = sCountryLocalizationBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            }
            case COUNTRY_ID: {
                cursor = sCountryLocalizationBuilder.query(db, projection, CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_CODE + " = ?", new String[]{uri.getLastPathSegment()}, null, null, sortOrder);
                break;
            }
            case NEWS: {
                cursor = sNewsLocalizationQueryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            }
            case NEWS_ID: {
                cursor = sNewsLocalizationQueryBuilder.query(db, projection, NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_ID + " = ?", new String[]{uri.getLastPathSegment()}, null, null, sortOrder);
                break;
            }
            case POI: {
                cursor = sPoiAndTypeQueryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            }
            case POI_ID: {
                cursor = sPoiAndTypeQueryBuilder.query(db, projection, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID + " = ?", new String[]{uri.getLastPathSegment()}, null, null, sortOrder);
                break;
            }
            case POI_TYPE: {
                cursor = sPoiTypeLocalizationBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            }
        }

        if(cursor != null) {
            // register the uri to updates
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        // not needed in this implementation
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // get writable access to the Database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        // find the matching ID to the URI
        final int match = sUriMatcher.match(uri);
        Uri returnUri = null;

        // insert data to the correct table
        switch(match) {
            case COUNTRY: {
                long _id = db.insert(CountryEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = CountryEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case COUNTRY_LOCALIZED: {
                long _id = db.insert(CountryLocalizationEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = CountryLocalizationEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case NEWS: {
                long _id = db.insert(NewsEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = NewsEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case NEWS_LOCALIZED: {
                long _id = db.insert(NewsLocalizationEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = NewsLocalizationEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case POI: {
                long _id = db.insert(PoiEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = PoiEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case POI_LOCALIZED: {
                long _id = db.insert(PoiLocalizationEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = PoiLocalizationEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case POI_TYPE: {
                long _id = db.insert(PoiTypeEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = PoiTypeEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case POI_TYPE_LOCALIZED: {
                long _id = db.insert(PoiTypeLocalizationEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = PoiTypeLocalizationEntry.buildUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
        }

        // update uri observers
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        // get writable access to the Database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int returnCount = 0;
        String tableName = null;

        // find the matching ID to the URI
        final int match = sUriMatcher.match(uri);
        // get the correct table to bulkinsert data to
        switch (match) {
            case COUNTRY: {
                tableName = CountryEntry.TABLE_NAME;
                break;
            }
            case COUNTRY_LOCALIZED: {
                tableName = CountryLocalizationEntry.TABLE_NAME;
                break;
            }
            case NEWS: {
                tableName = NewsEntry.TABLE_NAME;
                break;
            }
            case NEWS_LOCALIZED: {
                tableName = NewsLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI: {
                tableName = PoiEntry.TABLE_NAME;
                break;
            }
            case POI_LOCALIZED: {
                tableName = PoiLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE: {
                tableName = PoiTypeEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE_LOCALIZED: {
                tableName = PoiTypeLocalizationEntry.TABLE_NAME;
                break;
            }
        }

        if(tableName != null) {
            // begin the transaction
            db.beginTransaction();
            try {
                // insert data
                for (ContentValues value : values) {
                    long _id = db.insert(tableName, null, value);
                    if (_id != -1) {
                        returnCount++;
                    }
                }
                // transaction was successful
                db.setTransactionSuccessful();
            } finally {
                // end transaction
                db.endTransaction();
            }

            // update uri observers
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // return amount of rows added
        return returnCount;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // get writable access to the Database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        String tableName = null;

        // find the matching ID to the URI
        final int match = sUriMatcher.match(uri);
        int rows = 0;

        // get the correct table to delete data from
        switch (match) {
            case COUNTRY: {
                tableName = CountryEntry.TABLE_NAME;
                break;
            }
            case COUNTRY_LOCALIZED: {
                tableName = CountryLocalizationEntry.TABLE_NAME;
                break;
            }
            case NEWS: {
                tableName = NewsEntry.TABLE_NAME;
                break;
            }
            case NEWS_LOCALIZED: {
                tableName = NewsLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI: {
                tableName = PoiEntry.TABLE_NAME;
                break;
            }
            case POI_LOCALIZED: {
                tableName = PoiLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE: {
                tableName = PoiTypeEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE_LOCALIZED: {
                tableName = PoiTypeLocalizationEntry.TABLE_NAME;
                break;
            }
        }

        if(tableName != null) {
            // delete rows
            rows = db.delete(tableName, selection, selectionArgs);
            // notify uri observers
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // return amount of rows deleted
        return rows;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // get writable access to the Database
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        String tableName = null;

        // find the matching ID to the URI
        final int match = sUriMatcher.match(uri);
        int rows = -1;

        // get the correct table to update data from
        switch (match) {
            case COUNTRY: {
                tableName = CountryEntry.TABLE_NAME;
                break;
            }
            case COUNTRY_LOCALIZED: {
                tableName = CountryLocalizationEntry.TABLE_NAME;
                break;
            }
            case NEWS: {
                tableName = NewsEntry.TABLE_NAME;
                break;
            }
            case NEWS_LOCALIZED: {
                tableName = NewsLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI: {
                tableName = PoiEntry.TABLE_NAME;
                break;
            }
            case POI_LOCALIZED: {
                tableName = PoiLocalizationEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE: {
                tableName = PoiTypeEntry.TABLE_NAME;
                break;
            }
            case POI_TYPE_LOCALIZED: {
                tableName = PoiTypeLocalizationEntry.TABLE_NAME;
                break;
            }
        }

        if(tableName != null) {
            // update rows
            rows = db.update(tableName, values, selection, selectionArgs);
            // notify uri observers
            getContext().getContentResolver().notifyChange(uri, null);
        }

        // return amount of rows updated
        return rows;
    }
}
