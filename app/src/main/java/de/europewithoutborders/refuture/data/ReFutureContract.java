package de.europewithoutborders.refuture.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract for the Content Provider.
 * May be delivered to other developers so they can use our data structure within their apps.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureContract {

    /**
     * Content authority for the content provider
     */
    public static final String CONTENT_AUTHORITY = "de.europewithoutborders.refuture";

    /**
     * Base URI to access our data structure
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Path for the Country URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.CountryEntry
     */
    public static final String PATH_COUNTRY = "country";

    /**
     * Path for the Country Localized URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.CountryLocalizationEntry
     */
    public static final String PATH_COUNTRY_LOCALIZED = "country_localized";

    /**
     * Path for the News URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.NewsEntry
     */
    public static final String PATH_NEWS = "news";

    /**
     * Path for the News Localized URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.NewsLocalizationEntry
     */
    public static final String PATH_NEWS_LOCALIZED = "news_localized";

    /**
     * Path for the POI Type URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeEntry
     */
    public static final String PATH_POI_TYPE = "poi_type";

    /**
     * Path for the POI Type Localized URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeLocalizationEntry
     */
    public static final String PATH_POI_TYPE_LOCALIZED = "poi_type_localized";

    /**
     * Path for the POI URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry
     */
    public static final String PATH_POI = "poi";

    /**
     * Path for the POI Localized URI
     * @see de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry
     */
    public static final String PATH_POI_LOCALIZED = "poi_localized";

    /**
     * Class to define the table and columns for the table "country"
     */
    public static final class CountryEntry implements BaseColumns {
        /**
         * Content URI to access CountryEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUNTRY).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COUNTRY;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COUNTRY;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "country";

        // Columns
        public static final String COLUMN_CODE = "_id";
        public static final String COLUMN_PROCEEDING_DURATION = "proceeding_duration";
        public static final String COLUMN_UNEMPLOYMENT_RATE = "unemployment_rate";
        public static final String COLUMN_NATIVE_LANGUAGE_REQUIRED = "native_language_required";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "country_localized"
     */
    public static final class CountryLocalizationEntry implements BaseColumns {
        /**
         * Content URI to access CountryLocalizationEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_COUNTRY_LOCALIZED).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COUNTRY_LOCALIZED;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_COUNTRY_LOCALIZED;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "country_localized";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_COUNTRY_CODE = "country_code";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_LANGUAGE = "language";
        public static final String COLUMN_LODGING_CAPACITY = "lodging_capacity";
        public static final String COLUMN_CAPACITY = "capacity";
        public static final String COLUMN_REQUIREMENTS_WORK_PERMIT = "requirements_work_permit";
        public static final String COLUMN_REGULATION_INFLUX = "regulation_influx";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "news"
     */
    public static final class NewsEntry implements BaseColumns {
        /**
         * Content URI to access NewsEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NEWS).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NEWS;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NEWS;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "news";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_COUNTRY = "country";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_TIMESTAMP = "timestamp";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "news_localized"
     */
    public static final class NewsLocalizationEntry implements BaseColumns {
        /**
         * Content URI to access NewsLocalizationEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NEWS_LOCALIZED).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NEWS_LOCALIZED;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NEWS_LOCALIZED;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "news_localized";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_NEWS_ID = "news_id";
        public static final String COLUMN_LANGUAGE = "language";
        public static final String COLUMN_HEADING = "heading";
        public static final String COLUMN_BODY = "body";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "poi"
     */
    public static final class PoiEntry implements BaseColumns {
        /**
         * Content URI to access PoiEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "poi";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_TYPE_ID = "type_id";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_RATIO = "ratio";
        public static final String COLUMN_CREATED_BY_GOVERNMENT = "createdByGovernment";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "poi_localized"
     */
    public static final class PoiLocalizationEntry implements BaseColumns {
        /**
         * Content URI to access PoiLocalizationEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI_LOCALIZED).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_LOCALIZED;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_LOCALIZED;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "poi_localized";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_POI_ID = "poi_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_LANGUAGE = "language";
        public static final String COLUMN_CAPACITY = "capacity";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "poi_type"
     */
    public static final class PoiTypeEntry implements BaseColumns {
        /**
         * Content URI to access PoiTypeEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI_TYPE).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_TYPE;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_TYPE;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "poi_type";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_ICON = "icon";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }

    /**
     * Class to define the table and columns for the table "poi_type_localized"
     */
    public static final class PoiTypeLocalizationEntry implements BaseColumns {
        /**
         * Content URI to access PoiTypeLocalizationEntry
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_POI_TYPE_LOCALIZED).build();

        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_TYPE_LOCALIZED;
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_POI_TYPE_LOCALIZED;

        /**
         * Table name
         */
        public static final String TABLE_NAME = "poi_type_localized";

        // Columns
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_POI_TYPE_ID = "poi_type_id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_LANGUAGE = "language";

        /**
         * Creates the Content URI with the ID appended to it.
         * @param id The ID to append to this Content URI
         * @return Content URI with appended ID
         */
        public static Uri buildUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
