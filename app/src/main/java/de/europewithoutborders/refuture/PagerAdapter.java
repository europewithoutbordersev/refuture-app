package de.europewithoutborders.refuture;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Custom FragmentStatePagerAdapter that creates the LocalNewsFragment and GlobalNewsFragment.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int mNumOfTabs) {
        super(fm);
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                LocalNewsFragment tab1 = new LocalNewsFragment();
                return tab1;
            case 1:
                GlobalNewsFragment tab2 = new GlobalNewsFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
