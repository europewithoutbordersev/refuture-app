package de.europewithoutborders.refuture;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeLocalizationEntry;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Shows the map with the data, such as the user's position and POIs
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class MapLocationActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener, LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "MapLocationActivity";

    private SharedPreferences mSharedPrefs;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment = null;
    boolean firstTime = true;
    private List<LatLng> mPoiList = new ArrayList<>();
    private LatLng mCurrentLatLng;
    private Map<Marker, Integer> mMarkerMap = new HashMap<Marker, Integer>();
    private boolean mMapPacked = false;

    private int mRadius;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPrefs = Util.getSharedPrefs(this);
        mRadius = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_RADIUS, Constants.DEFAULT_RADIUS_IN_KM);
        showLocationOnMap();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationChangeListener(this);
    }

    /**
     * Loads the view synchronously and the map asynchronously
     */
    public void showLocationOnMap() {
        if (Util.isGPSEnabled(getContext())) {
            setContentView(R.layout.activity_map_location);
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else {
            Util.disabledGPSError(this);
        }
    }

    /**
     * Packs the map such that unused space will be cut off by zooming in
     */
    private void packMap() {
        if(!mMapPacked) {
            LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();

            for (LatLng latLng : mPoiList) {
                latLngBuilder.include(latLng);
            }

            if (mCurrentLatLng != null) {
                latLngBuilder.include(mCurrentLatLng);
            }

            if (mCurrentLatLng != null || !mPoiList.isEmpty()) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBuilder.build(), 60));
                mMapPacked = true;
            }
        }
    }

    /**
     * Returns the {@link Context} of this Activity
     * @return A valid {@link Context}
     */
    public Context getContext() {
        return this;
    }

    /**
     * Will be called when the activity is about to be resumed.
     * See Android Lifecycle for further details.
     */
    public void onResume() {
        super.onResume();
        if(firstTime) {
            // do something
            firstTime = false;
        } else if (mMap == null){
            showLocationOnMap();
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = PoiEntry.CONTENT_URI;
        // Define projection
        String[] projection = new String[] {PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID,
                                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE,
                                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE,
                                PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ICON};

        // Get languages selected by the user
        List<String> languageList = Util.getSetLanguages(getContext());
        // Get the escaped selection based on the languages selected by the user, so that it can be used with prepared statements
        String escapedSelection = Util.getEscapedLanguageSelection(languageList);

        // Calculate Bounds based on the current position and the defined radius
        LatLngBounds bounds = Util.getLatLngBounds(mCurrentLatLng, mRadius);
        Log.d(TAG, "NorthEast: " + bounds.northeast.latitude + ", " + bounds.northeast.longitude);
        Log.d(TAG, "SouthWest: " + bounds.southwest.latitude + ", " + bounds.southwest.longitude);

        // Create selection String (SQL: WHERE ...)
        String selection = PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE + " IN (" + escapedSelection + ") AND " +
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE + " IN (" + escapedSelection + ") AND " +
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE + " <= " + bounds.northeast.latitude + " AND " + PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE + " >= " + bounds.southwest.latitude + " AND " +
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE + " <= " + bounds.northeast.longitude + " AND " + PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE + " >= " + bounds.southwest.longitude;

        Log.d(TAG, "Selection: " + selection);

        // duplicate languages, because we need them in both IN-statements!
        String[] languages = new String[languageList.size() * 2];
        for(int i = 0; i < languages.length; i++) {
            languages[i] = languageList.get(i % languageList.size());
        }
        Log.d(TAG, TextUtils.join(",", languages));

        return new CursorLoader(this, uri, projection, selection, languages, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(loader.getId() == Constants.LOADER_POI_DATA) {
            Log.d(TAG, "Loader found " + data.getCount() + " entries!");

            // clear old data
            mMap.clear();
            mPoiList.clear();
            mMarkerMap.clear();

            int poiId = 0;
            int poiLat = 1;
            int poiLon = 2;
            int typeIcon = 3;

            data.moveToFirst();

            // iterate over results
            for(int i = 0; i < data.getCount(); i++) {
                float lat = data.getFloat(poiLat);
                float lon = data.getFloat(poiLon);

                LatLng latLng = new LatLng(lat, lon);
                // add position to the list
                if(!mPoiList.contains(latLng)) {
                    mPoiList.add(latLng);

                    // get the name of the icon to display
                    String iconName = data.getString(typeIcon);

                    // show marker with the icon at the correct position
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(Util.getResourceId(iconName)));

                    // Adding marker on the Google Map
                    Marker marker = mMap.addMarker(markerOptions);
                    // Adding markerOptions to the MarkerMap
                    mMarkerMap.put(marker, data.getInt(poiId));
                }

                data.moveToNext();
            }

            // set onClickListener
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    int id = mMarkerMap.get(marker);
                    Log.d(TAG, "PoiId: " + id);
                    Intent intent = new Intent(getContext(), PoiDetailsActivity.class);
                    intent.putExtra("poiId", id);
                    startActivity(intent);
                    return true;
                }
            });
            // zoom
            packMap();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "Loader reset!");
        // remove all markers
        mMap.clear();
        mPoiList.clear();
        mMarkerMap.clear();
        // zoom
        packMap();
    }

    @Override
    public void onMyLocationChange(Location location) {
        if(mCurrentLatLng == null || Util.haversine(location.getLatitude(), location.getLongitude(), mCurrentLatLng.latitude, mCurrentLatLng.longitude) >= Constants.MIN_DISTANCE_IN_KM_FOR_MAP_UPDATES) {
            Log.d(TAG, "Updating Location and Map...");
            mCurrentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            // restart loader, because we have to revalidate the data :)
            getSupportLoaderManager().restartLoader(Constants.LOADER_POI_DATA, null, this);
        }
    }
}
