package de.europewithoutborders.refuture;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Comparator;

import de.europewithoutborders.refuture.model.Language;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Fragment to contain language selection
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class LanguagePicker extends DialogFragment implements Comparator<Language> {

    private SharedPreferences mSharedPrefs;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mSharedPrefs = Util.getSharedPrefs(getContext());

        // Inflate view
        View view = LayoutInflater.from(getContext()).inflate(R.layout.language_picker, null);
        ListView languageList = (ListView) view.findViewById(R.id.language_picker_listview);

        // Set adapter
        final LanguageListAdapter adapter = new LanguageListAdapter(getActivity());
        languageList.setAdapter(adapter);

        // show Dialog with languages
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.register_languages))
                .setPositiveButton(getString(R.string.register_languages_ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ArrayList<String> oldLanguages = new ArrayList<String>();
                                if (mSharedPrefs.getBoolean("ar", false)) {
                                    oldLanguages.add("ar");
                                }
                                if (mSharedPrefs.getBoolean("en", false)) {
                                    oldLanguages.add("en");
                                }
                                if (mSharedPrefs.getBoolean("fr", false)) {
                                    oldLanguages.add("fr");
                                }
                                if (mSharedPrefs.getBoolean("de", false)) {
                                    oldLanguages.add("de");
                                }

                                // save new languages
                                SharedPreferences.Editor editor = mSharedPrefs.edit();
                                for(int i = 0; i < adapter.getCount(); i++) {
                                    Language lang = (Language) adapter.getItem(i);
                                    editor.putBoolean(lang.getIsoCode(), lang.isChecked());
                                }
                                editor.apply();

                                // notify SettingsActivity that language selection is finished
                                Intent intent = new Intent(Constants.BROADCAST_LANGUAGES_SELECTED);
                                Bundle bundle = new Bundle();
                                bundle.putStringArrayList(Constants.BUNDLE_OLD_LANGUAGES, oldLanguages);
                                intent.putExtras(bundle);
                                LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
                            }
                        }
                )
                .setNegativeButton(getString(R.string.register_languages_cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                )
                .setView(view)
                .create();
    }

    @Override
    public int compare(Language lhs, Language rhs) {
        return lhs.getLanguage().compareToIgnoreCase(rhs.getLanguage());
    }
}
