package de.europewithoutborders.refuture;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Service to load / update the GCM Token
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureInstanceIDListenerService extends InstanceIDListenerService {

    private static final String TAG = "InstanceIDLS";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. This call is initiated by the
     * InstanceID provider.
     */
    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
