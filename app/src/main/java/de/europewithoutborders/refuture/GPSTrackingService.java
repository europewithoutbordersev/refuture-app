package de.europewithoutborders.refuture;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import de.europewithoutborders.refuture.restApi.RequestBodyBuilder;
import de.europewithoutborders.refuture.service.ReFutureRestServiceHelper;
import de.europewithoutborders.refuture.util.Constants;

/**
 * Service to locate the user's current GPS position
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GPSTrackingService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = "GPSTracking";

    /**
     * Fused access to Google APIs to request GPS position based on Google's data
     */
    private GoogleApiClient mGoogleApiClient;

    /**
     * Request object to determine details about location updates
     */
    private static LocationRequest sLocationRequest = new LocationRequest();

    static {
        // interval of default request updates
        sLocationRequest.setInterval(Constants.POSITION_UPDATE_TIME_MINS * 60 * 1000);
        // shortest request update, may be triggered by other apps / services
        sLocationRequest.setFastestInterval(Constants.POSITION_FASTEST_UPDATE_INTERVAL_MINS * 60 * 1000);
        // accuracy of the current position
        sLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public GPSTrackingService() {
        super();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");

        // access Google API
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        Log.d(TAG, "Connecting");
        mGoogleApiClient.connect();

        // Intent to trigger when clicked on the Notification
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        String notificationText = String.format(getResources().getString(R.string.notification_text),
                getResources().getString(R.string.app_name));

        // Build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setColor(ContextCompat.getColor(this, android.R.color.white))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(notificationText)
                .setContentIntent(pendingIntent); //Required on Gingerbread and below
        // Show notification, run as foreground service
        startForeground(23, mBuilder.build());

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "Request Location Updates");
        // start listening to location updates with the set parameters
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, sLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location changed!");
        Log.d(TAG, "Sent location (" + location.getLatitude() + ", " + location.getLongitude() + ") to the server!");
        // send location to server
        ReFutureRestServiceHelper serviceHelper = ReFutureRestServiceHelper.getInstance(getApplicationContext());
        RequestBodyBuilder builder = new RequestBodyBuilder(getApplicationContext());
        serviceHelper.postPosition(builder.buildPositionRequestBody(location.getLatitude(), location.getLongitude()).getBytes());
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onDestroy() {
        Log.d(TAG, "Destroying & Disconnecting");
        // disconnect from Google API
        mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
