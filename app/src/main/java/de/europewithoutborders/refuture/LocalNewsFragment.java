package de.europewithoutborders.refuture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * LocalNewsFragment displays all news in a list that were created for the same country or in the radius
 * of the user.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class LocalNewsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final String TAG = "LocalNewsFragment";

    private ListView mListView;
    private View view;
    private BroadcastReceiver requestReceiver;
    private String lang = "";
    private String countryCode = "";
    private static Location location = null;
    private LocationManager lm;
    private LocationListener locationListener;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);
        mListView = (ListView) view.findViewById(R.id.news_list);

        countryCode = getCountryCode();
        IntentFilter filter = new IntentFilter("NewsFragment");
        requestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                lang = intent.getStringExtra("LANG");
                Log.d(TAG, "language: " + lang);
                showLocalNews(lang, countryCode, location);
            }
        };
        getContext().registerReceiver(requestReceiver, filter);
        lm = (LocationManager) getActivity().getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        try {
            location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (location != null) {
                Log.d(TAG, "Got last known location: "  + location.getLatitude() + " and " + location.getLongitude());
                showLocalNews(lang, countryCode, location);
            } else {
                Log.d(TAG, "Location not found");
                locationListener = new LocationListener() {
                    public void onLocationChanged(Location loc) throws SecurityException {
                        Log.d(TAG, "location changed: " + loc.getLatitude() + " and " + loc.getLongitude());
                        location = loc;

                        showLocalNews(lang, countryCode, loc);
                    }

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {}

                    @Override
                    public void onProviderEnabled(String provider) {}

                    @Override
                    public void onProviderDisabled(String provider) {}
                };
            }
        } catch (SecurityException se) {
            se.printStackTrace();
        }

        return view;
    }

    /**
     * Displays ever country news and location news within the users radius in a ListView.
     *
     * @param lang
     * @param countryCode
     * @param loc
     */
    private void showLocalNews(String lang, String countryCode, Location loc) {
        Log.d(TAG, "language: " + lang);
        Log.d(TAG, "countryCode: " + countryCode);
        SharedPreferences mSharedPrefs = Util.getSharedPrefs(getActivity());
        Cursor mCursor = getLocalNews(lang, countryCode);

        if(mCursor != null){
            mListView = (ListView) view.findViewById(R.id.news_list);
            if (loc != null) {
                int radius = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_RADIUS, 50);
                double lat = loc.getLatitude();
                double lon = loc.getLongitude();
                LocationFilterCursorWrapper filteredCursor = new LocationFilterCursorWrapper(mCursor, radius, lat, lon);
                NewsCursorAdapter adapter = new NewsCursorAdapter(getActivity(), filteredCursor, 0);
                mListView.setAdapter(adapter);
                mListView.setOnItemClickListener(this);
            }
        }
    }

    /**
     * OnItemClick Event that starts an Intent with the news details for a list item.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
        int newsId = cursor.getInt(cursor.getColumnIndexOrThrow(ReFutureContract.NewsEntry.COLUMN_ID));
        Intent newsDetailsIntent = new Intent(getActivity(), NewsDetailsActivity.class);

        newsDetailsIntent.putExtra("newsId", newsId);
        newsDetailsIntent.putExtra("lang", lang);
        startActivity(newsDetailsIntent);
    }

    /**
     * Reads every country and location news from the database in a specific language and country code.
     *
     * @param lang
     * @param countryCode
     * @return Cursor
     */
    private Cursor getLocalNews(String lang, String countryCode) {
        Uri newsUri = ReFutureContract.NewsEntry.CONTENT_URI;
        String[] projection = {ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_ID,
                ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING,
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_TIMESTAMP,
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LONGITUDE,
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LATITUDE};

        String selection = "(" + ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_LANGUAGE + " = ? AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LATITUDE + " IS NOT NULL AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LONGITUDE + " IS NOT NULL AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_COUNTRY + " IS NULL) OR (" +
                ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_LANGUAGE + " = ? AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_COUNTRY + " = ?)";

        String[] selectionArgs = {lang, lang, countryCode};
        String sortOrder = ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_TIMESTAMP + " DESC";
        return getActivity().getContentResolver().query(newsUri, projection, selection, selectionArgs, sortOrder);
    }

    /**
     * Gets the country code from the telephony manager of the user.
     *
     * @return country code
     */
    private String getCountryCode(){
        TelephonyManager manager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        return manager.getSimCountryIso();
    }

    /**
     * Unregisters the Receiver in the onDestroy process.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(requestReceiver);
        locationListener = null;
    }
}
