package de.europewithoutborders.refuture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.countrypicker.CountryPicker;
import com.countrypicker.CountryPickerListener;

import java.util.List;
import java.util.Locale;

import de.europewithoutborders.refuture.restApi.RequestBodyBuilder;
import de.europewithoutborders.refuture.service.ReFutureRestServiceHelper;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Activity to show and edit the user's settings
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "RegisterActivity";

    private SharedPreferences mSharedPrefs;

    private Button mSaveButton;

    private TextInputLayout mAgeLayout;
    private EditText mAgeText;
    private RadioButton mMaleBox;
    private RadioButton mFemaleBox;
    private TextView mLanguagesText;
    private LinearLayout mStartCountryLayout;
    private TextView mStartCountryText;
    private ImageView mStartCountryIcon;
    private LinearLayout mEndCountryLayout;
    private TextView mEndCountryText;
    private ImageView mEndCountryIcon;
    private TextInputLayout mEntourageLayout;
    private EditText mEntourageText;
    private TextInputLayout mInfluxLayout;
    private EditText mInfluxText;
    private TextInputLayout mRadiusLayout;
    private EditText mRadiusText;

    private int mAge = -1;
    private boolean mIsMale;
    private int mEntourage = -1;
    private int mInflux = -1;
    private int mRadius = -1;

    private boolean mAgeValid = true;
    private boolean mEntourageValid = true;
    private boolean mInfluxValid = true;
    private boolean mRadiusValid = true;

    private boolean mIsInitialOpened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mIsInitialOpened = getIntent().getIntExtra(Constants.SETTINGS_CALL_DATA, -1) == Constants.SETTINGS_INITIAL_CALL;

        mSharedPrefs = Util.getSharedPrefs(this);

        getGCMToken();

        Toolbar toolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.register_title));

        if(!mIsInitialOpened) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mSaveButton = (Button) findViewById(R.id.register_save_button);
        mSaveButton.setOnClickListener(this);

        mAgeLayout = (TextInputLayout) findViewById(R.id.register_age_layout);
        mAgeText = (EditText) findViewById(R.id.register_age);

        mAgeText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mAgeValid = true;
                try {
                    mAge = Integer.parseInt(s.toString().trim());
                    if(mAge < 1 || mAge > 99) {
                        mAgeLayout.setError(getString(R.string.register_exception_age_invalid));
                        mAgeValid = false;
                    }
                } catch (NumberFormatException nfe) {
                    mAgeLayout.setError(getString(R.string.register_exception_age_characters));
                    mAgeValid = false;
                }

                mAgeLayout.setErrorEnabled(!mAgeValid);
            }
        });

        mMaleBox = (RadioButton) findViewById(R.id.register_sex_male);
        mFemaleBox = (RadioButton) findViewById(R.id.register_sex_female);

        Spinner spinner = (Spinner) findViewById(R.id.register_education_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.education, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSharedPrefs.edit().putLong(Constants.SHARED_PREF_SETTINGS_EDUCATION, id).apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLanguagesText = (TextView) findViewById(R.id.register_languages_text);
        mLanguagesText.setOnClickListener(this);
        setLanguages();

        mStartCountryLayout = (LinearLayout) findViewById(R.id.register_start_country);
        mStartCountryLayout.setOnClickListener(this);
        mStartCountryText = (TextView) findViewById(R.id.register_start_country_text);
        mStartCountryIcon = (ImageView) findViewById(R.id.register_start_country_icon);

        String startCountryName = mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY, null);
        String startCountryCode = mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY_CODE, null);
        if(startCountryName != null && startCountryCode != null) {
            mStartCountryText.setText(startCountryName);
            // Load drawable dynamically from country code
            String drawableName = "flag_"
                    + startCountryCode.toLowerCase(Locale.ENGLISH);
            mStartCountryIcon.setImageResource(Util.getFlagResourceId(drawableName));
            mStartCountryIcon.setVisibility(View.VISIBLE);
        }

        mEndCountryLayout = (LinearLayout) findViewById(R.id.register_end_country);
        mEndCountryLayout.setOnClickListener(this);
        mEndCountryText = (TextView) findViewById(R.id.register_end_country_text);
        mEndCountryIcon = (ImageView) findViewById(R.id.register_end_country_icon);

        String endCountryName = mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY, null);
        String endCountryCode = mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY_CODE, null);
        if(endCountryName != null && endCountryCode != null) {
            mEndCountryText.setText(endCountryName);
            // Load drawable dynamically from country code
            String drawableName = "flag_"
                    + endCountryCode.toLowerCase(Locale.ENGLISH);
            mEndCountryIcon.setImageResource(Util.getFlagResourceId(drawableName));
            mEndCountryIcon.setVisibility(View.VISIBLE);
        }

        mEntourageLayout = (TextInputLayout) findViewById(R.id.register_entourage_layout);
        mEntourageText = (EditText) findViewById(R.id.register_entourage_text);

        mEntourageText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEntourageValid = true;
                try {
                    mEntourage = Integer.parseInt(mEntourageText.getText().toString().trim());
                    if(mEntourage < 0) {
                        mEntourageLayout.setError(getString(R.string.register_exception_entourage_invalid));
                        mEntourageValid = false;
                    }
                } catch (NumberFormatException nfe) {
                    mEntourageLayout.setError(getString(R.string.register_exception_entourage_characters));
                    mEntourageValid = false;
                }

                mEntourageLayout.setErrorEnabled(!mEntourageValid);
            }
        });

        mInfluxLayout = (TextInputLayout) findViewById(R.id.register_influx_layout);
        mInfluxText = (EditText) findViewById(R.id.register_influx_text);

        mInfluxText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mInfluxValid = true;
                try {
                    mInflux = Integer.parseInt(mInfluxText.getText().toString().trim());
                    if(mInflux < 0) {
                        mInfluxLayout.setError(getString(R.string.register_exception_influx_invalid));
                        mInfluxValid = false;
                    }
                } catch (NumberFormatException nfe) {
                    mInfluxLayout.setError(getString(R.string.register_exception_influx_characters));
                    mInfluxValid = false;
                }

                mInfluxLayout.setErrorEnabled(!mInfluxValid);
            }
        });

        mRadiusLayout = (TextInputLayout) findViewById(R.id.register_radius_layout);
        mRadiusText = (EditText) findViewById(R.id.register_radius_text);

        mRadiusText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mRadiusValid = true;
                try {
                    mRadius = Integer.parseInt(mRadiusText.getText().toString().trim());
                    if(mRadius < 10) {
                        mRadiusLayout.setError(getString(R.string.register_exception_radius_invalid));
                        mRadiusValid = false;
                    }
                } catch (NumberFormatException nfe) {
                    mRadiusLayout.setError(getString(R.string.register_exception_radius_characters));
                    mRadiusValid = false;
                }

                mRadiusLayout.setErrorEnabled(!mRadiusValid);
            }
        });

        boolean alreadySaved = mSharedPrefs.getBoolean(Constants.SHARED_PREF_SETTINGS_SAVED, false);

        int age = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_AGE, -1);
        if(age != -1) {
            mAgeText.setText(String.valueOf(age));
        }

        boolean isMale = mSharedPrefs.getBoolean(Constants.SHARED_PREF_SETTINGS_IS_MALE, false);
        if(alreadySaved) {
            mMaleBox.setChecked(isMale);
            mFemaleBox.setChecked(!isMale);
        }

        long educationId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_EDUCATION, -1);
        spinner.setSelection((int) educationId);

        int entourage = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_ENTOURAGE, -1);
        if(entourage != -1) {
            mEntourageText.setText(String.valueOf(entourage));
        }

        int influx = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_INFLUX, -1);
        if(influx != -1) {
            mInfluxText.setText(String.valueOf(influx));
        }

        int radius = mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_RADIUS, Constants.DEFAULT_RADIUS_IN_KM);
        mRadiusText.setText(String.valueOf(radius));

        IntentFilter statusIntentFilter = new IntentFilter();
        statusIntentFilter.addAction(Constants.BROADCAST_LANGUAGES_SELECTED);
        // Registers the ResponseReceiver and its intent filters
        LocalBroadcastManager.getInstance(this).registerReceiver(new ResponseReceiver(), statusIntentFilter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            // close the activity
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.register_start_country: {
                final CountryPicker picker = CountryPicker.newInstance(getString(R.string.register_start_country));
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                picker.setListener(new CountryPickerListener() {

                    @Override
                    public void onSelectCountry(String name, String code) {
                        Log.d(TAG, "Country: " + name + " (" + code + ")");
                        SharedPreferences.Editor editor = mSharedPrefs.edit();
                        editor.putString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY, name);
                        editor.putString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY_CODE, code);
                        editor.apply();


                        mStartCountryText.setText(name);
                        // Load drawable dynamically from country code
                        String drawableName = "flag_"
                                + code.toLowerCase(Locale.ENGLISH);
                        mStartCountryIcon.setImageResource(Util.getFlagResourceId(drawableName));
                        mStartCountryIcon.setVisibility(View.VISIBLE);
                        picker.dismiss();
                    }
                });
                break;
            }
            case R.id.register_end_country: {
                final CountryPicker picker = CountryPicker.newInstance(getString(R.string.register_end_country));
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                picker.setListener(new CountryPickerListener() {

                    @Override
                    public void onSelectCountry(String name, String code) {
                        Log.d(TAG, "Country: " + name + " (" + code + ")");
                        SharedPreferences.Editor editor = mSharedPrefs.edit();
                        editor.putString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY, name);
                        editor.putString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY_CODE, code);
                        editor.apply();

                        mEndCountryText.setText(name);
                        // Load drawable dynamically from country code
                        String drawableName = "flag_"
                                + code.toLowerCase(Locale.ENGLISH);
                        mEndCountryIcon.setImageResource(Util.getFlagResourceId(drawableName));
                        mEndCountryIcon.setVisibility(View.VISIBLE);
                        picker.dismiss();
                    }
                });
                break;
            }
            case R.id.register_languages_text: {
                LanguagePicker picker = new LanguagePicker();
                picker.show(getSupportFragmentManager(), "LANGUAGE_PICKER");

                break;
            }
            case R.id.register_save_button: {
                // save values
                try {
                    validateInputs();

                    IntentFilter filter = new IntentFilter("REQUEST_RESULT");
                    BroadcastReceiver requestReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            if (mIsInitialOpened) {
                                getNewsAndPois();

                                Intent mainIntent = new Intent(context, MainActivity.class);
                                startActivity(mainIntent);
                            }
                            unregisterReceiver(this);
                            finish();
                        }
                    };

                    ReFutureRestServiceHelper mReFutureRestServiceHelper = ReFutureRestServiceHelper.getInstance(this);
                    this.registerReceiver(requestReceiver, filter);
                    RequestBodyBuilder builder = new RequestBodyBuilder(this);
                    String body = builder.buildRegisterRefugeeRequestBody();
                    mReFutureRestServiceHelper.postRegisterRefugee(body.getBytes());
                } catch (IllegalArgumentException iae) {
                    Snackbar.make(findViewById(android.R.id.content), iae.getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }
    }

    /**
     * Gets the GCM Token.
     */
    private void getGCMToken() {
        if (Util.checkPlayServices(this, 9001)) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    /**
     * Validates all user inputs.
     * @throws IllegalArgumentException If an input value isn't entered or isn't entered correctly
     */
    private void validateInputs() throws IllegalArgumentException {
        if(!mRadiusValid || !mAgeValid || !mEntourageValid || !mInfluxValid) {
            return;
        }

        if(mAge == -1) {
            mAgeLayout.setError(getString(R.string.register_exception_age_invalid));
            mAgeLayout.setErrorEnabled(true);
            mAgeValid = false;
            return;
        }

        if(mEntourage == -1) {
            mEntourageLayout.setError(getString(R.string.register_exception_entourage_invalid));
            mEntourageLayout.setErrorEnabled(true);
            mEntourageValid = false;
            return;
        }

        if(mInflux == -1) {
            mInfluxLayout.setError(getString(R.string.register_exception_influx_invalid));
            mInfluxLayout.setErrorEnabled(true);
            mInfluxValid = false;
            return;
        }

        if(mRadius == -1) {
            mRadiusLayout.setError(getString(R.string.register_exception_radius_invalid));
            mRadiusLayout.setErrorEnabled(true);
            mRadiusValid = false;
            return;
        }

        if(!mMaleBox.isChecked() && !mFemaleBox.isChecked()) {
            throw new IllegalArgumentException(getString(R.string.register_exception_gender_invalid));
        } else {
            mIsMale = mMaleBox.isChecked();
        }

        if(mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_EDUCATION, -1) == -1) {
            throw new IllegalArgumentException(getString(R.string.register_exception_education_invalid));
        }

        if(!mSharedPrefs.getBoolean(Constants.SHARED_PREF_SETTINGS_LANGUAGES, false)) {
            throw new IllegalArgumentException(getString(R.string.register_exception_languages_invalid));
        }

        if(mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY, null) == null) {
            throw new IllegalArgumentException(getString(R.string.register_exception_start_country_invalid));
        }

        if(mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY, null) == null) {
            throw new IllegalArgumentException(getString(R.string.register_exception_end_country_invalid));
        }

        SharedPreferences.Editor editor = mSharedPrefs.edit();
        editor.putInt(Constants.SHARED_PREF_SETTINGS_AGE, mAge);
        editor.putInt(Constants.SHARED_PREF_SETTINGS_ENTOURAGE, mEntourage);
        editor.putInt(Constants.SHARED_PREF_SETTINGS_INFLUX, mInflux);
        editor.putInt(Constants.SHARED_PREF_SETTINGS_RADIUS, mRadius);
        editor.putBoolean(Constants.SHARED_PREF_SETTINGS_IS_MALE, mIsMale);
        editor.putBoolean(Constants.SHARED_PREF_SETTINGS_SAVED, true);
        editor.apply();
    }

    // Broadcast receiver for receiving status updates from the IntentService
    private class ResponseReceiver extends BroadcastReceiver {
        // Called when the BroadcastReceiver gets an Intent it's registered to receive
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equalsIgnoreCase(Constants.BROADCAST_LANGUAGES_SELECTED)) {
                Bundle bundle = intent.getExtras();
                SharedPreferences sharedPrefs = Util.getSharedPrefs(context);

                List<String> oldLanguages = bundle.getStringArrayList(Constants.BUNDLE_OLD_LANGUAGES);
                List<String> newLanguages = Util.getSetLanguages(context);

                Util.updateGCMSubscriptions(context, oldLanguages, newLanguages);
                setLanguages();
            }
        }
    }

    /**
     * Sets the user selected languages in the view
     */
    private void setLanguages() {
        boolean arabic = mSharedPrefs.getBoolean("ar", false);
        boolean english = mSharedPrefs.getBoolean("en", false);
        boolean french = mSharedPrefs.getBoolean("fr", false);
        boolean german = mSharedPrefs.getBoolean("de", false);

        StringBuilder sb = new StringBuilder();

        if(arabic) {
            sb.append("Arabic");
        }
        if(english) {
            if(sb.length() != 0) {
                sb.append(", ");
            }
            sb.append("English");
        }
        if(french) {
            if(sb.length() != 0) {
                sb.append(", ");
            }
            sb.append("French");
        }
        if(german) {
            if(sb.length() != 0) {
                sb.append(", ");
            }
            sb.append("German");
        }

        if(sb.length() != 0) {
            mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_SETTINGS_LANGUAGES, true).apply();
            mLanguagesText.setText(sb.toString());
        } else {
            // no language set
            mLanguagesText.setText(getString(R.string.register_languages));
            mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_SETTINGS_LANGUAGES, false).apply();
        }
    }

    /**
     * Request news and POIs from the server based on the user selected data
     */
    public void getNewsAndPois() {
        ReFutureRestServiceHelper serviceHelper = ReFutureRestServiceHelper.getInstance(this);
        RequestBodyBuilder builder = new RequestBodyBuilder(this);
        serviceHelper.postAllNews(builder.buildAllNewsRequestBody().getBytes());
        serviceHelper.postAllPois(builder.buildAllPoisRequestBody().getBytes());
    }

}
