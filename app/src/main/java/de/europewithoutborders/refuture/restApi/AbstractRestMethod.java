package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import de.europewithoutborders.refuture.restApi.resource.Resource;

/**
 * Abstract RestMethod class that prepares the HTTP URL & HTTP request body, executes
 * the HTTP transaction and processes the HTTP response.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public abstract class AbstractRestMethod<T extends Resource> implements RestMethod<T> {

    /**
     * Executes the request and returns the corresponding response.
     *
     * @return RestMethodResult
     */
    public RestMethodResult<T> execute() {
        Request request = buildRequest();
        Response response = doRequest(request);
        return buildResult(response);
    }

    protected abstract Context getContext();

    /**
     * Builds a result for a response from the web service.
     *
     * @param response
     * @return RestMethodResult
     */
    private RestMethodResult<T> buildResult(Response response) {
        int statusCode = response.getStatusCode();
        String statusMessage = "";
        String responseBody;
        T resource = null;

        try {
            responseBody = new String(response.getBody());
            resource = parseResponseBody(responseBody);
        } catch (Exception e) {
            statusCode = 500;
            statusMessage = e.getMessage();
        }

        return new RestMethodResult<>(statusCode, statusMessage, resource);
    }

    /**
     * Building the Request for a resource.
     *
     * @return Request
     */
    protected abstract Request buildRequest();

    /**
     * Parsing the body from the web service response.
     *
     * @param responseBody
     * @return A resource
     * @throws Exception
     */
    protected abstract T parseResponseBody(String responseBody) throws Exception;

    /**
     * Executes the HTTP request.
     *
     * @param request
     * @return Response
     */
    private Response doRequest(Request request) {
        RestClient client = new RestClient();
        return client.execute(request);
    }
}
