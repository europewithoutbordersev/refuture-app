package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.resource.Refugee;

/**
 * RestMethod for a HTTP POST request for register a refugee.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PostRegisterRefugeeRestMethod extends AbstractRestMethod<Refugee> {

    private final Context mContext;
    private static final String URL = "http://141.28.107.189:8080/ReFutureREST/rest/register";
    private final Map<String, List<String>> headers;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     * @param body
     */
    public PostRegisterRefugeeRestMethod(Context context, Map<String, List<String>> headers, byte[] body) {
        mContext = context.getApplicationContext();
        this.headers = headers;
        this.body = body;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP POST request for registering a refugee.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        return new Request(URL, RestMethodFactory.Method.POST, headers, body);
    }

    /**
     * Parsing the response body from the HTTP POST request for registering a refugee.
     *
     * @param responseBody
     * @return Refugee resource
     * @throws Exception
     */
    @Override
    protected Refugee parseResponseBody(String responseBody) throws Exception {
        if (responseBody.equals("")) {
            return new Refugee(new JSONObject());
        }
        JSONObject json = new JSONObject(responseBody);
        return new Refugee(json);
    }
}
