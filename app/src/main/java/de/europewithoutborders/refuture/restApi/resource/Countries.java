package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.europewithoutborders.refuture.util.Constants;

/**
 * Represents all countries with their properties, that we get from the web service response
 * from requesting all countries.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Countries implements Resource {

    // Map with all countries.
    private final Map<String, Map<String, String>> countries;

    /**
     * Countries Constructor that handles the JSON object it gets.
     *
     * @param countriesJson
     * @throws JSONException
     */
    public Countries(JSONObject countriesJson) throws JSONException {
        this.countries = new HashMap<>();
        JSONArray countriesArray = countriesJson.getJSONArray(Constants.JSON_COUNTRIES);
        for (int i = 0; i < countriesArray.length(); i++) {
            JSONObject country = countriesArray.getJSONObject(i);
            Map<String, String> countryLocalization = new HashMap<>();
            String countryCode = country.getString(Constants.JSON_COUNTRIES_COUNTRY_CODE);
            JSONArray localizationArray = country.getJSONArray(Constants.JSON_COUNTRIES_LOCALIZATIONS);
            for (int j = 0; j < localizationArray.length(); j++) {
                JSONObject localization = localizationArray.getJSONObject(j);
                countryLocalization.put(localization.getString(Constants.JSON_COUNTRIES_LOCALIZATIONS_LANG),
                        localization.getString(Constants.JSON_COUNTRIES_LOCALIZATIONS_NAME));
            }
            countries.put(countryCode, countryLocalization);
        }
    }

    public Map<String, Map<String, String>> getCountries() {
        return countries;
    }
}
