package de.europewithoutborders.refuture.restApi;

import de.europewithoutborders.refuture.restApi.resource.Resource;

/**
 * A RestMethod that executes a HTTP request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface RestMethod<T extends Resource> {

    /**
     * Execute a HTTP request.
     *
     * @return RestMethodResult
     */
    RestMethodResult<T> execute();
 }
