package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.resource.Position;

/**
 * RestMethod for a HTTP POST request for the position.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PostPositionRestMethod extends AbstractRestMethod<Position> {

    private final Context mContext;
    private final Map<String, List<String>> headers;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     * @param body
     */
    public PostPositionRestMethod(Context context, Map<String, List<String>> headers, byte[] body) {
        mContext = context.getApplicationContext();
        this.headers = headers;
        this.body = body;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP POST request for the position.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        String URL = "http://141.28.107.189:8080/ReFutureREST/rest/position";
        return new Request(URL, RestMethodFactory.Method.POST, headers, body);
    }

    /**
     * Currently no response from sending the position, so return a new empty Position.
     *
     * @param responseBody
     * @return Position
     * @throws Exception
     */
    @Override
    protected Position parseResponseBody(String responseBody) throws Exception {
        return new Position();
    }
}
