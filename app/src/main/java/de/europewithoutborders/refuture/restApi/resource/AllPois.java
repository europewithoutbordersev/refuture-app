package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.europewithoutborders.refuture.util.Constants;

/**
 * Represents all pois with their properties, that we get from the web service response
 * from requesting all pois.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class AllPois implements Resource {

    // List of all pois
    private final List<Poi> allPois;

    /**
     * AllPois Constructor that handles the JSON object it gets.
     *
     * @param allPoisJson
     * @throws JSONException
     */
    public AllPois(JSONObject allPoisJson) throws JSONException {
        allPois = new ArrayList<>();
        JSONArray poisArray = allPoisJson.getJSONArray(Constants.JSON_POI);

        // For every pois array in the json, create a poi resource and add it to the poi list
        for (int i = 0; i < poisArray.length(); i++) {
            JSONObject jsonPoi = poisArray.getJSONObject(i);
            allPois.add(new Poi(jsonPoi));
        }
    }

    public List<Poi> getAllPois() {
        return allPois;
    }
}
