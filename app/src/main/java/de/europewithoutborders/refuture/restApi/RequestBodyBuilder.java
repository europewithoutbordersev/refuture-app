package de.europewithoutborders.refuture.restApi;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Helper class to create a request body for the HTTP request calls.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RequestBodyBuilder {

    private static final String TAG = "RequestBodyBuilder";

    private final SharedPreferences mSharedPrefs;

    public RequestBodyBuilder(Context ctx) {
        this.mSharedPrefs = Util.getSharedPrefs(ctx);
    }

    /**
     * Builds a request body for a single news HTTP POST request. Contains the refugee id and
     * the wanted languages.
     *
     * @return requestBody
     */
    public String buildSingleNewsRequestBody() {
        String requestNewsBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);
        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("refugee_id", refugeeId);
            requestBody.putOpt("languages", getLanguages());
            requestNewsBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "build single news request body: " + requestNewsBody);
        return requestNewsBody;
    }

    /**
     * Builds a request body for a all news HTTP POST request. Contains the refugee id,
     * the wanted language and the timestamp of the last news that came in.
     *
     * @return requestBody
     */
    public String buildAllNewsRequestBody() {
        String requestNewsBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);
        long latestTimestamp = mSharedPrefs.getLong(Constants.SHARED_PREF_LATEST_NEWS_TIMESTAMP, 0);
        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("refugee_id", refugeeId);
            requestBody.putOpt("languages", getLanguages());
            requestBody.put("timestamp", latestTimestamp);
            requestNewsBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "build single news request body: " + requestNewsBody);
        return requestNewsBody;
    }

    /**
     * Builds a request body for a single poi HTTP POST request. Contains the refugee id and
     * the wanted languages.
     *
     * @return requestBody
     */
    public String buildSinglePoiRequestBody() {
        String requestPoiBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);
        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("refugee_id", refugeeId);
            requestBody.putOpt("languages", getLanguages());
            requestPoiBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "build poi request body: " + requestPoiBody);
        return requestPoiBody;
    }

    /**
     * Builds a request body for a all pois HTTP POST request. Contains the refugee id,
     * the wanted languages and the timestamp of the last poi that came in.
     *
     * @return requestBody
     */
    public String buildAllPoisRequestBody() {
        String requestPoiBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);
        long timestamp = mSharedPrefs.getLong(Constants.SHARED_PREF_LATEST_POI_TIMESTAMP, 0);
        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("refugee_id", refugeeId);
            requestBody.putOpt("languages", getLanguages());
            requestBody.put("timestamp", timestamp);
            requestPoiBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "build poi request body: " + requestPoiBody);
        return requestPoiBody;
    }

    /**
     * Builds a request body for registering a refugee HTTP POST request. Contains all the information
     * about the refugee.
     *
     * @return requestBody
     */
    public String buildRegisterRefugeeRequestBody() {
        String requestRegisterRefugeeBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);

        try {
            JSONObject requestBody = new JSONObject();
            if (refugeeId != 0) {
                requestBody.put("refugee_id", refugeeId);
            }
            requestBody.put("start_country_code", mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_START_COUNTRY_CODE, ""));
            requestBody.put("stop_country_code", mSharedPrefs.getString(Constants.SHARED_PREF_SETTINGS_END_COUNTRY_CODE, ""));
            requestBody.putOpt("languages", getLanguages());
            requestBody.put("influx", mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_INFLUX, 0));
            requestBody.put("entourage", mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_ENTOURAGE, 0));
            requestBody.put("age", mSharedPrefs.getInt(Constants.SHARED_PREF_SETTINGS_AGE, 0));
            requestBody.put("is_male", mSharedPrefs.getBoolean(Constants.SHARED_PREF_SETTINGS_IS_MALE, true));
            requestBody.put("educationId", mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_EDUCATION, 1));
            requestRegisterRefugeeBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "built register refugee body: " + requestRegisterRefugeeBody);
        return requestRegisterRefugeeBody;
    }

    /**
     * Builds a request body for a sending position HTTP POST request. Contains the refugee id and
     * the latitude and longitude.
     *
     * @return requestBody
     */
    public String buildPositionRequestBody(double latitude, double longitude) {
        String requestPositionBody = "";
        long refugeeId = mSharedPrefs.getLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, 0);
        try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("refugee_id", refugeeId);
            requestBody.putOpt("latitude", latitude);
            requestBody.put("longitude", longitude);
            requestPositionBody = requestBody.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "build position request body: " + requestPositionBody);
        return requestPositionBody;
    }


    /**
     * Gets all languages the user selected in the settings and returns them as a JSON array.
     *
     * @return json array with all languages
     * @throws JSONException
     */
    private JSONArray getLanguages() {
        JSONArray languages = new JSONArray();
        boolean arabic = mSharedPrefs.getBoolean("ar", false);
        boolean english = mSharedPrefs.getBoolean("en", false);
        boolean french = mSharedPrefs.getBoolean("fr", false);
        boolean german = mSharedPrefs.getBoolean("de", false);

        if (arabic) {
            languages.put("ar");
        }
        if (english) {
            languages.put("en");
        }
        if (french) {
            languages.put("fr");
        }
        if (german) {
            languages.put("de");
        }

        return languages;
    }
}
