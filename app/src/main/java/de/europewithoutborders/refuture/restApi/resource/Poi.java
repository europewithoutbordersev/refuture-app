package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.util.Constants;

/**
 * Represents a point of interest (poi) with its properties, that we get from the web service
 * response from requesting a single poi.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Poi implements Resource {

    // These attributes represent the values of a poi JSON object
    private int id;
    private double latitude;
    private double longitude;
    private int ratio = -1;
    private boolean createdByGovernment = false;
    private Map<Integer, List<String>> localizations;
    private String type;
    private Map<String, String> typeLocalizations;

    /**
     * Poi Constructor that handles the JSON object it gets.
     *
     * @param poiJson
     * @throws JSONException
     */
    public Poi(JSONObject poiJson) throws JSONException {
        this.id = poiJson.getInt(Constants.JSON_POI_ID);
        this.latitude = poiJson.getDouble(Constants.JSON_POI_LATITUDE);
        this.longitude = poiJson.getDouble(Constants.JSON_POI_LONGITUDE);

        // Check if ratio or createdByGovernment is in the JSON object. If yes, than save it
        // in the corresponding variables.
        if (poiJson.has(Constants.JSON_POI_RATIO))
            this.ratio = poiJson.getInt(Constants.JSON_POI_RATIO);
        if (poiJson.has(Constants.JSON_POI_CREATED_BY_GOVERNMENT))
            this.createdByGovernment = poiJson.getBoolean(Constants.JSON_POI_CREATED_BY_GOVERNMENT);

        this.localizations = new HashMap<>();
        JSONArray localizationArray = poiJson.getJSONArray(Constants.JSON_POI_LOCALIZATIONS);
        for (int i = 0; i < localizationArray.length(); i++) {
            JSONObject localization = localizationArray.getJSONObject(i);
            int id = localization.getInt(Constants.JSON_POI_LOCALIZATIONS_ID);
            List<String> poiDescription = new ArrayList<>();

            poiDescription.add(localization.getString(Constants.JSON_POI_LOCALIZATIONS_LANGUAGE));
            if (localization.has(Constants.JSON_POI_LOCALIZATIONS_NAME)) {
                poiDescription.add(localization.getString(Constants.JSON_POI_LOCALIZATIONS_NAME));
            } else {
                poiDescription.add("");
            }
            if (localization.has(Constants.JSON_POI_LOCALIZATIONS_DESCRIPTION)) {
                poiDescription.add(localization.getString(Constants.JSON_POI_LOCALIZATIONS_DESCRIPTION));
            } else {
                poiDescription.add("");
            }
            if (localization.has(Constants.JSON_POI_LOCALIZATIONS_CAPACITY)) {
                poiDescription.add(localization.getString(Constants.JSON_POI_LOCALIZATIONS_CAPACITY));
            } else {
                poiDescription.add("");
            }

            // Put the id as key and the poi description as value in the Map
            localizations.put(id, poiDescription);
        }

        this.type = poiJson.getString(Constants.JSON_POI_TYPE);
        this.typeLocalizations = new HashMap<>();
        JSONArray localizationTypeArray = poiJson.getJSONArray(Constants.JSON_POI_TYPE_LOCALIZATIONS);
        for (int i = 0; i < localizationTypeArray.length(); i++) {
            JSONObject localizationType = localizationTypeArray.getJSONObject(i);
            String language = localizationType.getString(Constants.JSON_POI_TYPE_LOCALIZATIONS_LANGUAGE);
            String name = "";
            if (localizationType.has(Constants.JSON_POI_TYPE_LOCALIZATIONS_NAME)) {
                name = localizationType.getString(Constants.JSON_POI_TYPE_LOCALIZATIONS_NAME);
            }

            // Put the language as key and the poi type name as value in the Map
            //typeLocalizations.put(language, poiTypeName);
            typeLocalizations.put(language, name);
        }
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getRatio() {
        return ratio;
    }

    public Map<Integer, List<String>> getLocalizations() {
        return localizations;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getTypeLocalizations() {
        return typeLocalizations;
    }

    public boolean getCreatedByGovernment() {
        return createdByGovernment;
    }
}
