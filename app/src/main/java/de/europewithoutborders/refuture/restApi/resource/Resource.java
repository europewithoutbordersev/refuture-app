package de.europewithoutborders.refuture.restApi.resource;

/**
 * Interface which represents a Resource
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface Resource {

}
