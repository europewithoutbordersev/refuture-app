package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.resource.Countries;

/**
 * RestMethod for a HTTP GET Request for all countries.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GetAllCountriesRestMethod extends AbstractRestMethod<Countries> {

    private final Context mContext;
    private final Map<String, List<String>> headers;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     */
    public GetAllCountriesRestMethod(Context context, Map<String, List<String>> headers) {
        mContext = context.getApplicationContext();
        this.headers = headers;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP GET request for all countries.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        String URL = "http://141.28.107.189:8080/ReFutureREST/rest/countries";
        return new Request(URL, RestMethodFactory.Method.GET, headers, null);
    }

    /**
     * Parsing the response body from the HTTP GET request for all countries.
     *
     * @param responseBody
     * @return Countries resource
     * @throws Exception
     */
    @Override
    protected Countries parseResponseBody(String responseBody) throws Exception {
        JSONObject json = new JSONObject(responseBody);
        return new Countries(json);
    }
}
