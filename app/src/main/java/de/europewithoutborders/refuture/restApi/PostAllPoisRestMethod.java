package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.resource.AllPois;

/**
 * RestMethod for a HTTP POST request for all pois.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PostAllPoisRestMethod extends AbstractRestMethod<AllPois> {

    private final Context mContext;
    private final Map<String, List<String>> headers;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     * @param body
     */
    public PostAllPoisRestMethod(Context context, Map<String, List<String>> headers, byte[] body) {
        mContext = context.getApplicationContext();
        this.headers = headers;
        this.body = body;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP POST request for all pois.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        String URL = "http://141.28.107.189:8080/ReFutureREST/rest/poi";
        return new Request(URL, RestMethodFactory.Method.POST, headers, body);
    }

    /**
     * Parsing the response body from the HTTP POST request for all pois.
     *
     * @param responseBody
     * @return AllPois resource
     * @throws Exception
     */
    @Override
    protected AllPois parseResponseBody(String responseBody) throws Exception {
        JSONObject json = new JSONObject(responseBody);
        return new AllPois(json);
    }
}
