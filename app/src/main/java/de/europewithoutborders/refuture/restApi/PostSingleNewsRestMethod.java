package de.europewithoutborders.refuture.restApi;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.RestMethodFactory.Method;
import de.europewithoutborders.refuture.restApi.resource.News;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * RestMethod for a HTTP POST Request for a single news.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PostSingleNewsRestMethod extends AbstractRestMethod<News> {

    private final Context mContext;
    private String url = "http://141.28.107.189:8080/ReFutureREST/rest/news/";
    private final Map<String, List<String>> headers;
    private final byte[] body;
    private final SharedPreferences mSharedPrefs;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     * @param body
     */
    public PostSingleNewsRestMethod(Context context, Map<String, List<String>> headers, byte[] body) {
        mContext = context.getApplicationContext();
        this.headers = headers;
        this.body = body;
        mSharedPrefs = Util.getSharedPrefs(mContext);
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP POST request for a single news.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        url = url + mSharedPrefs.getInt(Constants.SHARED_PREF_REST_ID_PARAMETER, 0);
        return new Request(url, Method.POST, headers, body);
    }

    /**
     * Parsing the response body from the HTTP POST request for a single news.
     *
     * @param responseBody
     * @return News resource
     * @throws Exception
     */
    @Override
    protected News parseResponseBody(String responseBody) throws Exception {
        // Create a JSON object that contains only the id, (lat), (lon), (countyCode), timestamp und localizations,
        // since the JSON object only contains one news
        JSONObject json = new JSONObject(responseBody).getJSONArray(Constants.JSON_NEWS).getJSONObject(0);
        return new News(json);
    }
}
