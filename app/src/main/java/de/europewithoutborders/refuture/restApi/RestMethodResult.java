package de.europewithoutborders.refuture.restApi;

import de.europewithoutborders.refuture.restApi.resource.Resource;

/**
 * Represents the result of a RestMethod.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RestMethodResult<T extends Resource> {

    // Tbe status code of the result
    private int statusCode = 0;

    // The status message of the result
    private final String statusMessage;

    // The resource of the result
    private final T resource;

    /**
     * Constructor
     *
     * @param statusCode
     * @param statusMessage
     * @param resource
     */
    public RestMethodResult(int statusCode, String statusMessage, T resource) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.resource = resource;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public T getResource() {
        return resource;
    }
}
