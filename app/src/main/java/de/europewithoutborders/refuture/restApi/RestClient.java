package de.europewithoutborders.refuture.restApi;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Responsible for executing the HTTP Request and handling the response.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RestClient {

    private static final String TAG = "RestClient";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String APPLICATION_JSON = "application/json";

    /**
     * Executes the HTTP request.
     *
     * @param request
     * @return Response
     */
    public Response execute(Request request) {
        HttpURLConnection conn = null;
        Response response = null;
        int statusCode;

        try {
            URL url = new URL(request.getUrl());

            // Opens a http url connection
            conn = (HttpURLConnection) url.openConnection();

            // https://code.google.com/p/android/issues/detail?id=24672 still not fixed, Google is lying :(
            conn.setRequestProperty("Accept-Encoding", "");

            if (request.getHeaders() != null) {
                for (String header : request.getHeaders().keySet()) {
                    for (String value : request.getHeaders().get(header)) {
                        conn.addRequestProperty(header, value);
                    }
                }
            }

            // Sets properties to the connection
            switch (request.getMethod()) {
                case GET:
                    conn.setDoOutput(false);
                    break;
                case POST:
                    byte[] payload = request.getBody();
                    conn.setDoOutput(true);
                    conn.setRequestProperty(CONTENT_TYPE, APPLICATION_JSON);
                    conn.getOutputStream().write(payload);
                    break;
                default:
                    break;
            }
            
            statusCode = conn.getResponseCode();
            Log.d(TAG, "status code: " + statusCode);

            // Read the input stream if the request was successful
            if (statusCode == 200) {
                BufferedInputStream in = new BufferedInputStream(conn.getInputStream());
                byte[] body = readStream(in);
                response = new Response(conn.getResponseCode(), conn.getHeaderFields(), body);
            } else {
                response = new Response(conn.getResponseCode(), conn.getHeaderFields(), new byte[]{});
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        return response;
    }

    /**
     * Reads the input stream and returns the content.
     *
     * @param in
     * @return response body
     * @throws IOException
     */
    private byte[] readStream(BufferedInputStream in) throws IOException {
        byte[] buf = new byte[1024];
        int count;
        ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
        while ((count = in.read(buf)) != -1) {
            out.write(buf, 0, count);
        }
        return out.toByteArray();
    }
}
