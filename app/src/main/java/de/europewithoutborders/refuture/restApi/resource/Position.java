package de.europewithoutborders.refuture.restApi.resource;

/**
 * Represents a position - currently we don't get any response corresponding the position
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Position implements Resource {

    /**
     * Position constructor
     */
    public Position() {

    }
}
