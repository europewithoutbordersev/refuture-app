package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import java.util.List;
import java.util.Map;

/**
 * Base class for RestMethodFactory that handle creation and access of the RestMethod's as Singleton.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RestMethodFactory {

    private static RestMethodFactory instance;
    private static final Object lock = new Object();
    private final Context mContext;

    // Types of REST calls
    public static final int SINGLE_NEWS = 1;
    public static final int SINGLE_POI = 2;
    public static final int ALL_NEWS = 3;
    public static final int ALL_POIS = 4;
    public static final int REGISTER_REFUGEE = 5;
    public static final int POSITION = 6;
    public static final int COUNTRIES = 7;

    /**
     * private Constructor
     * @param context
     */
    private RestMethodFactory(Context context) {
        mContext = context.getApplicationContext();
    }

    public static RestMethodFactory getInstance(Context context) {
        synchronized (lock) {
            if (instance == null) {
                instance = new RestMethodFactory(context);
            }
        }

        return instance;
    }

    /**
     * Returns the corresponding RestMethod object for the REST call type
     *
     * @param type
     * @param method
     * @param headers
     * @param body
     * @return RestMethod
     */
    public RestMethod getRestMethod(int type, Method method, Map<String, List<String>> headers, byte[] body) {
        switch (type) {
            case SINGLE_NEWS:
                if (method == Method.POST) {
                    return new PostSingleNewsRestMethod(mContext, headers, body);
                }
                break;
            case ALL_NEWS:
                if (method == Method.POST) {
                    return new PostAllNewsRestMethod(mContext, headers, body);
                }
                break;
            case SINGLE_POI:
                if (method == Method.POST) {
                    return new PostSinglePoiRestMethod(mContext, headers, body);
                }
                break;
            case ALL_POIS:
                if (method == Method.POST) {
                    return new PostAllPoisRestMethod(mContext, headers, body);
                }
                break;
            case REGISTER_REFUGEE:
                if (method == Method.POST) {
                    return new PostRegisterRefugeeRestMethod(mContext, headers, body);
                }
                break;
            case POSITION:
                if (method == Method.POST) {
                    return new PostPositionRestMethod(mContext, headers, body);
                }
                break;
            case COUNTRIES:
                if (method == Method.GET) {
                    return new GetAllCountriesRestMethod(mContext, headers);
                }
                break;
        }

        return null;
    }

    /**
     * Enum representing the HTTP methods GET, POST, PUT and DELETE.
     */
    public enum Method {
        GET, POST, PUT, DELETE
    }
}
