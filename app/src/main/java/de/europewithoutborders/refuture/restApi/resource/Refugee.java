package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;

import de.europewithoutborders.refuture.util.Constants;


/**
 * Represents a refugee with its properties, that we get from the web service response
 * from registering a refugee.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Refugee implements Resource {

    // ID that is used for every request to identify a refugee
    private long refugeeId;

    /**
     * Refugee Constructor that handles the JSON object it gets.
     *
     * @param refugeeJson
     * @throws JSONException
     */
    public Refugee(JSONObject refugeeJson) throws JSONException {
        if (refugeeJson.has(Constants.JSON_REGISTER_REFUGEE_ID)) {
            this.refugeeId = refugeeJson.getLong(Constants.JSON_REGISTER_REFUGEE_ID);
        }
    }

    public long getRefugeeId() {
        return refugeeId;
    }
}
