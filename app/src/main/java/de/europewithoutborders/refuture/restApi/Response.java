package de.europewithoutborders.refuture.restApi;

import java.util.List;
import java.util.Map;

/**
 * Represents a HTTP response.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Response {

    // The HTTP status code
    private final int statusCode;

    // The HTTP headers received in the response
    private final Map<String, List<String>> headers;

    // The response body, if any
    private final byte[] body;

    /**
     * Constructor
     *
     * @param statusCode
     * @param headers
     * @param body
     */
    protected Response(int statusCode, Map<String, List<String>> headers, byte[] body) {
        this.statusCode = statusCode;
        this.headers = headers;
        this.body = body;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public byte[] getBody() {
        return body;
    }
}
