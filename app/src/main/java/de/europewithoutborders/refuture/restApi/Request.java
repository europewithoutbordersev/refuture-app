package de.europewithoutborders.refuture.restApi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.RestMethodFactory.Method;

/**
 * Represents a HTTP request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Request {

    // The url the request is sent
    private final String url;

    // The HTTP headers in the request
    private Map<String, List<String>> headers;

    // The request body, if any
    private final byte[] body;

    // The HTTP Method
    private final Method method;

    /**
     * Constructor
     *
     * @param url
     * @param method
     * @param headers
     * @param body
     */
    public Request(String url, Method method, Map<String, List<String>> headers, byte[] body) {
        super();
        this.url = url;
        this.method = method;
        this.headers = headers;
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public byte[] getBody() {
        return body;
    }

    public Method getMethod() {
        return method;
    }

    /**
     * Adds parameter to the request header.
     *
     * @param key
     * @param value
     */
    public void addHeader(String key, List<String> value) {
        if (headers == null) {
            headers = new HashMap<>();
        } else {
            headers.put(key, value);
        }
    }
}
