package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.util.Constants;

/**
 * Represents a news with its properties, that we get from the web service response
 * from requesting a single news.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class News implements Resource {

    // These attributes represent the values of a news JSON object.
    private int id;
    private double latitude;
    private double longitude;
    private String countryCode;
    private long timestamp;
    private Map<Integer, List<String>> localizations;

    // The news type indicates whether the news is global, location, or country based.
    private NewsType newsType;

    /**
     * News Constructor that handles the JSON object it gets.
     *
     * @param newsJson
     * @throws JSONException
     */
    public News(JSONObject newsJson) throws JSONException {
        // These attributes are always in the JSON object
        this.id = newsJson.getInt(Constants.JSON_NEWS_ID);
        this.timestamp = Long.parseLong(newsJson.getString(Constants.JSON_NEWS_TIMESTAMP));
        this.localizations = new HashMap<>();
        JSONArray localizationArray = newsJson.getJSONArray(Constants.JSON_NEWS_LOCALIZATIONS);
        for (int i = 0; i < localizationArray.length(); i++) {
            JSONObject localization = localizationArray.getJSONObject(i);
            int id = localization.getInt(Constants.JSON_NEWS_LOCALIZATIONS_ID);
            List<String> message = new ArrayList<>();

            // Add language, heading and body of the localized message in the ArrayList
            message.add(localization.getString(Constants.JSON_NEWS_LOCALIZATIONS_LANGUAGE));
            message.add(localization.getString(Constants.JSON_NEWS_LOCALIZATIONS_HEADING));
            message.add(localization.getString(Constants.JSON_NEWS_LOCALIZATIONS_BODY));

            // Put the id as key and the localized message as value in the Map
            localizations.put(id, message);
        }

        // Check if latitude and longitude or country_code is in the JSON object. If so, get the value and
        // save it in the corresponding variable(s).
        if (newsJson.has(Constants.JSON_NEWS_LATITUDE) && newsJson.has(Constants.JSON_NEWS_LONGITUDE)) {
            this.latitude = newsJson.getDouble(Constants.JSON_NEWS_LATITUDE);
            this.longitude = newsJson.getDouble(Constants.JSON_NEWS_LONGITUDE);
            this.newsType = NewsType.LOCATION_NEWS;
        } else if (newsJson.has(Constants.JSON_NEWS_COUNTRY_CODE)) {
            this.countryCode = newsJson.getString(Constants.JSON_NEWS_COUNTRY_CODE).toLowerCase();
            this.newsType = NewsType.COUNTRY_NEWS;
        } else {
            this.newsType = NewsType.GLOBAL_NEWS;
        }
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Map<Integer, List<String>> getLocalizations() {
        return localizations;
    }

    public NewsType getNewsType() {
        return newsType;
    }

    /**
     * Enum represents the three different types of news:
     *  - global news: news that were created for every user
     *  - country news: news that were created for a specific country
     *  - location news: news that were created for a specific location
     */
    public enum NewsType {
        GLOBAL_NEWS, COUNTRY_NEWS, LOCATION_NEWS
    }
}
