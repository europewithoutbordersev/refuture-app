package de.europewithoutborders.refuture.restApi;

import android.content.Context;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.restApi.resource.AllNews;

/**
 * RestMethod for a HTTP POST for all news.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PostAllNewsRestMethod extends AbstractRestMethod<AllNews> {

    private final Context mContext;
    private final Map<String, List<String>> headers;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param headers
     * @param body
     */
    public PostAllNewsRestMethod(Context context, Map<String, List<String>> headers, byte[] body) {
        mContext = context.getApplicationContext();
        this.headers = headers;
        this.body = body;
    }

    @Override
    protected Context getContext() {
        return mContext;
    }

    /**
     * Builds the HTTP POST request for all news.
     *
     * @return Request
     */
    @Override
    protected Request buildRequest() {
        String URL = "http://141.28.107.189:8080/ReFutureREST/rest/news";
        return new Request(URL, RestMethodFactory.Method.POST, headers, body);
    }

    /**
     * Parsing the response body from the HTTP POST request for all news.
     *
     * @param responseBody
     * @return AllNews resource
     * @throws Exception
     */
    @Override
    protected AllNews parseResponseBody(String responseBody) throws Exception {
        JSONObject json = new JSONObject(responseBody);
        return new AllNews(json);
    }
}
