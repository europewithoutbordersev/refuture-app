package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.europewithoutborders.refuture.util.Constants;

/**
 * Represents all news with their properties, that we get from the web service response
 * from requesting all news.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class AllNews implements Resource {

    // List of all news
    private final List<News> allNews;

    /**
     * AllNews Constructor that handles the JSON object it gets.
     *
     * @param allNewsJson
     * @throws JSONException
     */
    public AllNews(JSONObject allNewsJson) throws JSONException {
        allNews = new ArrayList<>();
        JSONArray newsArray = allNewsJson.getJSONArray(Constants.JSON_NEWS);

        // For every news array in the json, create a news resource and add it to the news list
        for (int i = 0; i < newsArray.length(); i++) {
            JSONObject jsonNews = newsArray.getJSONObject(i);
            allNews.add(new News(jsonNews));
        }
    }

    public List<News> getAllNews() {
        return allNews;
    }
}
