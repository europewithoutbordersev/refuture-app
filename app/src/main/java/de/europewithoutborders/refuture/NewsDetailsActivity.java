package de.europewithoutborders.refuture;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import de.europewithoutborders.refuture.data.ReFutureContract;

/**
 * The NewsDetailsActivity displays the title and text of a single news in a specific language.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsDetailsActivity extends AppCompatActivity {

    private static final String CHARSET_NAME = "UTF-8";
    private static final String TAG = "NewsDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        Intent myIntent = getIntent();
        int newsId = myIntent.getIntExtra("newsId", 0);
        Log.d(TAG, "newsId: " + newsId);
        String lang = myIntent.getStringExtra("lang");

        Cursor newsDetailsCursor = getNewsDetails(newsId, lang);

        if (newsDetailsCursor != null) {
            if (newsDetailsCursor.moveToFirst()) {
                String title = newsDetailsCursor.getString(newsDetailsCursor.getColumnIndexOrThrow(ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING));
                String body = newsDetailsCursor.getString(newsDetailsCursor.getColumnIndexOrThrow(ReFutureContract.NewsLocalizationEntry.COLUMN_BODY));

                TextView titleTV = (TextView) findViewById(R.id.newsDetailsTitle);
                TextView bodyTV = (TextView) findViewById(R.id.newsDetailsBody);

                try {
                    titleTV.setText(URLDecoder.decode(title, CHARSET_NAME));
                    bodyTV.setText(URLDecoder.decode(body, CHARSET_NAME));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Cursor getNewsDetails(int newsId, String lang) {
        Uri newsUri = ReFutureContract.NewsEntry.CONTENT_URI;
        String[] projection = {ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_ID, ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING, ReFutureContract.NewsLocalizationEntry.COLUMN_BODY};
        String selection = ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_ID + " = ? AND " +
                ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_LANGUAGE + " = ?";
        String[] selectionArgs = {String.valueOf(newsId), lang};
        Cursor cursor = getContentResolver().query(newsUri, projection, selection, selectionArgs, null);
        return cursor;
    }
}
