package de.europewithoutborders.refuture;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.europewithoutborders.refuture.model.Language;
import de.europewithoutborders.refuture.util.Util;

/**
 * Adapter to show language selection.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class LanguageListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Language> mLanguages;
    private LayoutInflater mInflater;
    private SharedPreferences mSharedPrefs;

    public LanguageListAdapter(Context context) {
        super();
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSharedPrefs = Util.getSharedPrefs(mContext);

        mLanguages = getSupportedLanguages();
    }

    /**
     * Creates {@link Language} objects and determines whether the user has them checked
     *
     * @return {@link List} of all {@link Language}s
     */
    private List<Language> getSupportedLanguages() {
        List<Language> languages = new ArrayList<>();

        Language german = new Language("German", "de", "de");
        Language english = new Language("English", "gb", "en");
        Language french = new Language("French", "fr", "fr");
        Language arabic = new Language("Arabic", "ae", "ar");

        german.setIsChecked(mSharedPrefs.getBoolean("de", false));
        english.setIsChecked(mSharedPrefs.getBoolean("en", false));
        french.setIsChecked(mSharedPrefs.getBoolean("fr", false));
        arabic.setIsChecked(mSharedPrefs.getBoolean("ar", false));

        languages.add(german);
        languages.add(english);
        languages.add(french);
        languages.add(arabic);

        return languages;
    }

    public List<Language> getLanguages() {
        return mLanguages;
    }

    @Override
    public int getCount() {
        return mLanguages.size();
    }

    @Override
    public Object getItem(int position) {
        return mLanguages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // old view, which may be recycled. can be null
        View cellView = convertView;
        // ViewHolder to hold information about old views. No need to find data in them
        final ViewHolder viewHolder;
        // the language to show in this row
        final Language language = mLanguages.get(position);

        // if the view isn't about to be recycled, create a new one
        if (convertView == null) {
            viewHolder = new ViewHolder();
            cellView = mInflater.inflate(R.layout.row_language, null);
            viewHolder.textView = (TextView) cellView.findViewById(R.id.row_title);
            viewHolder.imageView = (ImageView) cellView.findViewById(R.id.row_icon);
            viewHolder.checkBox = (CheckBox) cellView.findViewById(R.id.row_checkbox);
            cellView.setTag(viewHolder);
        } else {
            // get the ViewHolder from the view's tag
            viewHolder = (ViewHolder) cellView.getTag();
        }

        // set the language label
        viewHolder.textView.setText(language.getLanguage());

        // Load drawable dynamically from country code
        String drawableName = "flag_"
                + language.getFlagCode().toLowerCase(Locale.ENGLISH);
        // set the flag
        viewHolder.imageView.setImageResource(Util.getFlagResourceId(drawableName));

        // set the checkbox to (un)checked
        viewHolder.checkBox.setChecked(language.isChecked());

        // set an onClickListener to the checkbox
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkToggle(viewHolder.checkBox, false, language);
            }
        });

        // set an onClickListener to the whole row
        cellView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkToggle(viewHolder.checkBox, true, language);
            }
        });
        return cellView;
    }

    /**
     * Triggers an onClickEvent to update the {@link CheckBox} and {@link Language} appropriately.
     * @param checkBox The Checkbox to (un)check
     * @param changeCheckStatus {@code True}, if the view is clicked and the status of the Checkbox need to be triggered. {@code False} if the Checkbox is already triggered.
     * @param language The {@link Language} to contain the changes
     */
    private void checkToggle(CheckBox checkBox, boolean changeCheckStatus, Language language) {
        if(changeCheckStatus) {
            checkBox.setChecked(!checkBox.isChecked());
        }
        language.setIsChecked(checkBox.isChecked());
    }

    /**
     * Holder for the cell
     */
    static class ViewHolder {
        /**
         * The language label
         */
        public TextView textView;

        /**
         * The image for the flag
         */
        public ImageView imageView;

        /**
         * The {@link CheckBox} to determine whether the user understands this language
         */
        public CheckBox checkBox;
    }
}
