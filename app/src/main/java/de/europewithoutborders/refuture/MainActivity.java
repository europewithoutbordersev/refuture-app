package de.europewithoutborders.refuture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import de.europewithoutborders.refuture.restApi.RequestBodyBuilder;
import de.europewithoutborders.refuture.service.ReFutureRestServiceHelper;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Activity to open when the App starts.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";
    /**
     * Ratio of the logo
     */
    private static float sImageRatio = 2 / 7f;

    /**
     * Broadcast-Receiver for GPS-Trigger events
     */
    private static GpsTriggerReceiver sGpsTriggerReceiver;

    /**
     * Service to start / stop on GPS triggers
     */
    private static Intent sTrackingService;

    /**
     * Privte Shared Preferences
     */
    private SharedPreferences mSharedPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // show the layout
        setContentView(R.layout.activity_main);
        // set the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        mSharedPrefs = Util.getSharedPrefs(this);

        // check if it's the first start of the app
        if (!mSharedPrefs.getBoolean(Constants.SHARED_PREF_SETTINGS_SAVED, false)) {
            // first start detected
            getCountriesInformation();

            Intent intent = new Intent(this, SettingsActivity.class);
            intent.putExtra(Constants.SETTINGS_CALL_DATA, Constants.SETTINGS_INITIAL_CALL);
            startActivity(intent);
            finish();
        } else {
            // app was already configured
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            ImageView logoView = (ImageView) findViewById(R.id.main_logo_imageview);
            // set logo ratio
            logoView.getLayoutParams().height = (int) (displayMetrics.heightPixels * sImageRatio);

            // set OnClickListeners
            ImageButton refreshButton = (ImageButton) findViewById(R.id.main_refresh_button);
            refreshButton.setOnClickListener(this);

            Button mapButton = (Button) findViewById(R.id.main_show_map_button);
            mapButton.setOnClickListener(this);

            Button newsButton = (Button) findViewById(R.id.main_show_news_button);
            newsButton.setOnClickListener(this);

            Button settingsButton = (Button) findViewById(R.id.main_show_settings_button);
            settingsButton.setOnClickListener(this);

            Button aboutUsButton = (Button) findViewById(R.id.main_show_about_us_button);
            aboutUsButton.setOnClickListener(this);

            // create a static Trigger Receiver for GPS-Events
            sGpsTriggerReceiver = new GpsTriggerReceiver();
            // create the GPS Tracking Service
            sTrackingService = new Intent(this, GPSTrackingService.class);

            // register the GPS Trigger Receiver
            getApplicationContext().registerReceiver(sGpsTriggerReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

            triggerGPSService();
        }
    }

    /**
     * Start or stops the GPS Tracking Service, based on GPS availability.
     */
    private void triggerGPSService() {
        if (Util.isGPSEnabled(this)) {
            startService(sTrackingService);
        } else {
            stopService(sTrackingService);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_refresh_button: {
                // refresh data
                getNewsAndPois();
                break;
            }
            case R.id.main_show_map_button: {
                // show map
                Intent intent = new Intent(this, MapLocationActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.main_show_news_button: {
                // show news
                Intent intent = new Intent(this, NewsActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.main_show_settings_button : {
                // show settings
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.putExtra(Constants.SETTINGS_CALL_DATA, Constants.SETTINGS_NOT_INITIAL_CALL);
                startActivity(intent);
                break;
            }
            case R.id.main_show_about_us_button : {
                // show about us
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    /**
     * Download Country Data
     */
    public void getCountriesInformation() {
        ReFutureRestServiceHelper mReFutureRestServiceHelper = ReFutureRestServiceHelper.getInstance(this);
        mReFutureRestServiceHelper.getCountries();
    }

    /**
     * Download News and POIs
     */
    public void getNewsAndPois() {
        ReFutureRestServiceHelper serviceHelper = ReFutureRestServiceHelper.getInstance(this);
        RequestBodyBuilder builder = new RequestBodyBuilder(this);
        serviceHelper.postAllNews(builder.buildAllNewsRequestBody().getBytes());
        serviceHelper.postAllPois(builder.buildAllPoisRequestBody().getBytes());
    }

    /**
     * Inner Class to react on GPS Trigger events
     */
    public class GpsTriggerReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            // if GPS availability changes, trigger the service
            if(intent.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)){
                triggerGPSService();
            }
        }
    }
}