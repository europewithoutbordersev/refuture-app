package de.europewithoutborders.refuture.service;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.util.Map;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.restApi.RestMethod;
import de.europewithoutborders.refuture.restApi.RestMethodFactory;
import de.europewithoutborders.refuture.restApi.RestMethodResult;
import de.europewithoutborders.refuture.restApi.resource.Countries;

/**
 * Processor class for countries that handles the request call via ReFutureRestService. It manages the pre-process and
 * post-progress logic for the countries request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class CountriesProcessor {

    private static final String TAG = "CountriesProcessor";
    private final Context mContext;

    /**
     * Constructor
     *
     * @param context
     */
    public CountriesProcessor(Context context) {
        mContext = context;
    }

    /**
     * For the countries request it gets the countries request, let it executed, updates the ContentProvider and
     * sends a callback.
     *
     * @param callback
     */
    void postCountries(CountriesProcessorCallback callback) {
        @SuppressWarnings("unchecked") RestMethod<Countries> postCountriesMethod = RestMethodFactory.getInstance(mContext).getRestMethod(
                RestMethodFactory.COUNTRIES, RestMethodFactory.Method.GET, null, null
        );
        RestMethodResult<Countries> result = postCountriesMethod.execute();

        updateContentProvider(result);

        callback.send(result.getStatusCode());
    }

    /**
     * Creates a counties resource and saves the data in the database.
     *
     * @param result
     */
    private void updateContentProvider(RestMethodResult<Countries> result) {
        Countries countries = result.getResource();
        Map<String, Map<String, String>> allCountries = countries.getCountries();
        for (Map.Entry<String, Map<String, String>> entry : allCountries.entrySet()) {
            Uri countryUri = ReFutureContract.CountryEntry.CONTENT_URI;
            ContentValues countryValues = new ContentValues();
            countryValues.put(ReFutureContract.CountryEntry.COLUMN_CODE, entry.getKey().toLowerCase());
            countryValues.put(ReFutureContract.CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, 1);
            mContext.getContentResolver().insert(countryUri, countryValues);

            Log.d(TAG, "country code: " + entry.getKey());
            for (Map.Entry<String, String> entry1 : entry.getValue().entrySet()) {
                Uri countryLocalizationUri = ReFutureContract.CountryLocalizationEntry.CONTENT_URI;
                ContentValues countryLocalizationValues = new ContentValues();
                countryLocalizationValues.put(ReFutureContract.CountryLocalizationEntry.COLUMN_LANGUAGE, entry1.getKey());
                countryLocalizationValues.put(ReFutureContract.CountryLocalizationEntry.COLUMN_NAME, entry1.getValue());
                countryLocalizationValues.put(ReFutureContract.CountryLocalizationEntry.COLUMN_COUNTRY_CODE, entry.getKey().toLowerCase());
                mContext.getContentResolver().insert(countryLocalizationUri, countryLocalizationValues);

                Log.d(TAG, "    language: " + entry1.getKey());
                Log.d(TAG, "        name: " + entry1.getValue());
            }
            Log.d(TAG, "-----------------------");
        }
    }
}
