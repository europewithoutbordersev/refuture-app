package de.europewithoutborders.refuture.service;

/**
 * Callback for the RegisterRefugeeProcessor.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface RegisterRefugeeProcessorCallback {

    /**
     * Sends a callback with the resultCode.
     *
     * @param resultCode
     */
    void send(int resultCode);
}
