package de.europewithoutborders.refuture.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

/**
 * Service Class which calls the Processors for the different REST calls.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureRestService extends IntentService {

    private static final String TAG = "ReFutureRestService";

    public static final String METHOD_EXTRA = "METHOD_EXTRA";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String RESOURCE_TYPE_EXTRA = "RESOURCE_TYPE_EXTRA";

    // Resource types
    public static final int RESOURCE_TYPE_SINGLE_NEWS = 1;
    public static final int RESOURCE_TYPE_ALL_NEWS = 2;
    public static final int RESOURCE_TYPE_SINGLE_POI = 3;
    public static final int RESOURCE_TYPE_ALL_POIS = 4;
    public static final int RESOURCE_TYPE_REGISTER_REFUGEE = 5;
    public static final int RESOURCE_TYPE_POSITION = 6;
    public static final int RESOURCE_TYPE_ALL_COUNTRIES = 7;

    public static final String SERVICE_CALLBACK = "SERVICE_CALLBACK";
    public static final String ORIGINAL_INTENT_EXTRA = "ORIGINAL_INTENT_EXTRA";
    public static final String BODY_EXTRA = "BODY_EXTRA";
    private static final int REQUEST_INVALID = -1;

    private ResultReceiver mCallback;
    private Intent mOriginalRequestIntent;

    /**
     * Constructor
     */
    public ReFutureRestService() {
        super("ReFutureRestService");
    }

    /**
     * Receives the intent and starts the request by calling the corresponding Processor.
     * @param requestIntent
     */
    @Override
    protected void onHandleIntent(Intent requestIntent) {
        mOriginalRequestIntent = requestIntent;

        // Get request data from intent
        String method = requestIntent.getStringExtra(ReFutureRestService.METHOD_EXTRA);
        int resourceType = requestIntent.getIntExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, -1);
        mCallback = requestIntent.getParcelableExtra(ReFutureRestService.SERVICE_CALLBACK);
        byte[] body = requestIntent.getByteArrayExtra(ReFutureRestService.BODY_EXTRA);

        Log.d(TAG, "rest method: " + method);
        Log.d(TAG, "rest resourceType: " + resourceType);
        Log.d(TAG, "rest callback: " + mCallback);

        switch (resourceType) {
            case RESOURCE_TYPE_SINGLE_NEWS:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    SingleNewsProcessor processor = new SingleNewsProcessor(getApplicationContext(), body);
                    processor.postSingleNews(makeSingleNewsProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_ALL_NEWS:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    AllNewsProcessor processor = new AllNewsProcessor(getApplicationContext(), body);
                    processor.postAllNews(makeAllNewsProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_SINGLE_POI:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    SinglePoiProcessor processor = new SinglePoiProcessor(getApplicationContext(), body);
                    processor.postSinglePoi(makeSinglePoiProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_ALL_POIS:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    AllPoisProcessor processor = new AllPoisProcessor(getApplicationContext(), body);
                    processor.postAllPois(makeAllPoisProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_REGISTER_REFUGEE:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    RegisterRefugeeProcessor processor = new RegisterRefugeeProcessor(getApplicationContext(), body);
                    processor.postRegisterRefugee(makeRegisterRefugeeProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_POSITION:
                if (method.equalsIgnoreCase(METHOD_POST)) {
                    PositionProcessor processor = new PositionProcessor(getApplicationContext(), body);
                    processor.postPosition(makePositionProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
                break;
            case RESOURCE_TYPE_ALL_COUNTRIES:
                if (method.equalsIgnoreCase(METHOD_GET)) {
                    CountriesProcessor processor = new CountriesProcessor(getApplicationContext());
                    processor.postCountries(makeCountriesProcessorCallback());
                } else {
                    mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                }
            default:
                mCallback.send(REQUEST_INVALID, getOriginalIntentBundle());
                break;
        }
    }

    /**
     * Handles the binder callback fired by the SingleNewsProcessor.
     *
     * @return SingleNewsProcessorCallback
     */
    private SingleNewsProcessorCallback makeSingleNewsProcessorCallback() {

        return new SingleNewsProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the AllNewsProcessor.
     *
     * @return AllNewsProcessorCallback
     */
    private AllNewsProcessorCallback makeAllNewsProcessorCallback() {

        return new AllNewsProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the SinglePoiProcessor.
     *
     * @return SinglePoiProcessorCallback
     */
    private SinglePoiProcessorCallback makeSinglePoiProcessorCallback() {

        return new SinglePoiProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the AllPoisProcessor.
     *
     * @return AllPoisProcessorCallback
     */
    private AllPoisProcessorCallback makeAllPoisProcessorCallback() {

        return new AllPoisProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the RegisterRefugeeProcessor.
     *
     * @return RegisterRefugeeProcessorCallback
     */
    private RegisterRefugeeProcessorCallback makeRegisterRefugeeProcessorCallback() {

        return new RegisterRefugeeProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the CountriesProcessor.
     *
     * @return CountriesProcessorCallback
     */
    private CountriesProcessorCallback makeCountriesProcessorCallback() {

        return new CountriesProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Handles the binder callback fired by the PositionProcessor.
     *
     * @return PositionProcessorCallback
     */
    private PositionProcessorCallback makePositionProcessorCallback() {

        return new PositionProcessorCallback() {
            @Override
            public void send(int resultCode) {
                if (mCallback != null) {
                    mCallback.send(resultCode, getOriginalIntentBundle());
                }
            }
        };
    }

    /**
     * Returns the original intent Bundle
     *
     * @return Bundle
     */
    private Bundle getOriginalIntentBundle() {
        Bundle originalRequest = new Bundle();
        originalRequest.putParcelable(ORIGINAL_INTENT_EXTRA, mOriginalRequestIntent);
        return originalRequest;
    }
}
