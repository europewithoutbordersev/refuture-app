package de.europewithoutborders.refuture.service;

/**
 * Callback for the PositionProcessor.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public interface PositionProcessorCallback {

    /**
     * Sends a callback with the resultCode.
     *
     * @param resultCode
     */
    void send(int resultCode);
}
