package de.europewithoutborders.refuture.service;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.restApi.RestMethod;
import de.europewithoutborders.refuture.restApi.RestMethodFactory;
import de.europewithoutborders.refuture.restApi.RestMethodResult;
import de.europewithoutborders.refuture.restApi.resource.AllPois;
import de.europewithoutborders.refuture.restApi.resource.Poi;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Processor class for all pois that handles the request call via ReFutureRestService. It manages the pre-process and
 * post-progress logic for the all pois request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class AllPoisProcessor {

    private static final String TAG = "AllPoisProcessor";

    private final Context mContext;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param body
     */
    public AllPoisProcessor(Context context, byte[] body) {
        mContext = context;
        this.body = body;
    }

    /**
     * For the all pois request it gets the all pois request, let is executed, updates the ContentProvider and
     * sends a callback.
     *
     * @param callback
     */
    void postAllPois(AllPoisProcessorCallback callback) {
        @SuppressWarnings("unchecked") RestMethod<AllPois> postAllPoisMethod = RestMethodFactory.getInstance(mContext).getRestMethod(
                RestMethodFactory.ALL_POIS, RestMethodFactory.Method.POST, null, body
        );
        RestMethodResult<AllPois> result = postAllPoisMethod.execute();

        updateContentProvider(result);

        callback.send(result.getStatusCode());
    }

    /**
     * Creates an all pois resource and saves the data of every poi in the database.
     *
     * @param result
     */
    private void updateContentProvider(RestMethodResult<AllPois> result) {
        AllPois allPois = result.getResource();

        if (allPois != null) {
            List<Poi> pois = allPois.getAllPois();
            for (Poi poi : pois) {

                Map<String, String> typeLocalizations = poi.getTypeLocalizations();
                Uri uri = ReFutureContract.PoiTypeLocalizationEntry.CONTENT_URI;
                ContentValues values = new ContentValues();
                for (Map.Entry<String, String> entry : typeLocalizations.entrySet()) {
                    values.put(ReFutureContract.PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, poi.getType());
                    values.put(ReFutureContract.PoiTypeLocalizationEntry.COLUMN_LANGUAGE, entry.getKey());
                    values.put(ReFutureContract.PoiTypeLocalizationEntry.COLUMN_NAME, entry.getValue());
                    mContext.getContentResolver().insert(uri,values);
                }

                Uri poiUri = ReFutureContract.PoiEntry.CONTENT_URI;
                values.clear();
                values.put(ReFutureContract.PoiEntry.COLUMN_ID, poi.getId());
                values.put(ReFutureContract.PoiEntry.COLUMN_TYPE_ID, poi.getType());
                values.put(ReFutureContract.PoiEntry.COLUMN_LATITUDE, poi.getLatitude());
                values.put(ReFutureContract.PoiEntry.COLUMN_LONGITUDE, poi.getLongitude());
                values.put(ReFutureContract.PoiEntry.COLUMN_RATIO, poi.getRatio());
                values.put(ReFutureContract.PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, poi.getCreatedByGovernment());
                mContext.getContentResolver().insert(poiUri, values);

                Map<Integer, List<String>> localizations = poi.getLocalizations();
                uri = ReFutureContract.PoiLocalizationEntry.CONTENT_URI;
                for (Map.Entry<Integer, List<String>> entry : localizations.entrySet()) {
                    values.clear();
                    int id = entry.getKey();
                    List<String> poiDescription = entry.getValue();

                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_ID, id);
                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_POI_ID, poi.getId());
                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_LANGUAGE, poiDescription.get(0));
                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_NAME, poiDescription.get(1));
                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_DESCRIPTION, poiDescription.get(2));
                    values.put(ReFutureContract.PoiLocalizationEntry.COLUMN_CAPACITY, poiDescription.get(3));
                    mContext.getContentResolver().insert(uri, values);
                }

                Log.d(TAG, "id: " + poi.getId());
                Log.d(TAG, "latitude: " + poi.getLatitude());
                Log.d(TAG, "longitude: " + poi.getLongitude());
                Log.d(TAG, "ratio: " + poi.getRatio());
                for (Map.Entry<Integer, List<String>> entry : localizations.entrySet()) {
                    int id = entry.getKey();
                    Log.d(TAG, "id: " + id);
                    List<String> poiDescription = entry.getValue();
                    Log.d(TAG, "language: " + poiDescription.get(0));
                    Log.d(TAG, "    name: " + poiDescription.get(1));
                    Log.d(TAG, "        description: " + poiDescription.get(2));
                    Log.d(TAG, "            capacity: " + poiDescription.get(3));
                }
                Log.d(TAG, "type: " + poi.getType());
                for (Map.Entry<String, String> entry : typeLocalizations.entrySet()) {
                    Log.d(TAG, "    language: " + entry.getKey());
                    Log.d(TAG, "        name: " + entry.getValue());
                }

            }
            // save the current timestamp when the latest poi comes in
            Util.getSharedPrefs(mContext).edit().putLong(Constants.SHARED_PREF_LATEST_POI_TIMESTAMP, System.currentTimeMillis()).apply();
            Log.d(TAG, "timestamp changed to: " + System.currentTimeMillis());
        }

    }
}
