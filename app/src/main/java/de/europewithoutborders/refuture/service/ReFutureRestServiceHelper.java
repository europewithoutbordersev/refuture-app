package de.europewithoutborders.refuture.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

/**
 * Helper class to handle the HTTP requests as Singleton.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class ReFutureRestServiceHelper {


    private static final Object lock = new Object();
    private static ReFutureRestServiceHelper instance;

    private final Context ctx;

    /**
     * private Constructor
     *
     * @param ctx
     */
    private ReFutureRestServiceHelper(Context ctx) {
        this.ctx = ctx.getApplicationContext();
    }

    public static ReFutureRestServiceHelper getInstance(Context ctx) {
        synchronized (lock) {
            if (instance == null) {
                instance = new ReFutureRestServiceHelper(ctx);
            }
        }

        return instance;
    }

    /**
     * Starts the ReFutureService and executes a single news request.
     *
     * @param body
     */
    public void postSingleNews(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);

        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_SINGLE_NEWS);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes an all news request.
     *
     * @param body
     */
    public void postAllNews(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);
        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_ALL_NEWS);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes a single poi request.
     *
     * @param body
     */
    public void postSinglePoi(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);
        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_SINGLE_POI);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes an all pois request.
     *
     * @param body
     */
    public void postAllPois(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);
        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_ALL_POIS);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes a register refugee request.
     *
     * @param body
     */
    public void postRegisterRefugee(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);

        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_REGISTER_REFUGEE);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes a position request.
     *
     * @param body
     */
    public void postPosition(byte[] body) {
        ResultReceiver serviceCallback = new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);
        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_POST);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_POSITION);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);
        intent.putExtra(ReFutureRestService.BODY_EXTRA, body);

        this.ctx.startService(intent);
    }

    /**
     * Starts the ReFutureService and executes a countries request.
     */
    public void getCountries() {
        ResultReceiver serviceCallback = new ResultReceiver(null) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultBundle) {
                handleResponse(resultCode, resultBundle);
            }
        };

        Intent intent = new Intent(this.ctx, ReFutureRestService.class);
        intent.putExtra(ReFutureRestService.METHOD_EXTRA, ReFutureRestService.METHOD_GET);
        intent.putExtra(ReFutureRestService.RESOURCE_TYPE_EXTRA, ReFutureRestService.RESOURCE_TYPE_ALL_COUNTRIES);
        intent.putExtra(ReFutureRestService.SERVICE_CALLBACK, serviceCallback);

        this.ctx.startService(intent);
    }

    /**
     * Handles the Response and sends a broadcast.
     *
     * @param resultCode
     * @param resultData
     */
    private void handleResponse(int resultCode, Bundle resultData) {
        Intent origIntent = resultData.getParcelable(ReFutureRestService.ORIGINAL_INTENT_EXTRA);
        if (origIntent != null) {
            Intent resultBroadcast = new Intent("REQUEST_RESULT");
            resultBroadcast.putExtra("EXTRA_RESULT_CODE", resultCode);
            ctx.sendBroadcast(resultBroadcast);
        }
    }
}
