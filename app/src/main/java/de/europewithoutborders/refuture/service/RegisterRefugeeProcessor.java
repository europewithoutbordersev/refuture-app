package de.europewithoutborders.refuture.service;

import android.content.Context;
import android.util.Log;

import de.europewithoutborders.refuture.restApi.RestMethod;
import de.europewithoutborders.refuture.restApi.RestMethodFactory;
import de.europewithoutborders.refuture.restApi.RestMethodResult;
import de.europewithoutborders.refuture.restApi.resource.Refugee;
import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Processor class for registering the refugee that handles the request call via ReFutureRestService. It manages the pre-process and
 * post-progress logic for the refugee registering request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RegisterRefugeeProcessor {

    private static final String TAG = "RegRefugeeProcessor";

    private final Context mContext;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param body
     */
    public RegisterRefugeeProcessor(Context context, byte[] body) {
        mContext = context;
        this.body = body;
    }

    /**
     * For the register refugee request it gets the register refugee request, let it executed, updates the SharedPreferences and
     * sends a callback.
     *
     * @param callback
     */
    void postRegisterRefugee(RegisterRefugeeProcessorCallback callback) {
        @SuppressWarnings("unchecked") RestMethod<Refugee> postRegisterRefugeeMethod = RestMethodFactory.getInstance(mContext).getRestMethod(
                RestMethodFactory.REGISTER_REFUGEE, RestMethodFactory.Method.POST, null, body
        );
        RestMethodResult<Refugee> result = postRegisterRefugeeMethod.execute();

        updateSharedPreferences(result);

        callback.send(result.getStatusCode());
    }

    /**
     * Creates a refugee resource and save the id in the SharedPreferences.
     *
     * @param result
     */
    private void updateSharedPreferences(RestMethodResult<Refugee> result) {
        Refugee refugee = result.getResource();
        if (refugee.getRefugeeId() != 0) {
            Util.getSharedPrefs(mContext).edit().putLong(Constants.SHARED_PREF_SETTINGS_REFUGEE_ID, refugee.getRefugeeId()).apply();
            Log.d(TAG, "refugeeId: " + refugee.getRefugeeId());
        }
    }
}
