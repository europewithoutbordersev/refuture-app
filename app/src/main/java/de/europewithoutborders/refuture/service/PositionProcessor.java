package de.europewithoutborders.refuture.service;

import android.content.Context;

import de.europewithoutborders.refuture.restApi.RestMethod;
import de.europewithoutborders.refuture.restApi.RestMethodFactory;
import de.europewithoutborders.refuture.restApi.RestMethodResult;
import de.europewithoutborders.refuture.restApi.resource.Position;

/**
 * Processor class for sending the position that handles the request call via ReFutureRestService. It manages the pre-process
 * logic for sending the position request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PositionProcessor {

    private final Context mContext;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param body
     */
    public PositionProcessor(Context context, byte[] body) {
        mContext = context;
        this.body = body;
    }

    /**
     * For the sending position request it gets the request, let it executed and sends a callback.
     *
     * @param callback
     */
    void postPosition(PositionProcessorCallback callback) {
        @SuppressWarnings("unchecked") RestMethod<Position> postPositionMethod = RestMethodFactory.getInstance(mContext).getRestMethod(
                RestMethodFactory.POSITION, RestMethodFactory.Method.POST, null, body
        );
        RestMethodResult<Position> result = postPositionMethod.execute();

        callback.send(result.getStatusCode());
    }
}
