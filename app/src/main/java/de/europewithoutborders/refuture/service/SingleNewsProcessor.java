package de.europewithoutborders.refuture.service;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.util.List;
import java.util.Map;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.restApi.RestMethod;
import de.europewithoutborders.refuture.restApi.RestMethodFactory;
import de.europewithoutborders.refuture.restApi.RestMethodFactory.Method;
import de.europewithoutborders.refuture.restApi.RestMethodResult;
import de.europewithoutborders.refuture.restApi.resource.News;

/**
 * Processor class for a single news that handles the request call via ReFutureRestService. It manages the pre-process and
 * post-progress logic for the single news request.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class SingleNewsProcessor {

    private static final String TAG = "SingleNewsProcessor";

    private final Context mContext;
    private final byte[] body;

    /**
     * Constructor
     *
     * @param context
     * @param body
     */
    public SingleNewsProcessor(Context context, byte[] body) {
        mContext = context;
        this.body = body;
    }

    /**
     * For the single news request it gets the single news request, let it executed, updates the ContentProvider and
     * sends a callback.
     * @param callback
     */
    void postSingleNews(SingleNewsProcessorCallback callback) {
        @SuppressWarnings("unchecked") RestMethod<News> postSingleNewsMethod = RestMethodFactory.getInstance(mContext).getRestMethod(
                RestMethodFactory.SINGLE_NEWS, Method.POST, null, body
        );
        RestMethodResult<News> result = postSingleNewsMethod.execute();

        updateContentProvider(result);

        callback.send(result.getStatusCode());
    }

    /**
     * Creates a news resource and saves the data in the database.
     *
     * @param result
     */
    private void updateContentProvider(RestMethodResult<News> result) {
        News news = result.getResource();
        if (news != null) {
            Log.d(TAG, "id: " + news.getId());
            Log.d(TAG, "news type: " + news.getNewsType().toString());
            Log.d(TAG, "timestamp: " + news.getTimestamp());
            if (news.getNewsType().equals(News.NewsType.COUNTRY_NEWS)) {
                Log.d(TAG, "country code: " + news.getCountryCode());
            } else if (news.getNewsType().equals(News.NewsType.LOCATION_NEWS)) {
                Log.d(TAG, "latitude: " + news.getLatitude());
                Log.d(TAG, "longitude: " + news.getLongitude());
            }
            Log.d(TAG, "localizations:");
            Map<Integer, List<String>> localizations2 = news.getLocalizations();
            for (Map.Entry<Integer, List<String>> entry : localizations2.entrySet()) {
                int id = entry.getKey();
                List<String> message = entry.getValue();
                Log.d(TAG, "     id: " + id);
                Log.d(TAG, "     language: " + message.get(0));
                Log.d(TAG, "     heading: " + message.get(1));
                Log.d(TAG, "     body: " + message.get(2));
            }
            Log.d(TAG, "---------------");

            Uri newsUri = ReFutureContract.NewsEntry.CONTENT_URI;
            ContentValues newsValues = new ContentValues();
            News.NewsType newsType = news.getNewsType();

            newsValues.put(ReFutureContract.NewsEntry.COLUMN_ID, news.getId());

            if (newsType.equals(News.NewsType.COUNTRY_NEWS)) {
                newsValues.put(ReFutureContract.NewsEntry.COLUMN_COUNTRY, news.getCountryCode());
            } else if (newsType.equals(News.NewsType.LOCATION_NEWS)) {
                newsValues.put(ReFutureContract.NewsEntry.COLUMN_LATITUDE, news.getLatitude());
                newsValues.put(ReFutureContract.NewsEntry.COLUMN_LONGITUDE, news.getLongitude());
            }
            newsValues.put(ReFutureContract.NewsEntry.COLUMN_TIMESTAMP, news.getTimestamp());
            mContext.getContentResolver().insert(newsUri, newsValues);

            Map<Integer, List<String>> localizations = news.getLocalizations();
            for (Map.Entry<Integer, List<String>> entry : localizations.entrySet()) {
                int id = entry.getKey();
                List<String> message = entry.getValue();

                Uri localUri = ReFutureContract.NewsLocalizationEntry.CONTENT_URI;
                ContentValues localValues = new ContentValues();
                localValues.put(ReFutureContract.NewsLocalizationEntry.COLUMN_ID, id);
                localValues.put(ReFutureContract.NewsLocalizationEntry.COLUMN_LANGUAGE, message.get(0));
                localValues.put(ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING, message.get(1));
                localValues.put(ReFutureContract.NewsLocalizationEntry.COLUMN_BODY, message.get(2));
                localValues.put(ReFutureContract.NewsLocalizationEntry.COLUMN_NEWS_ID, news.getId());
                mContext.getContentResolver().insert(localUri, localValues);
            }
        }
    }
}
