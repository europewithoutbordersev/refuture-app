package de.europewithoutborders.refuture;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry;
import de.europewithoutborders.refuture.util.Util;

/**
 * The PoiDetailsActivity displays every information about a poi. Additionally it supports localization and
 * updates the view if the user changes the language.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class PoiDetailsActivity extends AppCompatActivity {

    private static final String TAG = "PoiDetailsActivity";

    private int poiId;
    private final String CHARSET_NAME = "UTF-8";
    private ArrayAdapter<String> spinnerArrayAdapter;
    private List<String> langs =  new ArrayList<>();
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.poi_details_toolbar);
        setSupportActionBar(toolbar);

        Intent myIntent = getIntent();
        poiId = myIntent.getIntExtra("poiId", 0);
        Log.d(TAG, "poi with id: " + poiId);

        cursor = getLocalizedLanguages(poiId);
        langs = Util.getAvailableLocalizationLanguages(cursor, this);

        final Spinner spinner = (Spinner) findViewById(R.id.poi_details_select_language_spinner);
        spinnerArrayAdapter = new ArrayAdapter<>(this, R.layout.language_spinner_item, langs);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.language_spinner_dropdown_item); // The drop down view
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                Cursor poiDetailsCursor;

                String language = spinner.getSelectedItem().toString();
                Log.d(TAG, "selected language: " + language);
                String languageCode = "";
                if (language.equals(getResources().getString(R.string.language_german))) {
                    languageCode = "de";
                } else if (language.equals(getResources().getString(R.string.language_english))) {
                    languageCode = "en";
                } else if (language.equals(getResources().getString(R.string.language_french))) {
                    languageCode = "fr";
                } else if (language.equals(getResources().getString(R.string.language_arabic))) {
                    languageCode = "ar";
                }

                setLocale(languageCode);

                poiDetailsCursor = getPoiDetails(poiId, languageCode);

                if(poiDetailsCursor != null) {
                    if(poiDetailsCursor.moveToFirst()) {
                        String name = poiDetailsCursor.getString(poiDetailsCursor.getColumnIndex(PoiLocalizationEntry.COLUMN_NAME));
                        String description = poiDetailsCursor.getString(poiDetailsCursor.getColumnIndex(PoiLocalizationEntry.COLUMN_DESCRIPTION));
                        String capacity = poiDetailsCursor.getString(poiDetailsCursor.getColumnIndex(PoiLocalizationEntry.COLUMN_CAPACITY));
                        float ratio = poiDetailsCursor.getFloat(poiDetailsCursor.getColumnIndex(PoiEntry.COLUMN_RATIO));
                        boolean createdByGov = poiDetailsCursor.getInt(poiDetailsCursor.getColumnIndex(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT)) != 0;

                        TextView poiName = (TextView) findViewById(R.id.poi_details_poiName);
                        TextView poiDescription = (TextView) findViewById(R.id.poi_details_description);
                        TextView poiRatio = (TextView) findViewById(R.id.poi_details_ratio);
                        TextView poiCreatedByGov = (TextView) findViewById(R.id.poi_details_createdByGovernment);
                        TextView poiCapacity = (TextView) findViewById(R.id.poi_details_capacity);

                        try {
                            // Check if the poi has values to set, if not hide the textview from the view
                            if (name.equals("")) {
                                findViewById(R.id.poi_details_poiName).setVisibility(View.GONE);
                            } else {
                                poiName.setText(URLDecoder.decode(name, CHARSET_NAME));
                            }

                            if (description.equals("")) {
                                findViewById(R.id.poi_details_descriptionLayout).setVisibility(View.GONE);
                            } else {
                                poiDescription.setText(URLDecoder.decode(description, CHARSET_NAME));
                            }

                            if (ratio < 0) {
                                findViewById(R.id.poi_details_ratioLayout).setVisibility(View.GONE);
                            } else {
                                String ratioText = String.valueOf(ratio) + " %";
                                poiRatio.setText(ratioText);
                            }

                            if (capacity.equals("")) {
                                findViewById(R.id.poi_details_capacityLayout).setVisibility(View.GONE);
                            } else {
                                poiCapacity.setText(URLDecoder.decode(capacity, CHARSET_NAME));
                            }

                            if(createdByGov) {
                                poiCreatedByGov.setText(getResources().getString(R.string.poi_details_created_by_government_yes));
                            } else {
                                findViewById(R.id.poi_details_createdByGovernmentLayout).setVisibility(View.GONE);
                            }
                        } catch (UnsupportedEncodingException uee) {
                            Log.e(TAG, "encoding not supported", uee);
                        }

                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }

        });
    }

    /**
     * Gets poi information from content provider by the given poi id and language.
     *
     * @param poiId
     * @param language
     * @return Cursor
     */
    private Cursor getPoiDetails(int poiId, String language) {
        Uri poiUri = ReFutureContract.PoiEntry.CONTENT_URI;

        String[] projection = {PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_NAME,
                                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_DESCRIPTION,
                                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_CAPACITY,
                                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_RATIO,
                                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT};
        String selection = ReFutureContract.PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_POI_ID + " = ? AND " +
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE + " = ?";
        String[] selectionArgs = {String.valueOf(poiId), language};
        return getContentResolver().query(poiUri, projection, selection, selectionArgs, null);
    }

    /**
     * Gets all of the languages the poi is localized in.
     *
     * @return Cursor with all languages
     */
    private Cursor getLocalizedLanguages(int poiId) {
        String[] projection = {PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE, PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_POI_ID};
        String selection = PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_POI_ID + " = ?";
        String[] selectionArgs = {String.valueOf(poiId)};
        return getContentResolver().query(PoiEntry.CONTENT_URI, projection, selection, selectionArgs, null);
    }

    /**
     * Changes the Locale and updates the view.
     *
     * @param lang
     */
    private void setLocale(String lang) {
        Util.updateLocale(lang, this);
        initLanguageSpinner();
        updateLabels();
    }

    /**
     * Updates the label for the tabs to the current localization.
     */
    private void updateLabels() {
        TextView description = (TextView) findViewById(R.id.poi_details_descriptionLabel);
        description.setText(getResources().getString(R.string.poi_details_description));
        TextView ratio = (TextView) findViewById(R.id.poi_details_ratioLabel);
        ratio.setText(getResources().getString(R.string.poi_details_ratio));
        TextView createdByGov = (TextView) findViewById(R.id.poi_details_createdByGovernmentLabel);
        createdByGov.setText(getResources().getString(R.string.poi_details_created_by_government));
        TextView capacity = (TextView) findViewById(R.id.poi_details_capacityLabel);
        capacity.setText(getResources().getString(R.string.poi_details_capacity));
    }

    /**
     * Initializes the Language spinner by clearing the used list, filling it with the new localized languages
     * and then notifies the spinner array adapter.
     */
    private void initLanguageSpinner() {
        langs.clear();
        List<String> languageList = Util.getAvailableLocalizationLanguages(cursor, this);
        for (int i = 0; i < languageList.size(); i++) {
            langs.add(languageList.get(i));
        }

        spinnerArrayAdapter.notifyDataSetChanged();
        Log.d(TAG, "spinner updated");
    }

    /**
     * Changes the Locale back to the default in the onDestroy process.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.setDefaultLocale(this);
    }

}
