package de.europewithoutborders.refuture.util;

/**
 * Constants used by this App.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Constants {

    /**
     * Default Radius in km
     */
    public static final int DEFAULT_RADIUS_IN_KM = 50;

    /**
     * Name of the Shared Preferences file
     */
    public static final String SHARED_PREF_NAME = "ReFutureSharedPrefs";

    public static final int MIN_DISTANCE_IN_KM_FOR_MAP_UPDATES = 20;

    // GCM related
    public static final String SHARED_PREF_GCM_TOKEN = "gcmToken";
    public static final String SHARED_PREF_SENT_GCM_TOKEN = "GcmTokenIsSent";
    public static final String SHARED_PREF_PUBSUB_SUCCESS = "pubSubSuccess";

    // Settings related
    public static final String SHARED_PREF_SETTINGS_AGE = "settingsAge";
    public static final String SHARED_PREF_SETTINGS_IS_MALE = "settingsIsMale";
    public static final String SHARED_PREF_SETTINGS_EDUCATION = "settingsEducation";
    public static final String SHARED_PREF_SETTINGS_LANGUAGES = "settingsLanguages";
    public static final String SHARED_PREF_SETTINGS_START_COUNTRY = "settingsStartCountry";
    public static final String SHARED_PREF_SETTINGS_START_COUNTRY_CODE = "settingsStartCountryCode";
    public static final String SHARED_PREF_SETTINGS_END_COUNTRY = "settingsEndCountry";
    public static final String SHARED_PREF_SETTINGS_END_COUNTRY_CODE = "settingsEndCountryCode";
    public static final String SHARED_PREF_SETTINGS_ENTOURAGE = "settingsEntourage";
    public static final String SHARED_PREF_SETTINGS_INFLUX = "settingsInflux";
    public static final String SHARED_PREF_SETTINGS_RADIUS = "settingsRadius";
    public static final String SHARED_PREF_SETTINGS_SAVED = "saved";

    public static final String BUNDLE_OLD_LANGUAGES = "oldLanguages";
    public static final String BROADCAST_LANGUAGES_SELECTED = "LanguagesSelected";

    // Settings related -- used to find out if Settings are already set
    public static final String SETTINGS_CALL_DATA = "SettingsCallData";
    public static final int SETTINGS_INITIAL_CALL = 0;
    public static final int SETTINGS_NOT_INITIAL_CALL = 1;

    // Timestamp of the latest requests
    public static final String SHARED_PREF_LATEST_NEWS_TIMESTAMP = "latestNewsTimestamp";
    public static final String SHARED_PREF_LATEST_POI_TIMESTAMP = "latestPoiTimestamp";

    // Cursor Loader IDs
    public static final int LOADER_POI_DATA = 23;

    // GPS Tracking values
    public static final int POSITION_UPDATE_TIME_MINS = 60;
    public static final int POSITION_FASTEST_UPDATE_INTERVAL_MINS = 30;

    // JSON attribute names
    public static final String JSON_NEWS = "news";
    public static final String JSON_NEWS_ID = "id";
    public static final String JSON_NEWS_LATITUDE = "latitude";
    public static final String JSON_NEWS_LONGITUDE = "longitude";
    public static final String JSON_NEWS_COUNTRY_CODE = "country_code";
    public static final String JSON_NEWS_TIMESTAMP = "timestamp";
    public static final String JSON_NEWS_LOCALIZATIONS = "localizations";
    public static final String JSON_NEWS_LOCALIZATIONS_ID = "id";
    public static final String JSON_NEWS_LOCALIZATIONS_HEADING = "heading";
    public static final String JSON_NEWS_LOCALIZATIONS_BODY = "body";
    public static final String JSON_NEWS_LOCALIZATIONS_LANGUAGE = "language";

    public static final String JSON_REGISTER_REFUGEE_ID = "refugee_id";

    public static final String JSON_POI = "locations";
    public static final String JSON_POI_ID = "id";
    public static final String JSON_POI_LATITUDE = "latitude";
    public static final String JSON_POI_LONGITUDE = "longitude";
    public static final String JSON_POI_RATIO = "ratio";
    public static final String JSON_POI_CREATED_BY_GOVERNMENT = "createdByGovernment";
    public static final String JSON_POI_LOCALIZATIONS = "localizations";
    public static final String JSON_POI_LOCALIZATIONS_ID = "id";
    public static final String JSON_POI_LOCALIZATIONS_LANGUAGE = "language";
    public static final String JSON_POI_LOCALIZATIONS_NAME = "name";
    public static final String JSON_POI_LOCALIZATIONS_DESCRIPTION = "description";
    public static final String JSON_POI_LOCALIZATIONS_CAPACITY = "capacity";
    public static final String JSON_POI_TYPE = "type";
    public static final String JSON_POI_TYPE_LOCALIZATIONS = "type_localizations";
    public static final String JSON_POI_TYPE_LOCALIZATIONS_LANGUAGE = "language";
    public static final String JSON_POI_TYPE_LOCALIZATIONS_NAME = "name";

    public static final String JSON_COUNTRIES = "countries";
    public static final String JSON_COUNTRIES_COUNTRY_CODE = "country_code";
    public static final String JSON_COUNTRIES_LOCALIZATIONS = "localizations";
    public static final String JSON_COUNTRIES_LOCALIZATIONS_LANG = "lang";
    public static final String JSON_COUNTRIES_LOCALIZATIONS_NAME = "name";


    public static final String SHARED_PREF_REST_ID_PARAMETER = "restIdParameter";
    public static final String SHARED_PREF_SETTINGS_REFUGEE_ID = "refugeeId";
}
