package de.europewithoutborders.refuture.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.europewithoutborders.refuture.MainActivity;
import de.europewithoutborders.refuture.R;
import de.europewithoutborders.refuture.SettingsActivity;

/**
 * Util functions used by this App.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Util {

    private static final String TAG = "Utils";

    /**
     * Returns private access to the Shared Preferences file
     *
     * @param context To open the file
     * @return SharedPreferences file uniquely used by this App
     */
    public static SharedPreferences getSharedPrefs (Context context) {
        return context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Check the device to make sure it has Google Play Services. If
     * it doesn't, display a dialog that allows users to download Google Play Services from
     * the Google Play Store or enable it in the device's system settings.
     */
    public static boolean checkPlayServices(Activity activity, int requestCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, requestCode)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    /**
     * Creates the ResourceID from a given name within this project. No data type is required here.
     *
     * @param drawableName The name of the drawable to load
     * @return The ResourceID from the drawable or -1, if no such drawable could be found.
     */
    public static int getResourceId(String drawableName) {
        try {
            Class<R.drawable> res = R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e(TAG, "Failure to get drawable id.", e);
        }
        return -1;
    }

    /**
     * Creates the ResourceID from a given name within the CountryPicker project. No data type is required here.
     *
     * @param drawableName The name of the drawable to load
     * @return The ResourceID from the drawable or -1, if no such drawable could be found.
     */
    public static int getFlagResourceId(String drawableName) {
        try {
            Class<com.countrypicker.R.drawable> res = com.countrypicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e(TAG, "Failure to get drawable id.", e);
        }
        return -1;
    }

    /**
     * Calculates the distance in km between two points somewhere on the earth.
     *
     * @param lat1 Latitude of Point 1
     * @param lon1 Longitude of Point 1
     * @param lat2 Latitude of Point 2
     * @param lon2 Longitude of Point 2
     * @return Distance of the two points in km
     * @see <a href="https://en.wikipedia.org/wiki/Haversine_formula">Haversine Formula</a>
     */
    public static double haversine(double lat1, double lon1, double lat2, double lon2) {
        double earthRadius = 6372.8; // In kilometers

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return earthRadius * c;
    }

    public static LatLngBounds getLatLngBounds(LatLng position, int distance) {
        double earthRadius = 6371;  // earth radius in km

        double lat1 = position.latitude + Math.toDegrees(distance/earthRadius);
        double lat2 = position.latitude - Math.toDegrees(distance/earthRadius);
        double lon1 = position.longitude - Math.toDegrees(distance/earthRadius/Math.cos(Math.toRadians(position.latitude)));
        double lon2 = position.longitude + Math.toDegrees(distance/earthRadius/Math.cos(Math.toRadians(position.latitude)));

        LatLng northEast = new LatLng(lat1, lon1);
        LatLng southWest = new LatLng(lat2, lon2);

        return new LatLngBounds.Builder().include(northEast).include(southWest).build();
    }

    /**
     * Get the escaped SQL statement part for the given languages.
     *
     * @param languages The languages to escape from the SQL statement
     * @return A string which can be used to prepare an SQL statement
     */
    public static String getEscapedLanguageSelection(List<String> languages) {
        StringBuilder selectionBuilder = new StringBuilder();

        for(int i = 0; i < languages.size(); i++) {
            if(i != 0) {
                selectionBuilder.append(", ");
            }
            selectionBuilder.append("?");
        }

        return selectionBuilder.toString();
    }

    /**
     * Unsubscribes GCM Token from the given languages
     *
     * @param context To access GCM
     * @param oldLanguages Languages to unsubscribe from
     * @throws IOException If PubSub is not reachable at this very moment
     */
    private static void unsubscribeTopics(Context context, List<String> oldLanguages) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(context);
        SharedPreferences sharedPreferences = getSharedPrefs(context);
        // get GCM Token
        String token = sharedPreferences.getString(Constants.SHARED_PREF_GCM_TOKEN, null);

        if(token != null) {
            for (String topic : oldLanguages) {
                // unsubscribe from news in every language
                pubSub.unsubscribe(token, "/topics/news_" + topic);
            }
        }
    }

    /**
     * Subscribes GCM to all locations and news for the given languages.
     *
     * @param context To access GCM
     * @param languages Languages to subscribe to
     * @throws IOException If PubSub is not reachable at this very moment
     */
    public static void subscribeTopics(Context context, List<String> languages) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(context);
        SharedPreferences sharedPreferences = getSharedPrefs(context);
        // get GCM Token
        String token = sharedPreferences.getString(Constants.SHARED_PREF_GCM_TOKEN, null);

        if(token != null) {
            for (String topic : languages) {
                // subscribe to news in every language
                pubSub.subscribe(token, "/topics/news_" + topic, null);
            }
            // subscribe to locations
            pubSub.subscribe(token, "/topics/locations", null);
        }
    }

    /**
     * Unsubscribes first from every language which are set in an earlier registration process
     * and subscribes then to the language which are set in this registration process.
     * If everything works out ok, Constants#SHARED_PREF_PUBSUB_SUCCESS will be set to true, else it will be set to false.
     *
     * @param context To access GCM
     * @param oldLanguages Languages to unsubscribe from
     * @param newLanguages Languages to subscribe to
     */
    public static void updateGCMSubscriptions(final Context context, final List<String> oldLanguages, final List<String> newLanguages) {
        final SharedPreferences sharedPrefs = getSharedPrefs(context);
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Util.unsubscribeTopics(context, oldLanguages);
                    Util.subscribeTopics(context, newLanguages);
                    Log.d(TAG, "Successfully updated language topic subscriptions!");
                    sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_PUBSUB_SUCCESS, true).apply();
                } catch (IOException ioe) {
                    Log.d(TAG, "Language subscription failed! " + ioe.getMessage());
                    ioe.printStackTrace();
                    sharedPrefs.edit().putBoolean(Constants.SHARED_PREF_PUBSUB_SUCCESS, false).apply();
                }
                return null;
            }
        }.execute();

    }

    /**
     * Checks if the GPS is enabled or disabled.
     *
     * @return GPS status (enabled/disabled)
     */
    public static boolean isGPSEnabled(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Shows an AlertDialog if the GPS is disabled. The User can either enable the GPS
     * or return to the main view.
     */
    public static void disabledGPSError(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(activity.getString(R.string.mapLocation_gpsErrorMessage))
                .setCancelable(false)
                .setPositiveButton(activity.getString(R.string.mapLocation_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        activity.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(activity.getString(R.string.mapLocation_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * Create and show a simple notification.
     *
     * @param context To access the notification panel
     * @param targetActivity Acitivity to open when clicked on the notification. If {@code NULL}, {@link MainActivity} will be opened
     * @param extras Extras passed to the activity to open
     * @param message Message to display in the notification panel
     */
    public static void showNotification(Context context, Class targetActivity, Bundle extras, String message) {
        Intent intent;
        if(targetActivity != null) {
            intent = new Intent(context, targetActivity);
        } else {
            intent = new Intent(context, MainActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if(extras != null) {
            intent.putExtras(extras);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    /**
     * Creates a List containing the selected languages in {@link SettingsActivity}.
     *
     * @param context To open the Shared Preferences file
     * @return A List containing the user-selected languages
     */
    public static List<String> getSetLanguages(Context context) {
        SharedPreferences sharedPrefs = getSharedPrefs(context);
        List<String> newLanguages = new ArrayList<>();

        if(sharedPrefs.getBoolean("ar", false)) {
            newLanguages.add("ar");
        }
        if(sharedPrefs.getBoolean("en", false)) {
            newLanguages.add("en");
        }
        if(sharedPrefs.getBoolean("fr", false)) {
            newLanguages.add("fr");
        }
        if(sharedPrefs.getBoolean("de", false)) {
            newLanguages.add("de");
        }

        return newLanguages;
    }

    /**
     * Sets the default Localization for this app.
     *
     * @param context
     */
    public static void setDefaultLocale(Context context) {
        Resources standardResources = context.getResources();
        DisplayMetrics metrics = standardResources.getDisplayMetrics();
        Configuration config = new Configuration(standardResources.getConfiguration());
        config.locale = Locale.getDefault();
        standardResources.updateConfiguration(config, metrics);
    }

    /**
     * Creates a list with all languages that are in the data and the user settings.
     *
     * @param cursor
     * @param context
     *
     * @return List with languages
     */
    public static List<String> getAvailableLocalizationLanguages(Cursor cursor, Context context) {
        List<String> localizedLanguages = new ArrayList<>();
        List<String> languageList = new ArrayList<>();

        SharedPreferences mSharedPrefs = getSharedPrefs(context);
        boolean german = mSharedPrefs.getBoolean("de", false);
        boolean english = mSharedPrefs.getBoolean("en", false);
        boolean french = mSharedPrefs.getBoolean("fr", false);
        boolean arabic = mSharedPrefs.getBoolean("ar", false);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    String lang = cursor.getString(0);
                    if (!localizedLanguages.contains(lang)) {
                        localizedLanguages.add(lang);
                    }
                } while (cursor.moveToNext());
            }
        }
        if (german && localizedLanguages.contains("de")) {
            languageList.add(context.getResources().getString(R.string.language_german));
        }
        if (english && localizedLanguages.contains("en")) {
            languageList.add(context.getResources().getString(R.string.language_english));
        }
        if (french && localizedLanguages.contains("fr")) {
            languageList.add(context.getResources().getString(R.string.language_french));
        }
        if (arabic && localizedLanguages.contains("ar")) {
            languageList.add(context.getResources().getString(R.string.language_arabic));
        }

        return languageList;
    }

    /**
     * Changes the Locale of this app to the given parameter.
     *
     * @param lang
     * @param context
     */
    public static void updateLocale(String lang, Context context) {
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

}
