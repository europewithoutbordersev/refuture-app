package de.europewithoutborders.refuture;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.util.Util;

/**
 * The NewsActivity consists of two fragments that displays the local/location news and the global news. It supports
 * localization and changes updates the list view if the user interacts.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class NewsActivity extends AppCompatActivity {

    private static final String TAG = "NewsActivity";
    private TabLayout tabLayout;
    private boolean initialized = false;
    private boolean firstTime = true;
    private List<String> langs = new ArrayList<>();
    private ArrayAdapter<String> spinnerArrayAdapter;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        init();
    }

    protected void onResume() {
        super.onResume();
        if (firstTime) {
            firstTime = false;
        } else {
            if (!initialized) {
                init();
            }
        }
    }

    /**
     * Initializes the View by created the tab layout and fills it with data.
     */
    private void init() {
        if (Util.isGPSEnabled(this)) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.news_toolbar);
            setSupportActionBar(toolbar);

            initialized = true;

            tabLayout = (TabLayout) findViewById(R.id.tab_layout);
            tabLayout.addTab(tabLayout.newTab().setText(getText(R.string.news_local)));
            tabLayout.addTab(tabLayout.newTab().setText(getText(R.string.news_global)));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
            final PagerAdapter adapter = new PagerAdapter
                    (getSupportFragmentManager(), tabLayout.getTabCount());
            viewPager.setAdapter(adapter);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

            cursor = getLocalizedLanguages();
            langs = Util.getAvailableLocalizationLanguages(cursor, this);

            final Spinner spinner = (Spinner) findViewById(R.id.news_select_language_spinner);
            spinnerArrayAdapter = new ArrayAdapter<>(this, R.layout.language_spinner_item, langs);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.language_spinner_dropdown_item); // The drop down view
            spinner.setAdapter(spinnerArrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    String language = spinner.getSelectedItem().toString();
                    Log.d(TAG, "selected language: " + language);
                    String languageCode = "";
                    if (language.equals(getResources().getString(R.string.language_german))) {
                        languageCode = "de";
                    } else if (language.equals(getResources().getString(R.string.language_english))) {
                        languageCode = "en";
                    } else if (language.equals(getResources().getString(R.string.language_french))) {
                        languageCode = "fr";
                    } else if (language.equals(getResources().getString(R.string.language_arabic))) {
                        languageCode = "ar";
                    }

                    setLocale(languageCode);

                    Intent resultBroadcast = new Intent("NewsFragment");
                    resultBroadcast.putExtra("LANG", languageCode);
                    getApplicationContext().sendBroadcast(resultBroadcast);
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {

                }

            });
        } else {
            Util.disabledGPSError(this);
        }
    }

    /**
     * Gets all of the languages the news are localized in.
     *
     * @return Cursor with all languages
     */
    private Cursor getLocalizedLanguages() {
        String[] projection = {ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.PoiLocalizationEntry.COLUMN_LANGUAGE};
        return getContentResolver().query(ReFutureContract.NewsEntry.CONTENT_URI, projection, null, null, null);
    }

    /**
     * Changes the Locale and updates the view.
     *
     * @param lang
     */
    private void setLocale(String lang) {
        Util.updateLocale(lang, this);
        initLanguageSpinner();
        updateLabels();
    }

    /**
     * Updates the label for the tabs to the current localization.
     */
    private void updateLabels() {
        tabLayout.getTabAt(0).setText(getText(R.string.news_local));
        tabLayout.getTabAt(1).setText(getText(R.string.news_global));
    }

    /**
     * Initializes the Language spinner by clearing the used list, filling it with the new localized languages
     * and then notifies the spinner array adapter.
     */
    private void initLanguageSpinner() {
        langs.clear();
        List<String> languageList = Util.getAvailableLocalizationLanguages(cursor, this);
        for (int i = 0; i < languageList.size(); i++) {
            langs.add(languageList.get(i));
        }

        spinnerArrayAdapter.notifyDataSetChanged();
        Log.d(TAG, "spinner updated");
    }

    /**
     * Changes the Locale back to the default in the onDestroy process.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Util.setDefaultLocale(this);
    }
}
