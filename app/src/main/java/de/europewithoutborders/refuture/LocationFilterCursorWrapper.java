package de.europewithoutborders.refuture;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.util.Util;

/**
 * CursorWrapper that filters all entries of a cursor with a latitude and longitude that are not
 * in the radius of the current location of the user.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class LocationFilterCursorWrapper extends CursorWrapper {

    private int radius;
    private int[] index;
    private int count=0;
    private int pos=0;

    public LocationFilterCursorWrapper(Cursor cursor, int radius, double positionLatitude, double positionLongitude) {
        super(cursor);
        this.radius = radius;
        if (this.radius != 0) {
            this.count = super.getCount();
            this.index = new int[this.count];
            for (int i=0; i < this.count; i++) {
                super.moveToPosition(i);
                float latitude = cursor.getFloat(cursor.getColumnIndexOrThrow(ReFutureContract.NewsEntry.COLUMN_LATITUDE));
                float longitude = cursor.getFloat(cursor.getColumnIndexOrThrow(ReFutureContract.NewsEntry.COLUMN_LONGITUDE));
                Log.d("LocFilterCursorWrapper", "Position Latitude: " + positionLatitude);
                Log.d("LocFilterCursorWrapper", "Position Longitude: " + positionLongitude);
                Log.d("LocFilterCursorWrapper", "News Latitude: " + latitude);
                Log.d("LocFilterCursorWrapper", "News Longitude: " + longitude);
                double distance = Util.haversine(positionLatitude, positionLongitude, latitude, longitude);
                Log.d("LocFilterCursorWrapper", "distance: " + distance);
                if (distance < radius)
                    this.index[this.pos++] = i;
            }
            this.count = this.pos;
            this.pos = 0;
            super.moveToFirst();
        } else {
            this.count = super.getCount();
            this.index = new int[this.count];
            for (int i=0;i<this.count;i++) {
                this.index[i] = i;
            }
        }
    }

    @Override
    public boolean move(int offset) {
        return this.moveToPosition(this.pos+offset);
    }

    @Override
    public boolean moveToNext() {
        return this.moveToPosition(this.pos+1);
    }

    @Override
    public boolean moveToPrevious() {
        return this.moveToPosition(this.pos-1);
    }

    @Override
    public boolean moveToFirst() {
        return this.moveToPosition(0);
    }

    @Override
    public boolean moveToLast() {
        return this.moveToPosition(this.count-1);
    }

    @Override
    public boolean moveToPosition(int position) {
        if (position >= this.count || position < 0)
            return false;
        return super.moveToPosition(this.index[position]);
    }

    @Override
    public int getCount() {
        return this.count;
    }

    @Override
    public int getPosition() {
        return this.pos;
    }

}
