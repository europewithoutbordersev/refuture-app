package de.europewithoutborders.refuture;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import de.europewithoutborders.refuture.data.ReFutureContract;

/**
 * GlobalNewsFragment displays all news that were created for all users in a list.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class GlobalNewsFragment extends Fragment implements AdapterView.OnItemClickListener {

    private final static String TAG = "GlobalNewsFragment";

    private View view;
    private ListView mListView;
    private BroadcastReceiver requestReceiver;
    private String lang = "";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);

        IntentFilter filter = new IntentFilter("NewsFragment");
        requestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                lang = intent.getStringExtra("LANG");
                Log.d(TAG, "language: " + lang);
                showGlobalNews(lang);
            }
        };

        getContext().registerReceiver(requestReceiver, filter);

        return view;
    }

    /**
     * Gets the globals news and displays it in the ListView.
     *
     * @param lang
     */
    private void showGlobalNews(String lang) {
        Log.d(TAG, "language: " + lang);
        Cursor mCursor = getGlobalNews(lang);

        if(mCursor != null){
            mListView = (ListView) view.findViewById(R.id.news_list);
            NewsCursorAdapter adapter = new NewsCursorAdapter(getActivity(), mCursor, 0);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(this);
        }
    }

    /**
     * Reads every global news from the database in a specific language.
     *
     * @param lang
     * @return Cursor
     */
    private Cursor getGlobalNews(String lang) {
        Uri newsUri = ReFutureContract.NewsEntry.CONTENT_URI;
        String[] projection = {ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_ID,
                ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_HEADING,
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_TIMESTAMP};
        String selection = ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LATITUDE + " IS NULL AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_LONGITUDE + " IS NULL AND " +
                ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_COUNTRY + " IS NULL AND " +
                ReFutureContract.NewsLocalizationEntry.TABLE_NAME + "." + ReFutureContract.NewsLocalizationEntry.COLUMN_LANGUAGE + " = ?";
        String[] selectionArgs = {lang};
        String sortOrder = ReFutureContract.NewsEntry.TABLE_NAME + "." + ReFutureContract.NewsEntry.COLUMN_TIMESTAMP + " DESC";
        return getActivity().getContentResolver().query(newsUri, projection, selection, selectionArgs, sortOrder);
    }

    /**
     * OnItemClick event, that starts a new intent with the news details for a list item.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
        int newsId = cursor.getInt(cursor.getColumnIndexOrThrow(ReFutureContract.NewsEntry.COLUMN_ID));
        Intent newsDetailsIntent = new Intent(getActivity(), NewsDetailsActivity.class);

        newsDetailsIntent.putExtra("newsId", newsId);
        newsDetailsIntent.putExtra("lang", lang);
        startActivity(newsDetailsIntent);
    }

    /**
     * Unregisters the Receiver in the onDestroy process.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        getContext().unregisterReceiver(requestReceiver);
    }
}
