package de.europewithoutborders.refuture;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.List;

import de.europewithoutborders.refuture.util.Constants;
import de.europewithoutborders.refuture.util.Util;

/**
 * Service to get a new GCM Token. If a Token is already given to this device and it's still valid, the old token will be returned.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    private SharedPreferences mSharedPrefs;

    @Override
    protected void onHandleIntent(Intent intent) {
        mSharedPrefs = Util.getSharedPrefs(getApplicationContext());

        try {
            // Proxy-Call to get the GCM Token
            InstanceID instanceID = InstanceID.getInstance(this);
            // R.string.gcm_defaultSenderId (the Sender ID) will be defined at "google-services.json"
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            Log.i(TAG, "GCM Registration Token: " + token);
            if(!mSharedPrefs.getString(Constants.SHARED_PREF_GCM_TOKEN, "").equals(token)) {
                // we have a new token
                mSharedPrefs.edit().putString(Constants.SHARED_PREF_GCM_TOKEN, token).apply(); // save token

                // get languages
                List<String> newLanguages = Util.getSetLanguages(getApplicationContext());
                if (!newLanguages.isEmpty()) {
                    // if we have languages already set, subscribe to the topics
                    try {
                        Util.subscribeTopics(getApplicationContext(), newLanguages);
                        mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_PUBSUB_SUCCESS, true).apply();
                    } catch (IOException ioe) {
                        mSharedPrefs.edit().putBoolean(Constants.SHARED_PREF_PUBSUB_SUCCESS, false).apply();
                    }
                }

            }

        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
    }

}
