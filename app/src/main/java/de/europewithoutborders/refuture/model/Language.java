package de.europewithoutborders.refuture.model;

/**
 * Represents the Language of the localizations.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
public class Language {

    private String language;
    private String flagCode;
    private String isoCode;
    private boolean isChecked = false;

    public Language(String language, String flagCode, String isoCode) {
        this.language = language;
        this.flagCode = flagCode;
        this.isoCode = isoCode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFlagCode() {
        return flagCode;
    }

    public void setFlagCode(String flagCode) {
        this.flagCode = flagCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
}
