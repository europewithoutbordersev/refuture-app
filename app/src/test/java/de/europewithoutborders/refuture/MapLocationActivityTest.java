package de.europewithoutborders.refuture;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowLocationManager;

import de.europewithoutborders.refuture.util.Util;

/**
 * Unit test for MapLocationActivity.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MapLocationActivityTest {

    private MapLocationActivity mapLocationActivity;
    private LocationManager locationManager;
    private ShadowLocationManager shadowLocationManager;

    @Before
    public void setUp() {
        mapLocationActivity = Robolectric.buildActivity(MapLocationActivity.class).create().start().resume().visible().get();
        locationManager = (LocationManager) mapLocationActivity.getSystemService(Context.LOCATION_SERVICE);
        shadowLocationManager = Shadows.shadowOf(locationManager);
    }

    @Test
    public void disabledGPSErrorTest() throws Exception {
        shadowLocationManager.setProviderEnabled(LocationManager.GPS_PROVIDER, false);
        mapLocationActivity.showLocationOnMap();
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        ShadowAlertDialog shadowAlertDialog = Shadows.shadowOf(alertDialog);
        Assert.assertEquals(shadowAlertDialog.getMessage().toString(), mapLocationActivity.getString(R.string.mapLocation_gpsErrorMessage));
    }

    @Test
    public void returnedToLastActivityTest() throws Exception {
        shadowLocationManager.setProviderEnabled(LocationManager.GPS_PROVIDER, false);
        mapLocationActivity.showLocationOnMap();
        ShadowAlertDialog shadowAlertDialog = Shadows.shadowOf(ShadowAlertDialog.getLatestAlertDialog());
        AlertDialog alertDialog = ShadowAlertDialog.getLatestAlertDialog();
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).performClick();
        ShadowActivity shadowActivity = Shadows.shadowOf(mapLocationActivity);
        Assert.assertTrue(shadowActivity.isFinishing());
    }

    @Test
    public void enabledGPSTest() throws Exception {
        shadowLocationManager.setProviderEnabled(LocationManager.GPS_PROVIDER, true);
        boolean gps = Util.isGPSEnabled(mapLocationActivity.getContext());
        Assert.assertTrue("should be true: ", gps);
    }

    @Test
    public void disabledGPSTest() throws Exception {
        shadowLocationManager.setProviderEnabled(LocationManager.GPS_PROVIDER, false);
        boolean gps = Util.isGPSEnabled(mapLocationActivity.getContext());
        Assert.assertFalse("should be false: ", gps);
    }
}