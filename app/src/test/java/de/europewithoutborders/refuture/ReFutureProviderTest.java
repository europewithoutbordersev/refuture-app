package de.europewithoutborders.refuture;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowContentResolver;

import de.europewithoutborders.refuture.data.ReFutureContract;
import de.europewithoutborders.refuture.data.ReFutureContract.CountryEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.CountryLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.NewsLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeEntry;
import de.europewithoutborders.refuture.data.ReFutureContract.PoiTypeLocalizationEntry;
import de.europewithoutborders.refuture.data.ReFutureProvider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Unit test for ReFutureProvider.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ReFutureProviderTest {

    private ContentResolver mContentResolver;
    private ShadowContentResolver mShadowContentResolver;
    private ReFutureProvider mProvider;

    @Before
    public void setup() {
        mProvider = new ReFutureProvider();
        mContentResolver = RuntimeEnvironment.application.getContentResolver();
        mShadowContentResolver = Shadows.shadowOf(mContentResolver);
        mProvider.onCreate();
        ShadowContentResolver.registerProvider(ReFutureContract.CONTENT_AUTHORITY, mProvider);
    }

    /**
     * Tests an insert into Country table
     */
    @Test
    public void testCountryInsert() {
        Uri uri = CountryEntry.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put(CountryEntry.COLUMN_CODE, "de");
        values.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 6);
        values.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 5.3);
        values.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, true);
        Uri resultingUri = mContentResolver.insert(uri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        values.clear();
        values.put(CountryLocalizationEntry.COLUMN_COUNTRY_CODE, "de");
        values.put(CountryLocalizationEntry.COLUMN_NAME, "Deutschland");
        values.put(CountryLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(CountryLocalizationEntry.COLUMN_LODGING_CAPACITY, "drölf tausend");
        values.put(CountryLocalizationEntry.COLUMN_CAPACITY, "fümf");
        values.put(CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT, "work");
        values.put(CountryLocalizationEntry.COLUMN_REGULATION_INFLUX, "kommen nach");

        resultingUri = mContentResolver.insert(CountryLocalizationEntry.CONTENT_URI, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
    }

    /**
     * Tests a bulk insert into Country table with 2 new entries
     */
    @Test
    public void testCountryBulkInsert() {
        Uri uri = CountryEntry.CONTENT_URI;
        ContentValues values1 = new ContentValues();
        values1.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 6);
        values1.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 5.3);
        values1.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, true);

        ContentValues values2 = new ContentValues();
        values2.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 7);
        values2.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 8.1);
        values2.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, false);
        ContentValues[] values = new ContentValues[] {values1, values2};

        int rows = mContentResolver.bulkInsert(uri, values);
        assertEquals(rows, 2);
    }

    /**
     * Tests a delete from the empty Country table
     */
    @Test
    public void testCountryDeleteEmptyTable() {
        Uri uri = CountryEntry.CONTENT_URI;
        int rows = mContentResolver.delete(uri, CountryEntry.COLUMN_CODE + " = ?", new String[]{"de"});
        assertEquals(0, rows);
    }

    /**
     * Tests a delete from Education table.
     * First an entry will be added (code: de), which will later be deleted.
     */
    @Test
    public void testCountryDeleteNotEmptyTable() {
        // insert row with id: 42
        Uri uri = CountryEntry.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put(CountryEntry.COLUMN_CODE, "de");
        values.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 6);
        values.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 5.3);
        values.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, true);
        Uri resultingUri = mContentResolver.insert(uri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        // row inserted, now let's delete it
        int rows = mShadowContentResolver.delete(uri, CountryEntry.COLUMN_CODE + " = ?", new String[]{"de"});
        assertEquals(1, rows);
    }

    /**
     * Tests a query from the empty Country table.
     */
    @Test
    public void testCountryQueryEmptyTable() {
        Uri uri = CountryEntry.CONTENT_URI;
        String[] projection = {CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_CODE,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LANGUAGE,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_NAME,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_CAPACITY,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LODGING_CAPACITY,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_PROCEEDING_DURATION,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REGULATION_INFLUX,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_UNEMPLOYMENT_RATE};
        Cursor cursor = mContentResolver.query(uri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(cursor.getCount(), 0);
        assertFalse(cursor.moveToFirst());
    }

    /**
     * Tests a query from Country table.
     * First an entry will be added (id: 42), which will later be queried.
     */
    @Test
    public void testCountryQueryNotEmptyTable() {
        Uri uri = CountryEntry.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put(CountryEntry.COLUMN_CODE, "de");
        values.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 6);
        values.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 5.3);
        values.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, true);
        Uri resultingUri = mContentResolver.insert(uri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        values.clear();
        values.put(CountryLocalizationEntry.COLUMN_COUNTRY_CODE, "de");
        values.put(CountryLocalizationEntry.COLUMN_NAME, "Deutschland");
        values.put(CountryLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(CountryLocalizationEntry.COLUMN_LODGING_CAPACITY, "drölf tausend");
        values.put(CountryLocalizationEntry.COLUMN_CAPACITY, "fümf");
        values.put(CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT, "work");
        values.put(CountryLocalizationEntry.COLUMN_REGULATION_INFLUX, "kommen nach");

        resultingUri = mContentResolver.insert(CountryLocalizationEntry.CONTENT_URI, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        // row inserted, now let's query it
        String[] projection = {CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_CODE,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LANGUAGE,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_NAME,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_CAPACITY,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LODGING_CAPACITY,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_PROCEEDING_DURATION,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REGULATION_INFLUX,
                CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT,
                CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_UNEMPLOYMENT_RATE};
        Cursor cursor = mShadowContentResolver.query(uri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(cursor.getCount(), 1);
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_CODE));
        assertEquals(1, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(2, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_NAME));
        assertEquals(3, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_CAPACITY));
        assertEquals(4, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_LODGING_CAPACITY));
        assertEquals(5, cursor.getColumnIndexOrThrow(CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED));
        assertEquals(6, cursor.getColumnIndexOrThrow(CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_PROCEEDING_DURATION));
        assertEquals(7, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REGULATION_INFLUX));
        assertEquals(8, cursor.getColumnIndexOrThrow(CountryLocalizationEntry.TABLE_NAME + "." + CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT));
        assertEquals(9, cursor.getColumnIndexOrThrow(CountryEntry.TABLE_NAME + "." + CountryEntry.COLUMN_UNEMPLOYMENT_RATE));
        assertEquals("de", cursor.getString(0));
        assertEquals("de", cursor.getString(1));
        assertEquals("Deutschland", cursor.getString(2));
        assertEquals("fümf", cursor.getString(3));
        assertEquals("drölf tausend", cursor.getString(4));
        assertEquals(1, cursor.getInt(5));
        assertEquals(6, cursor.getInt(6));
        assertEquals("kommen nach", cursor.getString(7));
        assertEquals("work", cursor.getString(8));
        assertEquals(5.3f, cursor.getFloat(9), 0.001);
    }

    /**
     * Tests an insert into News table
     * Because of foreign key constraint, we need to insert a country first..
     */
    @Test
    public void testNewsInsert() {
        Uri uri = CountryEntry.CONTENT_URI;
        ContentValues values = new ContentValues();
        values.put(CountryEntry.COLUMN_CODE, "de");
        values.put(CountryEntry.COLUMN_PROCEEDING_DURATION, 6);
        values.put(CountryEntry.COLUMN_UNEMPLOYMENT_RATE, 5.3);
        values.put(CountryEntry.COLUMN_NATIVE_LANGUAGE_REQUIRED, true);
        Uri resultingUri = mContentResolver.insert(uri, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        values.clear();
        values.put(CountryLocalizationEntry.COLUMN_COUNTRY_CODE, "de");
        values.put(CountryLocalizationEntry.COLUMN_NAME, "Deutschland");
        values.put(CountryLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(CountryLocalizationEntry.COLUMN_LODGING_CAPACITY, "drölf tausend");
        values.put(CountryLocalizationEntry.COLUMN_CAPACITY, "fümf");
        values.put(CountryLocalizationEntry.COLUMN_REQUIREMENTS_WORK_PERMIT, "work");
        values.put(CountryLocalizationEntry.COLUMN_REGULATION_INFLUX, "kommen nach");

        resultingUri = mContentResolver.insert(CountryLocalizationEntry.CONTENT_URI, values);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);

        Uri newsUri = NewsEntry.CONTENT_URI;
        ContentValues newsValues = new ContentValues();
        newsValues.put(NewsEntry.COLUMN_ID, 13);
        newsValues.put(NewsEntry.COLUMN_COUNTRY, "de");
        newsValues.put(NewsEntry.COLUMN_TIMESTAMP, 1231235698743L);
        resultingUri = mShadowContentResolver.insert(newsUri, newsValues);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        Uri localUri = NewsLocalizationEntry.CONTENT_URI;
        ContentValues localValues = new ContentValues();
        localValues.put(NewsLocalizationEntry.COLUMN_LANGUAGE, "de");
        localValues.put(NewsLocalizationEntry.COLUMN_HEADING, "head");
        localValues.put(NewsLocalizationEntry.COLUMN_BODY, "body");
        localValues.put(NewsLocalizationEntry.COLUMN_NEWS_ID, id);
        resultingUri = mShadowContentResolver.insert(localUri, localValues);
        assertNotNull(resultingUri);
    }

    /**
     * Tests an insert into News table without a country
     */
    @Test
    public void testNewsInsertWithoutCountry() {
        Uri newsUri = NewsEntry.CONTENT_URI;
        ContentValues newsValues = new ContentValues();
        newsValues.put(NewsEntry.COLUMN_ID, 13);
        newsValues.put(NewsEntry.COLUMN_LATITUDE, 47.54214);
        newsValues.put(NewsEntry.COLUMN_LONGITUDE, 9.54871);
        newsValues.put(NewsEntry.COLUMN_TIMESTAMP, System.currentTimeMillis());
        Uri resultingUri = mContentResolver.insert(newsUri, newsValues);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(13, id);
    }

    /**
     * Tests an insert into News table with no ID given
     */
    @Test
    public void testNewsInsertNoId() {
        // Country is inserted, move on to News...
        Uri newsUri = NewsEntry.CONTENT_URI;
        ContentValues newsValues = new ContentValues();
        newsValues.put(NewsEntry.COLUMN_TIMESTAMP, 87648532093L);
        Uri resultingUri = mShadowContentResolver.insert(newsUri, newsValues);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);
    }

    /**
     * Tests a bulk insert into News table with 2 new entries
     */
    @Test
    public void testNewsBulkInsert() {
        // Country is inserted, move on to News...
        Uri newsUri = NewsEntry.CONTENT_URI;

        ContentValues newsValues1 = new ContentValues();
        newsValues1.put(NewsEntry.COLUMN_TIMESTAMP, 1231231233123123L);

        ContentValues newsValues2 = new ContentValues();
        newsValues2.put(NewsEntry.COLUMN_TIMESTAMP, 12312319845123L);
        ContentValues[] values = new ContentValues[] {newsValues1, newsValues2};

        int rows = mShadowContentResolver.bulkInsert(newsUri, values);
        assertEquals(rows, 2);
    }

    /**
     * Tests a delete from the empty News table
     */
    @Test
    public void testNewsDeleteEmptyTable() {
        Uri uri = NewsEntry.CONTENT_URI;
        int rows = mContentResolver.delete(uri, NewsEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(42)});
        assertEquals(0, rows);
    }

    /**
     * Tests a delete from News table.
     * First an entry will be added (id: 42), which will later be deleted.
     * Second, the news entry should still be available in the db
     */
    @Test
    public void testNewsDeleteNotEmptyTable() {
        // Country is inserted, move on to News...
        // insert row with id: 42
        Uri newsUri = NewsEntry.CONTENT_URI;
        ContentValues newsValues = new ContentValues();
        newsValues.put(NewsEntry.COLUMN_ID, 42);
        newsValues.put(NewsEntry.COLUMN_TIMESTAMP, 1231231233L);
        Uri resultingUri = mShadowContentResolver.insert(newsUri, newsValues);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 42);

        // row inserted, now let's delete it
        int rows = mShadowContentResolver.delete(newsUri, NewsEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(42)});
        assertEquals(1, rows);

    }

    /**
     * Tests a query from the empty News table.
     */
    @Test
    public void testNewsQueryEmptyTable() {
        Uri uri = NewsEntry.CONTENT_URI;
        String[] projection = {NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_ID,
                NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_COUNTRY,
                NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_TIMESTAMP};
        Cursor cursor = mContentResolver.query(uri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(cursor.getCount(), 0);
        assertFalse(cursor.moveToFirst());
    }

    /**
     * Tests a query from News table.
     * Firstly a country entry will be added (id: 42).
     * Secondly a news will be added, which will later be queried.
     */
    @Test
    public void testNewsQueryNotEmptyTable() {
        // Country is inserted, move on to News...
        Uri newsUri = NewsEntry.CONTENT_URI;
        ContentValues newsValues = new ContentValues();
        newsValues.put(NewsEntry.COLUMN_TIMESTAMP, 876389484L);
        Uri resultingUri = mShadowContentResolver.insert(newsUri, newsValues);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        Uri localUri = NewsLocalizationEntry.CONTENT_URI;
        ContentValues localValues = new ContentValues();
        localValues.put(NewsLocalizationEntry.COLUMN_NEWS_ID, id);
        localValues.put(NewsLocalizationEntry.COLUMN_LANGUAGE, "de");
        localValues.put(NewsLocalizationEntry.COLUMN_HEADING, "head");
        localValues.put(NewsLocalizationEntry.COLUMN_BODY, "body");
        resultingUri = mShadowContentResolver.insert(localUri, localValues);
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        // row inserted, now let's query it
        String[] projection = {NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_ID,
                NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_LANGUAGE,
                NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_HEADING,
                NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_BODY,
                NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_TIMESTAMP};
        Cursor cursor = mShadowContentResolver.query(newsUri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(1, cursor.getCount());
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_ID));
        assertEquals(1, cursor.getColumnIndexOrThrow(NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(2, cursor.getColumnIndexOrThrow(NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_HEADING));
        assertEquals(3, cursor.getColumnIndexOrThrow(NewsLocalizationEntry.TABLE_NAME + "." + NewsLocalizationEntry.COLUMN_BODY));
        assertEquals(4, cursor.getColumnIndexOrThrow(NewsEntry.TABLE_NAME + "." + NewsEntry.COLUMN_TIMESTAMP));
        assertEquals(id, cursor.getInt(0));
        assertEquals("de", cursor.getString(1));
        assertEquals("head", cursor.getString(2));
        assertEquals("body", cursor.getString(3));
        assertEquals(876389484L, cursor.getLong(4));
    }

    /**
     * Tests a delete from the empty PoiType table
     */
    @Test
    public void testPoiTypeDeleteEmptyTable() {
        Uri uri = PoiTypeEntry.CONTENT_URI;
        int rows = mContentResolver.delete(uri, PoiTypeEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(42)});
        assertEquals(0, rows);
    }

    /**
     * Tests a delete from PoiType table.
     * First an entry will be added (id: 42), which will later be deleted.
     */
    @Test
    public void testPoiTypeDeleteNotEmptyTable2() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        boolean errorThrown = false;
        try {
            // row inserted, now let's delete it
            // shouldn't work because of foreign key constraint
            int rows = mShadowContentResolver.delete(PoiTypeEntry.CONTENT_URI, PoiTypeEntry.COLUMN_ID + " = ?", new String[]{"housing"});
            fail("Foreign-Key Constraint erlaubt es nicht einen POI zu löschen, von dem es noch Übersetzungen gibt!");
        } catch(SQLiteException sqle) {
            errorThrown = true;
        } finally {
            assertTrue(errorThrown);
        }

    }

    /**
     * Tests a query from the empty PoiType table.
     */
    @Test
    public void testPoiTypeQueryEmptyTable() {
        Uri uri = PoiTypeEntry.CONTENT_URI;
        String[] projection = {PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID};
        Cursor cursor = mContentResolver.query(uri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(0, cursor.getCount());
        assertFalse(cursor.moveToFirst());
    }

    /**
     * Tests a query from PoiType table.
     * First an entry will be added (id: 42), which will later be queried.
     */
    @Test
    public void testPoiTypeQueryNotEmptyTable() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        // row inserted, now let's query it
        String[] projection = {PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION};
        Cursor cursor = mShadowContentResolver.query(PoiTypeEntry.CONTENT_URI, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(cursor.getCount(), 1);
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID));
        assertEquals(1, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(2, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME));
        assertEquals(3, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION));
        assertEquals("housing", cursor.getString(0));
        assertEquals("de", cursor.getString(1));
        assertEquals("Konsulat", cursor.getString(2));
        assertEquals("Ein ganz tolles Haus.", cursor.getString(3));
    }

    /**
     * Tests an insert into Poi table
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiInsert() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_ID, 13);
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        Uri poiLocalUri = PoiLocalizationEntry.CONTENT_URI;
        ContentValues poiLocalVals = new ContentValues();
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_POI_ID, 13);
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_NAME, "Stuttgarter Rathaus");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_DESCRIPTION, "Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_CAPACITY, "Treffen sich zwei Beamte...");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_LANGUAGE, "de");

        resultingUri = mContentResolver.insert(poiLocalUri, poiLocalVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);
    }


    /**
     * Tests an insert into Poi table with no ID given
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiInsertNoId() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        Uri poiLocalUri = PoiLocalizationEntry.CONTENT_URI;
        ContentValues poiLocalVals = new ContentValues();
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_POI_ID, id);
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_NAME, "Stuttgarter Rathaus");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_DESCRIPTION, "Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_CAPACITY, "Treffen sich zwei Beamte...");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_LANGUAGE, "de");

        resultingUri = mContentResolver.insert(poiLocalUri, poiLocalVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);
    }

    /**
     * Tests a bulk insert into Poi table with 2 new entries
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiBulkInsert() {
        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals1 = new ContentValues();
        poiVals1.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals1.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals1.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals1.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals1.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);

        ContentValues poiVals2 = new ContentValues();
        poiVals2.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals2.put(PoiEntry.COLUMN_LATITUDE, 48.7783206);
        poiVals2.put(PoiEntry.COLUMN_LONGITUDE, 9.1605733);
        poiVals2.put(PoiEntry.COLUMN_RATIO, 73.31);
        poiVals2.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, true);

        ContentValues[] vals = new ContentValues[] {poiVals1, poiVals2};

        int rows = mContentResolver.bulkInsert(poiUri, vals);
        assertEquals(rows, 2);
    }

    /**
     * Tests a delete from the empty Poi table
     */
    @Test
    public void testPoiDeleteEmptyTable() {
        Uri uri = PoiEntry.CONTENT_URI;
        int rows = mContentResolver.delete(uri, PoiEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(42)});
        assertEquals(0, rows);
    }

    /**
     * Tests a delete from Poi table.
     * First an entry will be added (id: 13), which will later be deleted.
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiDeleteNotEmptyTable() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_ID, 13);
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        // row inserted, now let's delete it
        int rows = mShadowContentResolver.delete(poiUri, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(13)});
        assertEquals(1, rows);

        // Bonus: check if PoiType with ID: 42 still exists...
        String[] projection = {PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION};
        Cursor cursor = mShadowContentResolver.query(PoiTypeEntry.CONTENT_URI, projection, PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID + " = ?", new String[]{"housing"}, null);
        assertNotNull(cursor);
        assertEquals(1, cursor.getCount());
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID));
        assertEquals(1, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(2, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME));
        assertEquals(3, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION));
        assertEquals("housing", cursor.getString(0));
        assertEquals("de", cursor.getString(1));
        assertEquals("Konsulat", cursor.getString(2));
        assertEquals("Ein ganz tolles Haus.", cursor.getString(3));
    }

    /**
     * Tests a delete from Poi table.
     * First an entry will be added (id: 13), which will later be deleted.
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiDeleteNotEmptyTable2() {
        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_ID, 13);
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        Uri resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        Uri poiLocalUri = PoiLocalizationEntry.CONTENT_URI;
        ContentValues poiLocalVals = new ContentValues();
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_POI_ID, id);
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_NAME, "Stuttgarter Rathaus");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_DESCRIPTION, "Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_CAPACITY, "Treffen sich zwei Beamte...");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_LANGUAGE, "de");

        resultingUri = mContentResolver.insert(poiLocalUri, poiLocalVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        boolean errorThrown = false;
        try {
            // row inserted, now let's delete it
            int rows = mShadowContentResolver.delete(poiUri, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID + " = ?", new String[]{String.valueOf(13)});
            fail("Foreign-Key Constraint erlaubt es nicht einen POI zu löschen, von dem es noch Übersetzungen gibt!");
        } catch(SQLiteException sqle) {
            errorThrown = true;
        } finally {
            assertTrue(errorThrown);
        }
    }


    /**
     * Tests a query from the empty Poi table.
     */
    @Test
    public void testPoiQueryEmptyTable() {
        Uri uri = PoiEntry.CONTENT_URI;
        String[] projection = {PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_TYPE_ID,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_NAME, PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_DESCRIPTION,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_RATIO, PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_CAPACITY, PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID, PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_NAME, PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_DESCRIPTION};
        Cursor cursor = mContentResolver.query(uri, projection, null, null, null);
        assertNotNull(cursor);
        assertEquals(0, cursor.getCount());
        assertFalse(cursor.moveToFirst());
    }

    /**
     * Tests a query from Poi table.
     * First an entry will be added (id: 42), which will later be queried.
     * Because of foreign key constraint, we need to insert a PoiType first..
     */
    @Test
    public void testPoiQueryNotEmptyTable() {
        ContentValues values = new ContentValues();

        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_ID, 13);
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        Uri poiLocalUri = PoiLocalizationEntry.CONTENT_URI;
        ContentValues poiLocalVals = new ContentValues();
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_POI_ID, id);
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_NAME, "Stuttgarter Rathaus");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_DESCRIPTION, "Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_CAPACITY, "Treffen sich zwei Beamte...");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_LANGUAGE, "de");

        resultingUri = mContentResolver.insert(poiLocalUri, poiLocalVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        // row inserted, now let's query it
        String[] projection = {PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_ID,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_TYPE_ID + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_TYPE_ID,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_NAME + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_NAME,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_DESCRIPTION + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_DESCRIPTION,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LATITUDE,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LONGITUDE,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_RATIO + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_RATIO,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_CAPACITY + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_CAPACITY,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID + " as " + PoiTypeEntry.TABLE_NAME + PoiTypeEntry.COLUMN_ID,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_NAME,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION};
        Cursor cursor = mShadowContentResolver.query(poiUri, projection, null, null, null);

        assertNotNull(cursor);
        assertEquals(1, cursor.getCount());
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_ID));
        assertEquals(1, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_TYPE_ID));
        assertEquals(2, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_NAME));
        assertEquals(3, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_DESCRIPTION));
        assertEquals(4, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LATITUDE));
        assertEquals(5, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LONGITUDE));
        assertEquals(6, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_RATIO));
        assertEquals(7, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT));
        assertEquals(8, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_CAPACITY));
        assertEquals(9, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(10, cursor.getColumnIndexOrThrow(PoiTypeEntry.TABLE_NAME + PoiTypeEntry.COLUMN_ID));
        assertEquals(11, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(12, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_NAME));
        assertEquals(13, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION));

        assertEquals(13, cursor.getInt(0));
        assertEquals("housing", cursor.getString(1));
        assertEquals("Stuttgarter Rathaus", cursor.getString(2));
        assertEquals("Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.", cursor.getString(3));
        assertEquals(48.7750705, cursor.getFloat(4), 0.001);
        assertEquals(9.1779613, cursor.getFloat(5), 0.001);
        assertEquals(13.37, cursor.getFloat(6), 0.001);
        assertEquals(0, cursor.getInt(7));
        assertEquals("Treffen sich zwei Beamte...", cursor.getString(8));
        assertEquals("de", cursor.getString(9));
        assertEquals("housing", cursor.getString(10));
        assertEquals("de", cursor.getString(11));
        assertEquals("Konsulat", cursor.getString(12));
        assertEquals("Ein ganz tolles Haus.", cursor.getString(13));
    }

    /**
     * Tests a query from Poi table.
     * First an entry will be added (id: 42), which will later be queried.
     * Because of foreign key constraint, we need to insert a PoiType first..
     * The URI for this query looks like: content://de.europewithoutborders.refuture/poi/*id*
     */
    @Test
    public void testPoiQueryNotEmptyTable2() {
        ContentValues values = new ContentValues();
        values.put(PoiTypeLocalizationEntry.COLUMN_POI_TYPE_ID, "housing");
        values.put(PoiTypeLocalizationEntry.COLUMN_LANGUAGE, "de");
        values.put(PoiTypeLocalizationEntry.COLUMN_NAME, "Konsulat");
        values.put(PoiTypeLocalizationEntry.COLUMN_DESCRIPTION, "Ein ganz tolles Haus.");
        Uri resultingUri = mContentResolver.insert(PoiTypeLocalizationEntry.CONTENT_URI, values);
        assertNotNull(resultingUri);

        Uri poiUri = PoiEntry.CONTENT_URI;
        ContentValues poiVals = new ContentValues();
        poiVals.put(PoiEntry.COLUMN_ID, 13);
        poiVals.put(PoiEntry.COLUMN_TYPE_ID, "housing");
        poiVals.put(PoiEntry.COLUMN_LATITUDE, 48.7750705);
        poiVals.put(PoiEntry.COLUMN_LONGITUDE, 9.1779613);
        poiVals.put(PoiEntry.COLUMN_RATIO, 13.37);
        poiVals.put(PoiEntry.COLUMN_CREATED_BY_GOVERNMENT, false);
        resultingUri = mContentResolver.insert(poiUri, poiVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        long id = ContentUris.parseId(resultingUri);
        assertEquals(id, 13);

        Uri poiLocalUri = PoiLocalizationEntry.CONTENT_URI;
        ContentValues poiLocalVals = new ContentValues();
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_POI_ID, id);
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_NAME, "Stuttgarter Rathaus");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_DESCRIPTION, "Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_CAPACITY, "Treffen sich zwei Beamte...");
        poiLocalVals.put(PoiLocalizationEntry.COLUMN_LANGUAGE, "de");

        resultingUri = mContentResolver.insert(poiLocalUri, poiLocalVals);
        // Then you can test the correct execution of your insert:
        assertNotNull(resultingUri);
        id = ContentUris.parseId(resultingUri);
        assertTrue(id > 0);

        // row inserted, now let's query it
        String[] projection = {PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_ID + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_ID,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_TYPE_ID + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_TYPE_ID,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_NAME + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_NAME,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_DESCRIPTION + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_DESCRIPTION,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LATITUDE + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LATITUDE,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_LONGITUDE + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LONGITUDE,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_RATIO + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_RATIO,
                PoiEntry.TABLE_NAME + "." + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT + " as " + PoiEntry.TABLE_NAME + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_CAPACITY + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_CAPACITY,
                PoiLocalizationEntry.TABLE_NAME + "." + PoiLocalizationEntry.COLUMN_LANGUAGE + " as " + PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeEntry.TABLE_NAME + "." + PoiTypeEntry.COLUMN_ID + " as " + PoiTypeEntry.TABLE_NAME + PoiTypeEntry.COLUMN_ID,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_LANGUAGE + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_LANGUAGE,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_NAME + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_NAME,
                PoiTypeLocalizationEntry.TABLE_NAME + "." + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION + " as " + PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION};
        Cursor cursor = mShadowContentResolver.query(PoiEntry.buildUri(13), projection, null, null, null);

        assertNotNull(cursor);
        assertEquals(1, cursor.getCount());
        assertTrue(cursor.moveToFirst());
        assertEquals(0, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_ID));
        assertEquals(1, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_TYPE_ID));
        assertEquals(2, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_NAME));
        assertEquals(3, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_DESCRIPTION));
        assertEquals(4, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LATITUDE));
        assertEquals(5, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_LONGITUDE));
        assertEquals(6, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_RATIO));
        assertEquals(7, cursor.getColumnIndexOrThrow(PoiEntry.TABLE_NAME + PoiEntry.COLUMN_CREATED_BY_GOVERNMENT));
        assertEquals(8, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_CAPACITY));
        assertEquals(9, cursor.getColumnIndexOrThrow(PoiLocalizationEntry.TABLE_NAME + PoiLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(10, cursor.getColumnIndexOrThrow(PoiTypeEntry.TABLE_NAME + PoiTypeEntry.COLUMN_ID));
        assertEquals(11, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_LANGUAGE));
        assertEquals(12, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_NAME));
        assertEquals(13, cursor.getColumnIndexOrThrow(PoiTypeLocalizationEntry.TABLE_NAME + PoiTypeLocalizationEntry.COLUMN_DESCRIPTION));

        assertEquals(13, cursor.getInt(0));
        assertEquals("housing", cursor.getString(1));
        assertEquals("Stuttgarter Rathaus", cursor.getString(2));
        assertEquals("Das Stuttgarter Rathaus steht in Stuttgart-Mitte und ist Sitz der Stuttgarter Stadtverwaltung.", cursor.getString(3));
        assertEquals(48.7750705, cursor.getFloat(4), 0.001);
        assertEquals(9.1779613, cursor.getFloat(5), 0.001);
        assertEquals(13.37, cursor.getFloat(6), 0.001);
        assertEquals(0, cursor.getInt(7));
        assertEquals("Treffen sich zwei Beamte...", cursor.getString(8));
        assertEquals("de", cursor.getString(9));
        assertEquals("housing", cursor.getString(10));
        assertEquals("de", cursor.getString(11));
        assertEquals("Konsulat", cursor.getString(12));
        assertEquals("Ein ganz tolles Haus.", cursor.getString(13));
    }
}
