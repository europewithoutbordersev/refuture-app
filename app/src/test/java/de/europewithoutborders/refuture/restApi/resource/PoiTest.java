package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import de.europewithoutborders.refuture.BuildConfig;
import de.europewithoutborders.refuture.util.Constants;

import static junit.framework.Assert.assertEquals;

/**
 * Unit test for Poi.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class PoiTest {

    private String poiJson = "{\"locations\":[{\"id\":1,\"latitude\":60.442423,\"longitude\":22.26044,\"localizations\":[{\"id\": 40, \"language\":\"en\",\"name\":\"test+poi\",\"description\":\"poi+for+testing\",\"capacity\":\"\"},{\"id\": 41, \"language\":\"de\",\"name\":\"Test+Ort\",\"description\":\"Ort+zum+Testen\",\"capacity\":\"\"}],\"type\":\"housing\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Unterbringung\"},{\"language\":\"en\",\"name\":\"Housing\"}]}]}";

    @Test
    public void createPoiTest() throws JSONException {
        JSONObject json = new JSONObject(poiJson);
        Poi poi = new Poi(json.getJSONArray(Constants.JSON_POI).getJSONObject(0));
        assertEquals(poi.getId(), 1);
        assertEquals(poi.getLatitude(), 60.442423);
        assertEquals(poi.getLongitude(), 22.26044);
        assertEquals(poi.getRatio(), -1);
        assertEquals(poi.getLocalizations().get(41).get(0), "de");
        assertEquals(poi.getLocalizations().get(41).get(1), "Test+Ort");
        assertEquals(poi.getLocalizations().get(41).get(2), "Ort+zum+Testen");
        assertEquals(poi.getLocalizations().get(41).get(3), "");
        assertEquals(poi.getLocalizations().get(40).get(0), "en");
        assertEquals(poi.getLocalizations().get(40).get(1), "test+poi");
        assertEquals(poi.getLocalizations().get(40).get(2), "poi+for+testing");
        assertEquals(poi.getLocalizations().get(40).get(3), "");
        assertEquals(poi.getType(), "housing");
        assertEquals(poi.getTypeLocalizations().get("en"), "Housing");
        assertEquals(poi.getTypeLocalizations().get("de"), "Unterbringung");
    }
}
