package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import java.util.HashMap;

import de.europewithoutborders.refuture.BuildConfig;

import static junit.framework.Assert.assertEquals;

/**
 * Unit test for Countries.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class CountriesTest {

    private String countriesJson = "{\"countries\":[{\"country_code\":\"AD\",\"localizations\":[]},{\"country_code\":\"AT\",\"localizations\":[]},{\"country_code\":\"BE\",\"localizations\":[]},{\"country_code\":\"CY\",\"localizations\":[]},{\"country_code\":\"CZ\",\"localizations\":[]},{\"country_code\":\"DE\",\"localizations\":[{\"lang\":\"en\",\"name\":\"Germany\"}]},{\"country_code\":\"DK\",\"localizations\":[]},{\"country_code\":\"HR\",\"localizations\":[]}]}";

    @Test
    public void createCountriesTest() throws JSONException {
        JSONObject json = new JSONObject(countriesJson);
        Countries countries = new Countries(json);
        assertEquals(countries.getCountries().get("AD"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("AT"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("BE"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("CY"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("CZ"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("DE").get("en"), "Germany");
        assertEquals(countries.getCountries().get("DK"), new HashMap<String, String>());
        assertEquals(countries.getCountries().get("HR"), new HashMap<String, String>());
    }
}
