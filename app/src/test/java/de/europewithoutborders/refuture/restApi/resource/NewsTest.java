package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import de.europewithoutborders.refuture.BuildConfig;
import de.europewithoutborders.refuture.util.Constants;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Unit test for News.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class NewsTest {

    private String globalNewsJson = "{\"news\":[{\"id\":4,\"timestamp\":1451989479000,\"localizations\":[{\"id\": 30, \"language\":\"fr\",\"heading\":\"French+Titel\",\"body\":\"French+Nachricht\"},{\"id\": 31, \"language\":\"en\",\"heading\":\"English+Title\",\"body\":\"English+Message\"},{\"id\": 32, \"language\":\"de\",\"heading\":\"Deutscher+Titel\",\"body\":\"Deutscher+Text\"}]}]}";
    private String countryNewsJson = "{\"news\":[{\"id\":4,\"country_code\": \"de\",\"timestamp\":1451989479000,\"localizations\":[{\"id\": 33, \"language\":\"fr\",\"heading\":\"French+Titel\",\"body\":\"French+Nachricht\"},{\"id\": 34, \"language\":\"en\",\"heading\":\"English+Title\",\"body\":\"English+Message\"},{\"id\": 35, \"language\":\"de\",\"heading\":\"Deutscher+Titel\",\"body\":\"Deutscher+Text\"}]}]}";
    private String locationNewsJson = "{\"news\":[{\"id\":4,\"latitude\": 47.12345,\"longitude\": 42.12345,\"timestamp\":1451989479000,\"localizations\":[{\"id\": 36, \"language\":\"fr\",\"heading\":\"French+Titel\",\"body\":\"French+Nachricht\"},{\"id\": 37, \"language\":\"en\",\"heading\":\"English+Title\",\"body\":\"English+Message\"},{\"id\": 38, \"language\":\"de\",\"heading\":\"Deutscher+Titel\",\"body\":\"Deutscher+Text\"}]}]}";

    @Test
    public void createGlobalNewsTest() throws JSONException {
            JSONObject json = new JSONObject(globalNewsJson);
            News news = new News(json.getJSONArray(Constants.JSON_NEWS).getJSONObject(0));
            assertEquals(news.getId(), 4);
            assertEquals(news.getTimestamp(), 1451989479000L);
            assertEquals(news.getLatitude(), 0.0);
            assertEquals(news.getLongitude(), 0.0);
            assertNull(news.getCountryCode());
            assertEquals(news.getNewsType(), News.NewsType.GLOBAL_NEWS);
            assertEquals(news.getLocalizations().get(30).get(0), "fr");
            assertEquals(news.getLocalizations().get(30).get(1), "French+Titel");
            assertEquals(news.getLocalizations().get(30).get(2), "French+Nachricht");
            assertEquals(news.getLocalizations().get(31).get(0), "en");
            assertEquals(news.getLocalizations().get(31).get(1), "English+Title");
            assertEquals(news.getLocalizations().get(31).get(2), "English+Message");
            assertEquals(news.getLocalizations().get(32).get(0), "de");
            assertEquals(news.getLocalizations().get(32).get(1), "Deutscher+Titel");
            assertEquals(news.getLocalizations().get(32).get(2), "Deutscher+Text");
    }

    @Test
    public void createCountryNewsTest() throws JSONException {
        JSONObject json = new JSONObject(countryNewsJson);
        News news = new News(json.getJSONArray(Constants.JSON_NEWS).getJSONObject(0));
        assertEquals(news.getId(), 4);
        assertEquals(news.getTimestamp(), 1451989479000L);
        assertEquals(news.getLatitude(), 0.0);
        assertEquals(news.getLongitude(), 0.0);
        assertEquals(news.getCountryCode(), "de");
        assertEquals(news.getNewsType(), News.NewsType.COUNTRY_NEWS);
        assertEquals(news.getLocalizations().get(33).get(0), "fr");
        assertEquals(news.getLocalizations().get(33).get(1), "French+Titel");
        assertEquals(news.getLocalizations().get(33).get(2), "French+Nachricht");
        assertEquals(news.getLocalizations().get(34).get(0), "en");
        assertEquals(news.getLocalizations().get(34).get(1), "English+Title");
        assertEquals(news.getLocalizations().get(34).get(2), "English+Message");
        assertEquals(news.getLocalizations().get(35).get(0), "de");
        assertEquals(news.getLocalizations().get(35).get(1), "Deutscher+Titel");
        assertEquals(news.getLocalizations().get(35).get(2), "Deutscher+Text");
    }

    @Test
    public void createLocationNewsTest() throws JSONException {
        JSONObject json = new JSONObject(locationNewsJson);
        News news = new News(json.getJSONArray(Constants.JSON_NEWS).getJSONObject(0));
        assertEquals(news.getId(), 4);
        assertEquals(news.getTimestamp(), 1451989479000L);
        assertEquals(news.getLatitude(), 47.12345);
        assertEquals(news.getLongitude(), 42.12345);
        assertNull(news.getCountryCode());
        assertEquals(news.getNewsType(), News.NewsType.LOCATION_NEWS);
        assertEquals(news.getLocalizations().get(36).get(0), "fr");
        assertEquals(news.getLocalizations().get(36).get(1), "French+Titel");
        assertEquals(news.getLocalizations().get(36).get(2), "French+Nachricht");
        assertEquals(news.getLocalizations().get(37).get(0), "en");
        assertEquals(news.getLocalizations().get(37).get(1), "English+Title");
        assertEquals(news.getLocalizations().get(37).get(2), "English+Message");
        assertEquals(news.getLocalizations().get(38).get(0), "de");
        assertEquals(news.getLocalizations().get(38).get(1), "Deutscher+Titel");
        assertEquals(news.getLocalizations().get(38).get(2), "Deutscher+Text");
    }
}
