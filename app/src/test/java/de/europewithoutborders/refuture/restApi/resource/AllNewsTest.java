package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import de.europewithoutborders.refuture.BuildConfig;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Unit test for AllNews.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class AllNewsTest {

    private String allNewsJson = "{\"news\":[{\"id\":2,\"latitude\":60.442423,\"longitude\":22.26044,\"timestamp\":1451400418000,\"localizations\":[{\"id\": 12, \"language\":\"de\",\"heading\":\"Test+Nachricht\",\"body\":\"Dies+ist+eine+Testnachricht\"}]},{\"id\":4,\"timestamp\":1451989479000,\"localizations\":[{\"id\": 16, \"language\":\"ar\",\"heading\":\"Arabic+Titel\",\"body\":\"Arabic+Nachricht\"},{\"id\": 17, \"language\":\"fr\",\"heading\":\"French+Titel\",\"body\":\"French+Nachricht\"},{\"id\": 18, \"language\":\"en\",\"heading\":\"English+Title\",\"body\":\"English+Message\"},{\"id\": 19, \"language\":\"de\",\"heading\":\"Deutscher+Titel\",\"body\":\"Deutscher+Text\"}]},{\"id\":6,\"country_code\":\"DE\",\"timestamp\":1452198210000,\"localizations\":[{\"id\": 20, \"language\":\"de\",\"heading\":\"test\",\"body\":\"test%0A\"}]}]}";

    @Test
    public void createAllNewsTest() throws JSONException {
        JSONObject json = new JSONObject(allNewsJson);
        AllNews allNews = new AllNews(json);

        assertEquals(allNews.getAllNews().get(0).getId(), 2);
        assertEquals(allNews.getAllNews().get(0).getTimestamp(), 1451400418000L);
        assertEquals(allNews.getAllNews().get(0).getLatitude(), 60.442423);
        assertEquals(allNews.getAllNews().get(0).getLongitude(), 22.26044);
        assertNull(allNews.getAllNews().get(0).getCountryCode());
        assertEquals(allNews.getAllNews().get(0).getNewsType(), News.NewsType.LOCATION_NEWS);
        assertEquals(allNews.getAllNews().get(0).getLocalizations().get(12).get(0), "de");
        assertEquals(allNews.getAllNews().get(0).getLocalizations().get(12).get(1), "Test+Nachricht");
        assertEquals(allNews.getAllNews().get(0).getLocalizations().get(12).get(2), "Dies+ist+eine+Testnachricht");

        assertEquals(allNews.getAllNews().get(1).getId(), 4);
        assertEquals(allNews.getAllNews().get(1).getTimestamp(), 1451989479000L);
        assertEquals(allNews.getAllNews().get(1).getLatitude(), 0.0);
        assertEquals(allNews.getAllNews().get(1).getLongitude(), 0.0);
        assertNull(allNews.getAllNews().get(1).getCountryCode());
        assertEquals(allNews.getAllNews().get(1).getNewsType(), News.NewsType.GLOBAL_NEWS);
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(16).get(0), "ar");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(16).get(1), "Arabic+Titel");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(16).get(2), "Arabic+Nachricht");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(17).get(0), "fr");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(17).get(1), "French+Titel");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(17).get(2), "French+Nachricht");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(18).get(0), "en");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(18).get(1), "English+Title");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(18).get(2), "English+Message");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(19).get(0), "de");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(19).get(1), "Deutscher+Titel");
        assertEquals(allNews.getAllNews().get(1).getLocalizations().get(19).get(2), "Deutscher+Text");

        assertEquals(allNews.getAllNews().get(2).getId(), 6);
        assertEquals(allNews.getAllNews().get(2).getTimestamp(), 1452198210000L);
        assertEquals(allNews.getAllNews().get(2).getLatitude(), 0.0);
        assertEquals(allNews.getAllNews().get(2).getLongitude(), 0.0);
        assertEquals(allNews.getAllNews().get(2).getCountryCode(), "de");
        assertEquals(allNews.getAllNews().get(2).getNewsType(), News.NewsType.COUNTRY_NEWS);
        assertEquals(allNews.getAllNews().get(2).getLocalizations().get(20).get(0), "de");
        assertEquals(allNews.getAllNews().get(2).getLocalizations().get(20).get(1), "test");
        assertEquals(allNews.getAllNews().get(2).getLocalizations().get(20).get(2), "test%0A");
    }
}
