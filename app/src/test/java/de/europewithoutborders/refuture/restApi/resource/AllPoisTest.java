package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import de.europewithoutborders.refuture.BuildConfig;

import static junit.framework.Assert.assertEquals;

/**
 * Unit test for AllPois.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class AllPoisTest {

    private String allPoisJson = "{\"locations\":[{\"id\":1,\"latitude\":60.442423,\"longitude\":22.26044,\"localizations\":[{\"id\": 1, \"language\":\"en\",\"name\":\"test+poi\",\"description\":\"poi+for+testing\",\"capacity\":\"\"},{\"id\": 2, \"language\":\"de\",\"name\":\"Test+Ort\",\"description\":\"Ort+zum+Testen\",\"capacity\":\"\"}],\"type\":\"housing\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Unterbringung\"},{\"language\":\"en\",\"name\":\"Housing\"}]},{\"id\":2,\"latitude\":60.442423,\"longitude\":22.26044,\"ratio\":20,\"localizations\":[{\"id\": 3, \"language\":\"de\",\"name\":\"Ort+Zwo\",\"description\":\"Zweiter+Ort\",\"capacity\":\"100+Betten\"},{\"id\": 4, \"language\":\"en\",\"name\":\"Second+location\",\"description\":\"second+location+for+testing+%3A%29\",\"capacity\":\"100+beds\"}],\"type\":\"housing\",\"type_localizations\":[{\"language\":\"de\",\"name\":\"Unterbringung\"},{\"language\":\"en\",\"name\":\"Housing\"}]}]}";

    @Test
    public void createAllNewsTest() throws JSONException {
        JSONObject json = new JSONObject(allPoisJson);
        AllPois allPois = new AllPois(json);
        assertEquals(allPois.getAllPois().get(0).getId(), 1);
        assertEquals(allPois.getAllPois().get(0).getLatitude(), 60.442423);
        assertEquals(allPois.getAllPois().get(0).getLongitude(), 22.26044);
        assertEquals(allPois.getAllPois().get(0).getRatio(), -1);
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(1).get(0), "en");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(1).get(1), "test+poi");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(1).get(2), "poi+for+testing");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(1).get(3), "");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(2).get(0), "de");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(2).get(1), "Test+Ort");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(2).get(2), "Ort+zum+Testen");
        assertEquals(allPois.getAllPois().get(0).getLocalizations().get(2).get(3), "");
        assertEquals(allPois.getAllPois().get(0).getType(), "housing");
        assertEquals(allPois.getAllPois().get(0).getTypeLocalizations().get("en"), "Housing");
        assertEquals(allPois.getAllPois().get(0).getTypeLocalizations().get("de"), "Unterbringung");

        assertEquals(allPois.getAllPois().get(1).getId(), 2);
        assertEquals(allPois.getAllPois().get(1).getLatitude(), 60.442423);
        assertEquals(allPois.getAllPois().get(1).getLongitude(), 22.26044);
        assertEquals(allPois.getAllPois().get(1).getRatio(), 20);
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(3).get(0), "de");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(3).get(1), "Ort+Zwo");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(3).get(2), "Zweiter+Ort");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(3).get(3), "100+Betten");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(4).get(0), "en");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(4).get(1), "Second+location");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(4).get(2), "second+location+for+testing+%3A%29");
        assertEquals(allPois.getAllPois().get(1).getLocalizations().get(4).get(3), "100+beds");
        assertEquals(allPois.getAllPois().get(1).getType(), "housing");
        assertEquals(allPois.getAllPois().get(1).getTypeLocalizations().get("en"), "Housing");
        assertEquals(allPois.getAllPois().get(1).getTypeLocalizations().get("de"), "Unterbringung");
    }
}
