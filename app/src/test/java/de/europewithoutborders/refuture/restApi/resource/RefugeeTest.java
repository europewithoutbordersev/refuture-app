package de.europewithoutborders.refuture.restApi.resource;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import de.europewithoutborders.refuture.BuildConfig;

import static junit.framework.Assert.assertEquals;

/**
 * Unit test for Refugee.
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class RefugeeTest {

    private String refugeeJson = "{\"refugee_id\": 2}";

    @Test
    public void createRefugeeTest() throws JSONException {
        JSONObject json = new JSONObject(refugeeJson);
        Refugee refugee = new Refugee(json);
        assertEquals(refugee.getRefugeeId(), 2);
    }
}
