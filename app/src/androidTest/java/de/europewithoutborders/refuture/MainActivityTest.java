package de.europewithoutborders.refuture;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.notNullValue;

/**
 * UI test for MainActivity
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void aboutMapButtonTest() {
        onView(withId(R.id.main_show_map_button)).check(matches(notNullValue()));
        onView(withId(R.id.main_show_map_button)).check(matches(withText(R.string.show_map)));
        onView(withId(R.id.main_show_map_button)).check(matches(isDisplayed()));
    }

    @Test
    public void aboutNewsButtonTest() {
        onView(withId(R.id.main_show_news_button)).check(matches(notNullValue()));
        onView(withId(R.id.main_show_news_button)).check(matches(withText(R.string.show_news)));
        onView(withId(R.id.main_show_news_button)).check(matches(isDisplayed()));
        onView(withId(R.id.main_show_news_button)).perform(click());
    }

    @Test
    public void aboutSettingsButtonTest() {
        onView(withId(R.id.main_show_settings_button)).check(matches(notNullValue()));
        onView(withId(R.id.main_show_settings_button)).check(matches(withText(R.string.show_settings)));
        onView(withId(R.id.main_show_settings_button)).check(matches(isDisplayed()));
        onView(withId(R.id.main_show_settings_button)).perform(click());
    }

    @Test
    public void aboutUsButtonTest() {
        onView(withId(R.id.main_show_about_us_button)).check(matches(notNullValue()));
        onView(withId(R.id.main_show_about_us_button)).check(matches(withText(R.string.show_about)));
        onView(withId(R.id.main_show_about_us_button)).check(matches(isDisplayed()));
        onView(withId(R.id.main_show_about_us_button)).perform(click());
    }

    @Test
    public void aboutRefreshButtonTest() {
        onView(withId(R.id.main_refresh_button)).check(matches(notNullValue()));
        onView(withId(R.id.main_refresh_button)).check(matches(isDisplayed()));
        onView(withId(R.id.main_refresh_button)).perform(click());
    }
    
}