package de.europewithoutborders.refuture;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * UI test for AboutActivity
 *
 * @author Europe Without Borders e.V.
 * @version 1.0
 */
@RunWith(AndroidJUnit4.class)
public class AboutActivityTest {

    @Rule
    public ActivityTestRule<AboutActivity> mActivityRule = new ActivityTestRule<>(AboutActivity.class);

    @Test
    public void componentTest() {
        onView(withText(R.string.app_name));
        onView(withText(R.string.disclaimer));
    }
}
